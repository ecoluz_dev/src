
from ecoclientes.models import Inversionista, Flujo
import pandas as pd
from re import sub
from decimal import Decimal as D
import datetime



s3 = pd.read_csv('flu.csv', encoding='utf-8')
s3.fillna(' ', inplace=True)
s3.replace('.', '', inplace=True)
s3.replace(',', '.', inplace=True)
s3 = s3.drop('Unnamed: 0', axis=1)

s = 0
n = 0
for index, row in s3.iterrows():
    if len(unicode(row[0]))<4 or len(unicode(row[11])) < 5:
        0
    else:
        inv = Inversionista.objects.create(persona=unicode(row[0]))
        setattr(inv, 'PRLV', unicode(row[1]))
        setattr(inv, 'capital', sub(r'[^\d.]', '', row[2]))
        setattr(inv, 'tasa_efectiva', sub(r'[^\d.]', '', row[4]))
        setattr(inv, 'tasa_efectiva_real', sub(r'[^\d.]', '', row[5]))
        setattr(inv, 'tasa_anual', sub(r'[^\d.]', '',row[6]))
        #setattr(inv, 'interes', row[7])
        #setattr(inv, 'monto_deuda', row[7])
        try:
            setattr(inv, 'inicio', datetime.datetime.strptime(row[9], "mm/dd/yyyy"))
            setattr(inv, 'termino', datetime.datetime.strptime(row[10], "mm/dd/yyyy"))
        except:
            print row[9], row[10]
        #setattr(inv, 'dias', row[7])
        #setattr(inv, 'intereses_hoy', row[7])
        setattr(inv, 'folio_fiscales', row[15])
        setattr(inv, 'estatus_PRLV', row[16])
        #print row[4], row[6], row[9], row[10]
        inv.save()



#
# s = 0
# n = 0
# for index, row in s1.iterrows():
#     if len(unicode(row[3]))<4:
#         0
#     else:
#         inv = Inversionista.objects.create(persona=row[3] )
#         setattr(inv, 'ventas_cxc_inventarios', unicode(row[1]))
#         setattr(inv, 'banco', unicode(row[2]))
#         setattr(inv, 'total_entradas', unicode(row[4]))
#         setattr(inv, 'capital', unicode(row[5]))
#         setattr(inv, 'deuda', row[7])
#         setattr(inv, 'intereses_hoy', row[7])
#         setattr(inv, 'monto_deuda', row[7])
#         setattr(inv, 'cxp_comdar', row[7])
#         setattr(inv, 'cxp_fabricantes', row[7])
#         setattr(inv, 'superhabit_deficit_hoy', row[7])
#         setattr(inv, 'superhabit_deficit_total', row[7])
#         setattr(inv, 'hora_entrega', row[7])
#         setattr(inv, 'puntuacion', row[7])
#         inv.save()
#         if not created:
#             n += 1
#             print row[3]
# print s, n
#
#
#

