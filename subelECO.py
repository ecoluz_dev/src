import os, sys
import pandas as pd
import numpy as np
import math
import io
from ecoclientes.models import Eco, Equipo

xls = pd.ExcelFile('/Users/ecoluzsistemas2/Desktop/eco171.xlsx')
s1 = xls.parse(4, header=2, skiprows=[0,1,2])
s2 = xls.parse(5, header=2, skiprows=[0,1,2])
s3 = xls.parse(6, header=2, skiprows=[0,1,2])
s4 = xls.parse(7, header=2, skiprows=[0,1,2])
s5 = xls.parse(8, header=2, skiprows=[0,1,2])
s6 = xls.parse(9, header=2, skiprows=[0,1,2])
s7 = xls.parse(10, header=2, skiprows=[0,1,2])
s8 = xls.parse(11, header=2, skiprows=[0,1,2])
s9 = xls.parse(12, header=2, skiprows=[0,1,2])
s10 = xls.parse(13, header=2, skiprows=[0,1,2])

todas = [s1, s2,s3,s4,s5,s6,s7,s8,s9,s10]

for s in todas:
    del s['Unnamed: 0']
    for index, row in s.iterrows():
        if len(unicode(row[0]))<4:
            0
        else:
            yastaba = Eco.objects.filter(fxf=row[0])
            if yastaba:
                equipo = Equipo.objects.create(fxf_eqp=Eco.objects.get(fxf=row[0]))
                setattr(equipo, 'modelo_equipo', unicode(row[4])[:58])
                setattr(equipo, 'cantidad', 0 if math.isnan(float(row[5])) or len(unicode(row[5]))<1 else row[5] )
                setattr(equipo, 'subtotal_equipo', 0 if len(unicode(row[6]))<1 or len(unicode(row[6]))>20 else row[6])
                setattr(equipo, 'cayd_cobrado_e', 0 if len(unicode(row[7]))<1 else row[7])
                setattr(equipo, 'numero_serie', 'CHECALO' if len(unicode(row[13]))> 99 else row[13] )
                setattr(equipo, 'eqp_e', 0 if len(unicode(row[14]))<1 else row[14])
                setattr(equipo, 'reguladores_e', 0 if len(unicode(row[15]))<1 else row[15])
                setattr(equipo, 'mdm_e', 0 if len(unicode(row[16]))<1 else row[16])
                setattr(equipo, 'com_e', 0 if len(unicode(row[17]))<1 else row[17])
                setattr(equipo, 'man_e', 0 if len(unicode(row[18]))<1 else row[18])
                setattr(equipo, 'cayd_pagado_e', 0 if len(unicode(row[19]))<1 else row[19])
                setattr(equipo, 'subtotal_operacion_e', 0 if len(unicode(row[20]))<1 else row[20])
                setattr(equipo, 'financiamiento_e', 0 if len(unicode(row[21]))<1 else row[21])
                setattr(equipo, 'costo_total_financiamiento_e', 0 if len(unicode(row[22]))<1 else row[22])
                setattr(equipo, 'pago_total_e', 0 if len(unicode(row[23]))<1 else row[23])
                setattr(equipo, 'numero_dias_e', 0 if pd.isnull(row[24]) or len(unicode(row[24]))>4 else row[24] )
                setattr(equipo, 'com_dar_e', 0 if len(unicode(row[25]))<1 else row[25] )
                setattr(equipo, 'adeudos_eq', 0 if len(unicode(row[26]))<1 else row[26])
                setattr(equipo, 'factura_fab_n', 'CHECALO' if len(unicode(row[10]))> 20 else row[10])
                equipo.save()
            else:
                eco = Eco.objects.create()
                setattr(eco, 'fxf', row[0])
                setattr(eco, 'numero_credito', 'CHECALO' if len(unicode(row[1]))> 20 else row[1])
                setattr(eco, 'tipo_venta', 'PAEEEM' if unicode(row[1])[:5] == 'PAEEEM' else row[1])
                setattr(eco, 'nombre_beneficiario', row[2])
                setattr(eco, 'estatus', unicode(row[3]).upper())
                setattr(eco, 'fecha_pago', None if len(unicode(row[9]))<6 or  row[9]=='FAC - 0370044'  or row[9] == 'F185378' or row[9] == '408 Facturama'  else row[9])
                setattr(eco, 'fecha_compra', None if len(unicode(row[11]))<6 or row[11]=='FAC - 0370044' or row[11] == 'F185378' or row[11] == '408 Facturama' else row[11])
                setattr(eco, 'obs', row[27])
                setattr(eco, 'factura_ecoluz_n', 'CHECALO' if len(unicode(row[8]))> 20 else row[8])
                setattr(eco, 'factura_costos_dar_n', 'CHECALO' if len(unicode(row[12]))> 20 else row[12])
                eco.save()
                equipo = Equipo.objects.create(fxf_eqp=eco)
                setattr(equipo, 'modelo_equipo', unicode(row[4])[:58])
                setattr(equipo, 'cantidad', 0 if math.isnan(float(row[5])) or len(unicode(row[5]))<1 else row[5] )
                setattr(equipo, 'subtotal_equipo', 0 if len(unicode(row[6]))<1 or len(unicode(row[6]))>20 else row[6])
                setattr(equipo, 'cayd_cobrado_e', 0 if len(unicode(row[7]))<1 else row[7])
                setattr(equipo, 'numero_serie', 'CHECALO' if len(unicode(row[13]))> 99 else row[13] )
                setattr(equipo, 'eqp_e', 0 if len(unicode(row[14]))<1 else row[14])
                setattr(equipo, 'reguladores_e', 0 if len(unicode(row[15]))<1 else row[15])
                setattr(equipo, 'mdm_e', 0 if len(unicode(row[16]))<1 else row[16])
                setattr(equipo, 'com_e', 0 if len(unicode(row[17]))<1 else row[17])
                setattr(equipo, 'man_e', 0 if len(unicode(row[18]))<1 else row[18])
                setattr(equipo, 'cayd_pagado_e', 0 if len(unicode(row[19]))<1 else row[19])
                setattr(equipo, 'subtotal_operacion_e', 0 if len(unicode(row[20]))<1 else row[20])
                setattr(equipo, 'financiamiento_e', 0 if len(unicode(row[21]))<1 else row[21])
                setattr(equipo, 'costo_total_financiamiento_e', 0 if len(unicode(row[22]))<1 else row[22])
                setattr(equipo, 'pago_total_e', 0 if len(unicode(row[23]))<1 else row[23])
                setattr(equipo, 'numero_dias_e', 0 if pd.isnull(row[24]) or len(unicode(row[24]))>4 else row[24] )
                setattr(equipo, 'com_dar_e', 0 if len(unicode(row[25]))<1 else row[25] )
                setattr(equipo, 'adeudos_eq', 0 if len(unicode(row[26]))<1 else float(row[26]))
                setattr(equipo, 'factura_fab_n', 'CHECALO' if len(unicode(row[10]))> 20 else row[10] )
                equipo.save()

