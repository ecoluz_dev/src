# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

#Este modelo se usa para almacenar los costos de los páneles y demás en la BD. En la BD hay sólo un registro y ese es
# el único que se usa en la plataforma
class Costo(models.Model):
    precio_panel = models.DecimalField(verbose_name="Precio del panel (dólares)", max_digits=20, decimal_places=2)
    precio_microinversor = models.DecimalField(verbose_name="Precio del microinversor (dólares)", max_digits=20, decimal_places=2)
    precio_sistema_montaje = models.DecimalField(verbose_name="Precio del sistema de montaje (dólares)", max_digits=20, decimal_places=2)
    precio_sistema_monitoreo = models.DecimalField(verbose_name="Precio del sistema de monitoreo (dólares)", max_digits=20, decimal_places=2)
    precio_instalacion_envio = models.DecimalField(verbose_name="Costo de instalación con envío (MXN)", max_digits=20, decimal_places=2)
    interes_mensual = models.DecimalField(verbose_name="Tasa de interés mensual", max_digits=20, decimal_places=5, blank=True)