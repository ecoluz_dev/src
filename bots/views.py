# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from selenium import webdriver
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from bots.forms import ConoceTuAhorroForm, CotizacionForm, DatosDeConsumoForm, PlanForm
from django.urls import reverse
from selenium.common.exceptions import NoSuchElementException
from django.contrib import messages
import math
from re import sub
from decimal import Decimal
from .models import Costo
from clientes.models import Cliente
from .exceptions import PeriodoCFEInsuficienteException, AdeudoCFEException, NoDatosDeConsumoException, NoSeEncontroException, NoCosteableException, TarifaNoPermitidaException, FIDELoginException, NoPendienteException
from selenium.common.exceptions import TimeoutException
from django.contrib.auth.decorators import login_required
from django.views.generic import FormView
from decouple import config
from pyvirtualdisplay import Display
from selenium.webdriver.common.keys import Keys
import time
from django.template.loader import render_to_string
import requests

# Create your views here.

def initiate_webdriver():
    """
    Esta función crea el objeto Chromedriver que será usado por los bots para ingresar a las diferentes páginas.
    :return: webdriver
    """
    option = webdriver.ChromeOptions()
    option.binary_location = config('GOOGLE_CHROME_BIN')
   # option.add_argument('--disable-gpu')
    option.add_argument('window-size=1600,900')
    option.add_argument('--no-sandbox')
    if not config('DEBUG', cast=bool):
        display = Display(visible=0, size=(1600, 900))
        display.start()
        option.add_argument("--headless")
    else:
        option.add_argument("--incognito")
    # prefs = {"profile.managed_default_content_settings.images": 2}
    # option.add_experimental_option("prefs", prefs)

    # create new instance of chrome in headless mode
    return webdriver.Chrome(executable_path=config('CHROMEDRIVER_PATH'), chrome_options=option)


def cfe_login(browser):
    """
    Abre la página de la CFE e ingresa con los datos de la cuenta de EcoLuz
    :param browser: Instancia de Chromedriver para ingresar a la página
    :return:
    """
    try:
        browser.get("http://www.cfe.gob.mx/casa/1_Micuenta/Paginas/Consulta-tu-recibo.aspx")
        # user credentials
        user = browser.find_element_by_xpath('//*[@id="ctl00_PHContenidoPag_UCLogin2_LoginUsuario_UserName"]')
        user.send_keys(config('CFE_LOGIN'))
        password = browser.find_element_by_css_selector('#ctl00_PHContenidoPag_UCLogin2_LoginUsuario_Password')

        password.send_keys(config('CFE_PASS'))
        login = browser.find_element_by_xpath('//*[@id="ctl00_PHContenidoPag_UCLogin2_LoginUsuario_LoginButton"]')
        login.click()
    except TimeoutException:
            raise NoDatosDeConsumoException


def registrar_nuevo_servicio(browser, nombre, rpu):
    """
    Registra un nuevo servicio en la página de la CFE usando el RPU y el nombre del usuario
    :param browser: instancia de Chromedriver
    :param nombre: Nombre para registrar nuevo servicio en la página
    :param rpu: RPU del nuevo servicio a registrar
    :return:
    """
    try:
        browser.get("https://app.cfe.gob.mx/Aplicaciones/CCFE/Recibos/Consulta/Registro.aspx")
        agregar_nuevo_rpu = browser.find_element_by_xpath('//*[@id="ctl00_PHContenidoPag_btnNuevo"]')
        agregar_nuevo_rpu.send_keys("\n")
        nombre_del_servicio = browser.find_element_by_xpath('//*[@id="ctl00_PHContenidoPag_txtNombre"]')
        nombre_del_servicio.send_keys(nombre)
        numero_de_servicio = browser.find_element_by_xpath('//*[@id="ctl00_PHContenidoPag_txtRPU"]')
        numero_de_servicio.send_keys(rpu)
        agregar = browser.find_element_by_xpath('//*[@id="ctl00_PHContenidoPag_btnAceptar"]')
        agregar.click()
    except TimeoutException:
            raise NoDatosDeConsumoException


def consulta_datos_servicio(browser, rpu):
    """
    Consulta los datos del historial de consumo del nuevo servicio que se registró en la página de la CFE
    :param browser: Instancia de Chromedriver
    :param rpu: RPU a consultar
    :return:
    """
    try:
        browser.get("https://app.cfe.gob.mx/Aplicaciones/CCFE/Recibos/Consulta/Default.aspx")
        selector = Select(browser.find_element_by_xpath('//*[@id="ddlRPU"]'))
        selector.select_by_visible_text(rpu)
        ver_historial = browser.find_element_by_xpath('//*[@id="lnkHistorialDePagos"]')
        ver_historial.click()
    except TimeoutException:
            raise NoDatosDeConsumoException
    except NoSuchElementException:
            raise NoSeEncontroException


def obtener_datos_de_consumo(browser, tarifa):
    """
    Extrae los datos del historial de consumo de la página de la CFE
    :param browser: Instancia de Chromedriver
    :param tarifa: Tipo de tarifa
    :return: datos_de_consumo es un arreglo que su vez contiene diccionarios los cuales contienen la información
    """
    try:
        # Dependiendo del tipo de tarifa se obtiene un conjunto de 6 o 12 datos
        if tarifa == '02' or tarifa == "dac" or tarifa == "DAC":
            numero_de_datos = 6
        else:
            numero_de_datos = 12
        consumo_kw = []
        monto_abonado = []
        monto_total = []

        for i in range(2, numero_de_datos + 2):
            kw = browser.find_element_by_xpath(
                '// *[ @ id = "ctl00_PHContenidoPag_gvHistorialPagos"] /tbody / tr[% 02d] / td[3]' % i)
            kw_consumidos = Decimal(kw.text)
            consumo_kw.append(kw_consumidos)

            abono = browser.find_element_by_xpath(
                '// *[ @ id = "ctl00_PHContenidoPag_gvHistorialPagos"] /tbody / tr[% 02d] / td[4]' % i)
            abono_value = Decimal(sub(r'[^\d.]', '', abono.text))
            monto_abonado.append(abono_value)

            total = browser.find_element_by_xpath(
                '// *[ @ id = "ctl00_PHContenidoPag_gvHistorialPagos"] /tbody / tr[% 02d] / td[5]' % i)
            total_value = Decimal(sub(r'[^\d.]', '', total.text))
            monto_total.append(total_value)

        precio_actual_kw = monto_total[0]/consumo_kw[0]
        for k in range(1, len(monto_total)):
            monto_total[k] = precio_actual_kw * consumo_kw[k]

        if len(monto_total) > len(monto_abonado):
            raise AdeudoCFEException
        if len(consumo_kw) < numero_de_datos:
            raise PeriodoCFEInsuficienteException
        datos_de_consumo = [consumo_kw, monto_abonado, monto_total]
    except TimeoutException:
            raise NoDatosDeConsumoException
    return datos_de_consumo


def calcular_consumos_paneles_y_precio_final(browser, datos):
    """
    Realiza el cálculo de consumo promedio mensual de Kw del usuario, calcula el número de paneles necesarios para
    cubrir ese consumo y el precio final de la instalación del equipo solar.
    :param browser: Instancia de Chromedriver
    :param datos: Datos de consumo del usuario
    :return:
    """
    costos_list = list(Costo.objects.all()) # get_object_or_404(Costo, id=int(2))
    costos = costos_list[0]
    print(costos)
    kw = datos[0]
    abonos = datos[1]
    totales = datos[2]
    precio_actual_Kw = totales[0] / kw[0]
    for i in range(1, len(abonos)):
        abonos[i] = kw[i] * precio_actual_Kw

    promedio_kw = round((round(Decimal(sum(kw) / len(kw)), 2) / 2), 2)
    promedio_pago = round((round(Decimal(sum(abonos) / len(abonos)), 2) / 2), 2)
    numero_de_paneles = Decimal(math.ceil(Decimal(promedio_kw) / Decimal(Decimal(4.5)*Decimal(30.5)*(Decimal(320)/Decimal(1000))*Decimal(0.9))))
    browser.get("http://www.dof.gob.mx/")
    dof = browser.find_element_by_xpath(
        '//*[@id="table_menu_right"]/tbody/tr[1]/td/table[3]/tbody/tr[2]/td/table/tbody/tr[3]/td/p[1]')
    valor_dof = dof.text
    precio_dolar = valor_dof.splitlines()[1]
    tipo_de_cambio = Decimal(sub(r'[^\d.]', '', precio_dolar))
    print("Consulta DOF: hecho")
    paneles = Decimal(costos.precio_panel) * tipo_de_cambio * numero_de_paneles
    microinversores = tipo_de_cambio * Decimal(costos.precio_microinversor) * numero_de_paneles
    sistema_montaje = tipo_de_cambio * Decimal(costos.precio_sistema_montaje) * numero_de_paneles
    sistema_monitoreo = tipo_de_cambio * Decimal(costos.precio_sistema_monitoreo)
    instalacion_envio = Decimal(costos.precio_instalacion_envio) * numero_de_paneles
    # paneles = Decimal(156.81) * tipo_de_cambio * numero_de_paneles
    # microinversores = tipo_de_cambio * Decimal(85.12) * numero_de_paneles
    # sistema_montaje = tipo_de_cambio * Decimal(72.68) * numero_de_paneles
    # sistema_monitoreo = tipo_de_cambio * Decimal(231.07)
    # instalacion_envio = Decimal(1223.29) * numero_de_paneles
    costo_total_iva = (paneles + microinversores + sistema_montaje + sistema_monitoreo + instalacion_envio) + Decimal(
        1.16)
    precio_final = round(costo_total_iva / Decimal(0.7), 2)
    data = {'promedio_kw': str(promedio_kw), 'promedio_pago': str(promedio_pago), 'numero_de_paneles': str(numero_de_paneles),
            'precio_final': str(precio_final)}
    browser.close()
    return data


def calcular_numero_de_pagos(pago_mensual, credito):
    """
    Calcula el número de pagos necesarios para cubrir el crédito a partir de cierto pago mensual especificado.
    Usa el modelo de interés compuesto
    :param pago_mensual:
    :param credito: Monto del crédito
    :return:
    """
    tasa_de_interes = Decimal(0.0145)
    a = Decimal((Decimal(tasa_de_interes)*Decimal(credito))/Decimal(pago_mensual))
    if a > 1:
        raise NoCosteableException
    num = -1 * round(Decimal(math.log(Decimal(1) - a)), 5)
    den = round(Decimal(math.log(Decimal(1) + Decimal(tasa_de_interes))), 5)
    n = math.ceil(num / den)
    return n


@login_required(login_url='clientes:registro')
def index(request):
    """
    Maneja la página de Conoce tu ahorro y usa las demás funciones para el bot de CFE
    :param request:
    :return:
    """
    error_messages = messages.get_messages(request)
    form_class = ConoceTuAhorroForm
    tarifas = {"02", "09", "DAC"}
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            data = {}
            try:
                nombre = request.POST.get('nombre')
                rpu = request.POST.get('rpu')
                tarifa = request.POST.get('tarifa')
                if tarifa not in tarifas:
                    raise TarifaNoPermitidaException
                browser = initiate_webdriver()
                print("iniciar browser: hecho")
                cfe_login(browser)
                print("login_cfe: hecho")
                registrar_nuevo_servicio(browser, nombre, rpu)
                print("registrar nuevo servicio: hecho")
                consulta_datos_servicio(browser, rpu)
                print("consultar: hecho")
                datos_de_consumo = obtener_datos_de_consumo(browser, tarifa)
                if datos_de_consumo:
                    print("obtener datos de consumo: hecho")
                    data = calcular_consumos_paneles_y_precio_final(browser, datos_de_consumo)
                    print("calculos: hecho")
                else:
                    raise NoDatosDeConsumoException('Error obteniendo datos de consumo de CFE')
            except NoSuchElementException:
                messages.error(request, 'Por favor, revise que la información ingresada sea correcta.')
            request.session['saved_data'] = data
            request.session['ahorro-submitted']=True
            return HttpResponseRedirect(reverse('bots:calculadora'))
    else:
        form = form_class()
    return render(request, 'bots/conoce_tu_ahorro.html', {'form': form, 'authenticated': request.user.is_authenticated()}, {'messages': error_messages})


def resultados(request):
    interes = 0.00145
    datos_de_consumo = request.session.pop('saved_data', None)
    form_class = CotizacionForm
    factor = 1
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            form.data = form.data.copy()
            factor = Decimal(request.POST.get('factor_de_pago_mensual'))
            promedio_pago = Decimal(request.POST.get('importe_mensual'))
            credito = Decimal(request.POST.get('credito'))
            pago_mensual = math.ceil(Decimal(Decimal(promedio_pago / 2) * Decimal(factor)))
            n = calcular_numero_de_pagos(pago_mensual, credito)
            form.data['credito'] = request.POST.get('credito')
            form.data['numero_de_paneles'] = request.POST.get('numero_de_paneles')
            form.data['consumo_kw_mes'] = request.POST.get('consumo_kw_mes')
            form.data['factor_de_pago_mensual'] = request.POST.get('factor_de_pago_mensual')
            form.data['importe_mensual'] = request.POST.get('importe_mensual')
            form.data['numero_de_pagos'] = n
            form.data['pago_mensual'] = pago_mensual
            return render(request, 'bots/resultados.html', {'form': form})

    else:
        form = form_class(request.GET or None)
        if datos_de_consumo:
            form.fields['credito'].initial = datos_de_consumo['precio_final']
            form.fields['numero_de_paneles'].initial = datos_de_consumo['numero_de_paneles']
            form.fields['consumo_kw_mes'].initial = datos_de_consumo['promedio_kw']
            form.fields['factor_de_pago_mensual'].initial = 1
            form.fields['importe_promedio'].initial = datos_de_consumo['promedio_pago']
            importe_promedio = Decimal(datos_de_consumo['promedio_pago'])
            pago_mensual = math.ceil(Decimal(Decimal(importe_promedio / 2) * factor))
            form.fields['pago_mensual'].initial = pago_mensual
            credito = Decimal(datos_de_consumo['precio_final'])
            n = calcular_numero_de_pagos(pago_mensual, credito)
            form.fields['numero_de_pagos'].initial = n

    return render(request, 'bots/resultados.html', {'form': form})

#Realiza los cálculos de cuántos páneles serán necesarios y presenta las tres opciones: moderado, express y largo plazo
class Calculadora(FormView):
    def get(self, request, *args, **kwargs):
        excedePR = False
        form_consumo = DatosDeConsumoForm
        form_express = CotizacionForm
        form_moderado = CotizacionForm
        form_lp = CotizacionForm

        factor_express = Decimal(2)
        factor_moderado = Decimal(1.20)
        factor_lp = Decimal(1)

        consumo = form_consumo(request.GET or None)
        express = form_express(request.GET or None)
        moderado = form_moderado(request.GET or None)
        lp = form_lp(request.GET or None)

        datos_de_consumo = request.session.pop('saved_data', None)
        if not request.session.get('ahorro-submitted', False):
            return HttpResponseRedirect(reverse('clientes:registro'))
        else:
            if datos_de_consumo:
                credito = round(Decimal(datos_de_consumo['precio_final']), 2)
                consumo.fields['credito'].initial = datos_de_consumo['precio_final']
                consumo.fields['numero_de_paneles'].initial = datos_de_consumo['numero_de_paneles']
                consumo.fields['consumo_kw_mes'].initial = datos_de_consumo['promedio_kw']
                consumo.fields['importe_promedio'].initial = datos_de_consumo['promedio_pago']

                importe_promedio = round(Decimal(datos_de_consumo['promedio_pago']), 2)

                pago_mensual_lp = math.ceil(Decimal(Decimal(importe_promedio) * factor_lp))
                lp.fields['pago_mensual_cot'].initial = pago_mensual_lp
                lp.fields['pago_mensual_cot'].widget.attrs['id'] = 'id_pago_mensual_lp'
                n_lp = calcular_numero_de_pagos(pago_mensual_lp, credito)

                lp.fields['numero_de_pagos_cot'].initial = n_lp
                lp.fields['numero_de_pagos_cot'].widget.attrs['id'] = 'id_numero_de_pagos_lp'
                lp.fields['numero_de_pagos_cot'].widget.attrs['class'] = 'lp'
                lp.fields['plan_cot'].initial = "Largo Plazo"

                lp.fields['numero_de_paneles_cot'].initial = datos_de_consumo['numero_de_paneles']
                lp.fields['consumo_kw_mes_cot'].initial = datos_de_consumo['promedio_kw']
                lp.fields['importe_mensual_cfe_cot'].initial = datos_de_consumo['promedio_pago']
                lp.fields['credito_cot'].initial = datos_de_consumo['precio_final']
                lp.fields['enganche_cot'].initial = 0
                lp.fields['enganche_cot'].widget.attrs['id'] = 'id_enganche_lp'

                pago_mensual_express = math.ceil(Decimal(Decimal(importe_promedio) * factor_express))
                express.fields['pago_mensual_cot'].initial = pago_mensual_express
                express.fields['pago_mensual_cot'].widget.attrs['id'] = 'id_pago_mensual_express'
                n_express = calcular_numero_de_pagos(pago_mensual_express, credito)
                if n_express > 120:
                    excedePR = True

                express.fields['numero_de_pagos_cot'].initial = n_express
                express.fields['numero_de_pagos_cot'].widget.attrs['id'] = 'id_numero_de_pagos_express'
                express.fields['numero_de_pagos_cot'].widget.attrs['class'] = 'express'
                express.fields['plan_cot'].initial = "Express"

                express.fields['numero_de_paneles_cot'].initial = datos_de_consumo['numero_de_paneles']
                express.fields['consumo_kw_mes_cot'].initial = datos_de_consumo['promedio_kw']
                express.fields['importe_mensual_cfe_cot'].initial = datos_de_consumo['promedio_pago']
                express.fields['credito_cot'].initial = datos_de_consumo['precio_final']
                express.fields['enganche_cot'].initial = 0
                express.fields['enganche_cot'].widget.attrs['id'] = 'id_enganche_express'

                pago_mensual_moderado = math.ceil(Decimal(Decimal(importe_promedio) * factor_moderado))
                moderado.fields['pago_mensual_cot'].initial = pago_mensual_moderado
                moderado.fields['pago_mensual_cot'].widget.attrs['id'] = 'id_pago_mensual_moderado'
                n_moderado = calcular_numero_de_pagos(pago_mensual_moderado, credito)
                moderado.fields['numero_de_pagos_cot'].initial = n_moderado
                moderado.fields['numero_de_pagos_cot'].widget.attrs['id'] = 'id_numero_de_pagos_moderado'
                moderado.fields['numero_de_pagos_cot'].widget.attrs['class'] = 'moderado'
                moderado.fields['plan_cot'].initial = "Moderado"

                moderado.fields['numero_de_paneles_cot'].initial = datos_de_consumo['numero_de_paneles']
                moderado.fields['consumo_kw_mes_cot'].initial = datos_de_consumo['promedio_kw']
                moderado.fields['importe_mensual_cfe_cot'].initial = datos_de_consumo['promedio_pago']
                moderado.fields['credito_cot'].initial = datos_de_consumo['precio_final']
                moderado.fields['enganche_cot'].initial = 0
                moderado.fields['enganche_cot'].widget.attrs['id'] = 'id_enganche_moderado'

                return render(request, 'bots/calculadora.html', {'form_consumo': consumo, 'form_express': express, 'form_moderado': moderado, 'form_lp': lp, 'excedePR': excedePR, 'authenticated': request.user.is_authenticated()})
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('bots:index-calc'))
        else:
            return HttpResponseRedirect(reverse('clientes:registro'))


def procesar_opcion(request):

    form_plan = PlanForm
    cliente_actual = get_object_or_404(Cliente, pk=request.user.id)

    if request.method == 'POST':
        plan = form_plan(data=request.POST, prefix='plan')
        if plan.is_valid():
            cliente_actual.plan_elegido = plan.data['plan_cot']
            cliente_actual.monto_credito = plan.cleaned_data['credito_cot']
            cliente_actual.numero_de_paneles = plan.cleaned_data['paneles']
            cliente_actual.enganche = plan.cleaned_data['enganche_cot']
            cliente_actual.numero_de_pagos = plan.cleaned_data['numero_de_pagos_cot']
            cliente_actual.pago_mensual = plan.cleaned_data['pago_mensual_cot']
            cliente_actual.consumo_kw_promedio = plan.cleaned_data['consumo_kw_mes_cot']
            cliente_actual.importe_promedio_cfe = plan.cleaned_data['importe_mensual_cfe_cot']
            cliente_actual.save()
    return HttpResponseRedirect(reverse('clientes:editar_perfil'))

#Procesa la opción del tipo de plan seleccionado por el usuario y guarda los datos en el perfil del usuario
class ProcesarPlanView(FormView):
    form_class = CotizacionForm
    template_name = 'bots/error.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(self.request.GET or None)

        context = self.get_context_data(**kwargs)

        context['question_form'] = form
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(self.request.POST)
        form.data = form.data.copy()
        form.data['credito_cot'] = str(form.data['credito_cot']).replace(',', '')
        form.data['enganche_cot'] = str(form.data['enganche_cot']).replace(',', '')
        form.data['pago_mensual_cot'] = str(form.data['pago_mensual_cot']).replace(',', '')
        form.data['importe_mensual_cfe_cot'] = str(form.data['importe_mensual_cfe_cot']).replace(',', '')
        if form.is_valid():
            request.session['form-submitted'] = True
            cliente_actual = get_object_or_404(Cliente, pk=request.user.id)
            cliente_actual.plan_elegido = form.cleaned_data['plan_cot']
            cliente_actual.monto_credito = form.cleaned_data['credito_cot']
            cliente_actual.numero_de_paneles = form.cleaned_data['numero_de_paneles_cot']
            cliente_actual.enganche = form.cleaned_data['enganche_cot']
            cliente_actual.numero_de_pagos = form.cleaned_data['numero_de_pagos_cot']
            cliente_actual.pago_mensual = form.cleaned_data['pago_mensual_cot']
            cliente_actual.consumo_kw_promedio = form.cleaned_data['consumo_kw_mes_cot']
            cliente_actual.importe_promedio_cfe = form.cleaned_data['importe_mensual_cfe_cot']
            cliente_actual.estado_del_financiamiento = 'PENDIENTE'
            cliente_actual.save()
            # current_site = get_current_site(request)
            # enviar_mail_cotizacion(cliente_actual.first_name, cliente_actual.email, current_site)
            return HttpResponseRedirect(reverse('bots:tu-cotizacion'))

        else:
            print("Not valid")
            form = self.form_class(self.request.GET or None)

            context = self.get_context_data(**kwargs)

            context['question_form'] = form

            context = {
                'form': form,
                'authenticated': request.user.is_authenticated()
            }

            return render(request, self.template_name, context)


def enviar_mail_cotizacion(user, to_mail, domain):
    """
    Envía la cotización del plan seleccionado
    :param user:
    :param to_mail:
    :param domain:
    :return:
    """
    subject = 'Tu cotización Eco Luz'
    message = render_to_string('bots/tu_cotizacion.html', {
        'user': user,
        'domain': domain,
    })
    return requests.post(
        "https://api.mailgun.net/v3/mail.ecoluz.mx/messages",
        auth=("api", config('EMAIL_API_KEY')),
        data={"from": "Ecoluz <contacto@ecoluz.mx>",
              "to": to_mail,
              "subject": subject,
              "html": message})

#Le muestra al usuario los datos de su cotización y plan seleccionado
class TuCotizacion(FormView):

    def get(self, request):
        if not request.session.get('form-submitted', False):
            return HttpResponseRedirect(reverse('clientes:acceso'))
        else:
            args = {'user': request.user, 'authenticated': request.user.is_authenticated()}
            return render(request, 'bots/gracias-cotizacion.html', args)


def fide_login(browser):
    """
    Ingresa a la página del FIDE con las credenciales de sistemas.ecoluz
    :param browser:
    :return:
    """
    try:
        browser.get("http://paeeem.fide.org.mx:2000/Cuenta/Acceder")
        # user credentials
        user = browser.find_element_by_xpath('//*[@id="NombreUsuario"]')
        user.send_keys(config('FIDE_USER'))
        password = browser.find_element_by_xpath('//*[@id="Contrasena"]')
        password.send_keys(config('FIDE_PASS'))



        login = browser.find_element_by_xpath('/html/body/div[1]/div/div/div/div/div[3]/div/div/div[1]/form/fieldset/div[2]/button')
        login.click()
        WebDriverWait(browser, 20).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="sidebar"]/ul/li[1]/a'))).click()      #  try:


   #     barra_izq = browser.find_element_by_xpath('//*[@id="sidebar"]/ul/li[1]/a')
  #      barra_izq.send_keys("\n")
        solicitud_credito = browser.find_element_by_xpath('//*[@id="sidebar"]/ul/li[1]/ul/li[1]/a')
        solicitud_credito.send_keys("\n")

      #   barra_izq = browser.find_element_by_xpath('//*[@id="sidebar"]/ul/li[1]/a')
      #   barra_izq.click()
      #
      # #  solicitud_credito = browser.find_element_by_xpath('//*[@id="sidebar"]/ul/li[1]/ul/li[1]/a')
      #   solicitud_credito = browser.find_element_by_xpath('/html/body/div[2]/div[1]/ul/li[1]/ul/li[1]/a')
      #   solicitud_credito.send_keys("\n")
    except TimeoutException or NoSuchElementException:
        raise FIDELoginException


def consulta_datos_credito(browser, numero_paeem):
    """
    Consulta y extracción automatizada de datos para el número de crédito proporcionado. Usa Selenium y Chromedriver.
    :param browser: Instancia de chromedriver
    :param numero_paeem: Número de crédito FIDE a consultar
    :return: Diccionario data['monto_credito'(float), 'tipo_persona'(string), 'datos_solicitante'(dictionary),
                              'contacto'(dictionary), 'domicilio_fiscal'(dictionary), 'equipos'(dictionary),
                              'presupesto'(dictionary)]
    """
    data = {} # Contenedor principal de datos
    # Los siguientes diccionarios se agregarán a data
    datos = {}
    domicilio_fiscal = {}
    contacto = {}
    equipos = {}
    presupuesto = {}

    time.sleep(10)
    input_credito = browser.find_element_by_xpath('//*[@id="NumeroCredito"]')
    input_credito.send_keys(numero_paeem)
    buscar = browser.find_element_by_xpath('//*[@id="divDetalleEco"]/form/div[5]/div/button')
    buscar.click()
    time.sleep(3)
    celda_estatus = browser.find_element_by_xpath('//*[@id="solicitudesGrid"]/div[2]/table/tbody/tr/td[3]')
    estatus = celda_estatus.text
    if estatus == "PENDIENTE":
        celda = browser.find_element_by_xpath('//*[@id="solicitudesGrid"]/div[2]/table/tbody/tr[1]/td[3]')
        celda.click()
        time.sleep(2)
        selector = browser.find_element_by_xpath('//*[@id="divDetalleEco"]/form/div[6]/div/span/span/span[2]')
        #selector.send_keys("\n")
        selector.click()
        webdriver.ActionChains(browser).send_keys(Keys.ARROW_DOWN).perform()
        #selector.send_keys("\n")
      #  selector.click()
        webdriver.ActionChains(browser).send_keys(Keys.ARROW_DOWN).perform()
       # selector.send_keys("\n")
    #    selector.click()
        webdriver.ActionChains(browser).send_keys(Keys.ARROW_DOWN).perform()
        btn_ir = browser.find_element_by_xpath('//*[@id="btnAccionSuperior"]')
        btn_ir.send_keys("\n")

        time.sleep(3)
        monto_select = browser.find_element_by_xpath('//*[@id="DatCreMontoFinanciado"]')
        # DOMICILIO FISCAL
        cp_select = browser.find_element_by_xpath('//*[@id="DomicilioFisCp"]')
        estado_select = browser.find_element_by_xpath('//*[@id="DomicilioFisEstado"]')
        deleg_municipio_select = browser.find_element_by_xpath('//*[@id="DomicilioFisDelegacionMuni"]')
        colonia_select = browser.find_element_by_xpath('//*[@id="DomicilioFisColonia"]')
        calle_select = browser.find_element_by_xpath('//*[@id="DomicilioFisCalle"]')
        num_ext_select = browser.find_element_by_xpath('//*[@id="DomicilioFisNumEx"]')
        num_int_select = browser.find_element_by_xpath('//*[@id="DomicilioFisNumIn"]')

        domicilio_fiscal['calle'] = calle_select.get_attribute('value')
        domicilio_fiscal['num_ext'] = num_ext_select.get_attribute('value')
        domicilio_fiscal['num_int'] = num_int_select.get_attribute('value')
        domicilio_fiscal['colonia'] = colonia_select.get_attribute('value')
        domicilio_fiscal['deleg_municipio'] = deleg_municipio_select.get_attribute('value')
        domicilio_fiscal['estado'] = estado_select.get_attribute('value')
        domicilio_fiscal['cp'] = cp_select.get_attribute('value')

        telefono_select = browser.find_element_by_xpath('//*[@id="DomicilioFisTel"]')
        contacto['telefono'] = telefono_select.get_attribute('value')

        persona_select = browser.find_element_by_xpath('//*[@id="DatCreTipoPerson"]')

        tipo_persona = persona_select.get_attribute('value')

        if tipo_persona == "PERSONA FISICA":
            nombre_select = browser.find_element_by_xpath('//*[@id="DatSoliciNombre"]')
            apellido1_select = browser.find_element_by_xpath('//*[@id="DatSoliciApePat"]')
            apellido2_select = browser.find_element_by_xpath('//*[@id="DatSoliciApeMat"]')
            email_select = browser.find_element_by_xpath('//*[@id="DatSoliciEmail"]')
            rfc_select = browser.find_element_by_xpath('//*[@id="DatSoliciRfc"]')
            contacto['email'] = email_select.get_attribute('value')
            datos['rfc'] = rfc_select.get_attribute('value')
            datos['nombre'] = nombre_select.get_attribute('value')
            datos['apellido1'] = apellido1_select.get_attribute('value')
            datos['apellido2'] = apellido2_select.get_attribute('value')

        else:
            email_select = browser.find_element_by_xpath('//*[@id="DatSoliciEmailM"]')
            contacto['email'] = email_select.get_attribute('value')
            razon_social_select = browser.find_element_by_xpath('//*[@id="DatSoliciRazonSociM"]')
            rfc_selectM = browser.find_element_by_xpath('//*[@id="DatSoliciRfcM"]')
            datos['rfc'] = rfc_selectM.get_attribute('value')
            datos['razon_social'] = razon_social_select.get_attribute('value')

        data['monto_credito'] = sub(r'[^\d.]', '', monto_select.get_attribute('value'))
        data['tipo_persona'] = tipo_persona
        data['datos_solicitante'] = datos
        data['contacto'] = contacto
        data['domicilio_fiscal'] = domicilio_fiscal

        boton_siguiente = browser.find_element_by_xpath('//*[@id="Siguiente"]')
        boton_siguiente.send_keys("\n")

        # tabla de equipos alta
        table = browser.find_element_by_xpath('//*[@id="grdEquipoAlta"]/div[2]/table')
        i = 0
        for row in table.find_elements_by_xpath(".//tr"):
            eq = [td.text for td in row.find_elements_by_xpath(".//td[text()]")]# [@class='text-center']
            equipo = {}
            equipo['marca'] = eq[1]
            equipo['modelo'] = eq[2]
            equipo['tipo_producto'] = eq[3].encode('utf-8')
            equipo['cantidad'] = eq[4]
            equipo['precio_unitario'] = eq[5]
            equipo['capacidad'] = eq[6]
            equipo['gastos_instalacion'] = eq[7]
            equipos[i] = equipo
            print i, equipo['modelo'], equipo['cantidad']
            i += 1
        data['equipos'] = equipos
        costo_equipos_select = browser.find_element_by_xpath('//*[@id="pt2"]/div[11]/div[2]/div/div[2]/div/div/div/div[1]/div[2]')
        cayd_select = browser.find_element_by_xpath('//*[@id="pt2"]/div[11]/div[2]/div/div[2]/div/div/div/div[6]/div[2]')
        presupuesto['costo_equipos'] = costo_equipos_select.text
        presupuesto['cayd'] = cayd_select.text
        data['presupuesto'] = presupuesto
    else:
        print("Estatus folio: Error - No aparece como PENDIENTE")
        logoutFIDE(browser)
        raise NoPendienteException

    menu_select = browser.find_element_by_xpath('//*[@id="navbar-container"]/div[2]/ul/li/a')
    menu_select.send_keys("\n")
    logout_select = browser.find_element_by_xpath('//*[@id="CerrarSesion"]')
    logout_select.send_keys("\n")
    time.sleep(3)
    browser.close()
    return data


def logoutFIDE(browser):
    """
    Cierra sesión en la página del FIDE
    :param browser:
    :return:
    """
    menu_select = browser.find_element_by_xpath('//*[@id="navbar-container"]/div[2]/ul/li/a')
    menu_select.send_keys("\n")
    logout_select = browser.find_element_by_xpath('//*[@id="CerrarSesion"]')
    logout_select.send_keys("\n")
    time.sleep(3)
    browser.close()
    print("FIDE: logout")


