function calcular(){
    var glower1 = $('.express');
    var glower2 = $('.moderado');
    var glower3 = $('.lp');
    var monto = parseFloat(document.getElementById("id_credito").value.replace(/\,/g, ""));
    //console.log(monto);
    var prom_pago = parseFloat(document.getElementById("id_importe_promedio").value.replace(/\,/g, ""));
    //console.log(prom_pago);
    var enganche = parseFloat(document.getElementById("enganche").value.replace(/\,/g, ""));
    var cred = monto - enganche;
    var pag_exp = parseFloat(document.getElementById("id_pago_mensual_express").value.replace(/\,/g, ""));
    //console.log(pag_exp);
    var pag_mod = parseFloat(document.getElementById("id_pago_mensual_moderado").value.replace(/\,/g, ""));
    //console.log(pag_mod);
    var pag_lp = parseFloat(document.getElementById("id_pago_mensual_lp").value.replace(/\,/g, ""));
    //console.log(pag_lp);
    tdi = 0.0145;

    var n_exp = getN(pag_exp, tdi, cred)

    var n_mod = getN(pag_mod, tdi, cred)

    var n_lp = getN(pag_lp, tdi, cred)

    document.getElementById("id_enganche_express").value = enganche;
    document.getElementById("id_enganche_moderado").value = enganche;
    document.getElementById("id_enganche_lp").value = enganche;

    document.getElementById("id_numero_de_pagos_express").value = n_exp;
    glower1.toggleClass('active');
    document.getElementById("id_numero_de_pagos_moderado").value = n_mod;
    glower2.toggleClass('active');
    document.getElementById("id_numero_de_pagos_lp").value = n_lp;
    glower3.toggleClass('active');
    window.setTimeout(function(){
        glower1.toggleClass('active');
        glower2.toggleClass('active');
        glower3.toggleClass('active');
    }, 1000);

}

function getN(p_mens, tdi, cred){
    var a = (tdi * cred) / p_mens;
    var num = (-1 * Math.log(1 - a)).toFixed(5);
    var den = (Math.log(1 + tdi)).toFixed(5);
    var n = Math.ceil(num / den);
    return n
}

window.onload = function () {
    $("#calculate_button").on('click', function(e) {
        calcular();
    });
}