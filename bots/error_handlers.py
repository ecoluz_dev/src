# -*- coding: utf-8 -*-

from django.contrib import messages
from django.utils.deprecation import MiddlewareMixin
from .exceptions import PeriodoCFEInsuficienteException, AdeudoCFEException, NoDatosDeConsumoException, ExcedePRSException, NoSeEncontroException, NoCosteableException, TarifaNoPermitidaException, FIDELoginException, NoPendienteException
from responses import RedirectToRefererResponse


# Mixin for compatibility with Django <1.10
class HandleBusinessExceptionMiddleware(MiddlewareMixin):
    def process_exception(self, request, exception):
        if isinstance(exception, PeriodoCFEInsuficienteException):
            message = 'No cuentas con el tiempo requerido de servicio continuo de luz. Para acceder a un financiamiento EcoLuz es necesario contar con, por lo menos, un año.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, AdeudoCFEException):
            message = 'Tu recibo de CFE presenta adeudo. Es necesario que liquides ese adeudo para poder acceder a un financiamiento EcoLuz.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, NoDatosDeConsumoException):
            message = 'Hubo un problema al intentar obtener su información de CFE. Por favor, intente de nuevo más tarde.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, ExcedePRSException):
            message = 'Nuestra calculadora indica que el periodo de liquidación de tu financiamiento excedería 10 años. Actualmente no contamos con una opción financiera para estos casos.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, NoSeEncontroException):
            message = 'Por favor, revise que la información ingresada sea correcta.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, NoCosteableException):
            message = 'Tu costo mensual de luz es muy bajo. El financiamiento no es adecuado para tu caso. En un futuro tendremos una solución para casos como el tuyo.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, TarifaNoPermitidaException):
            message = 'Por el momento no contamos con una opción para usuarios con tu tipo de tarifa. En cuanto tengamos una solución para ti nos pondremos en contacto.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, FIDELoginException):
            message = 'La página del FIDE presenta problemas. Intenta de nuevo más tarde.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
        if isinstance(exception, NoPendienteException):
            message = 'El número de crédito PAEEEM ingresado no aparece como PENDIENTE. Recuerda que sólo se pueden ingresar folios que tengan estatus PENDIENTE.'
            messages.error(request, message)
            return RedirectToRefererResponse(request)
