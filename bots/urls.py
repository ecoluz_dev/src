from django.conf.urls import url

from . import views
app_name = 'bots'
urlpatterns = [
    url(r'^$', views.index, name='index-calc'),
    url(r'^consulta/$', views.consulta_datos_servicio, name='consulta'),
    url(r'^resultados/$', views.resultados, name='resultados'),
    url(r'^calculadora/$', views.Calculadora.as_view(), name='calculadora'),
    url(r'^procesar/$', views.procesar_opcion, name='procesar'),
    url(r'^plan-express/$', views.ProcesarPlanView.as_view(), name='plan-express'),
    url(r'^plan-moderado/$', views.ProcesarPlanView.as_view(), name='plan-moderado'),
    url(r'^plan-largo-plazo/$', views.ProcesarPlanView.as_view(), name='plan-lp'),
    url(r'^tu-cotizacion/$', views.TuCotizacion.as_view(), name='tu-cotizacion'),
]