# -*- coding: utf-8 -*-


class AdeudoCFEException(Exception):
    '''
    Excepción para cuando se detecta adeudo con CFE
    '''
    pass


class PeriodoCFEInsuficienteException(Exception):
    '''
    Excepción para cuando el recibo de CFE presenta menos de un año de pagos consecutivos
    '''
    pass

class ExcedePRSException(Exception):
    '''
    Excepción para cuando el período de recuperación es mayor a 10 años
    '''
    pass

class NoDatosDeConsumoException(Exception):
    '''
    Excepción en caso de que falle la obtención de datos de consumo de CFE
    '''
    pass

class NoSeEncontroException(Exception):
    '''
    Excepción para cuando Selenium no encuentra un elemento en la página
    '''
    pass

class NoCosteableException(Exception):
    """
    Excepción para cuando el pago mensual es demasiado pequeño
    """

class TarifaNoPermitidaException(Exception):
    """
    Excepción para cuando el usuario ingresa una tarifa distinta de DAC, 01 o 09
    """

class FIDELoginException(Exception):
    """
    Excepción para el hay un problema al intentar acceder a la cuenta del FIDE
    """

class NoPendienteException(Exception):
    """
    Excepción para cuando se consulta un crédito PAEEEM cuyo estatus no es PENDIENTE
    """