#-*- coding: utf-8 -*-
from django import forms
from django.utils.translation import gettext_lazy as _

#Aquí se definen los campos de los diferentes formularios que se usan en la plataforma
class ConoceTuAhorroForm(forms.Form):
    nombre = forms.CharField(label=_("Nombre"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    rpu = forms.IntegerField(label=_("Número de servicio"), widget=forms.NumberInput(attrs={'class': 'form-control'}), min_value=0, max_value=999999999999, required=True)
    tarifa = forms.CharField(label=_("Tipo de tarifa"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=5, required=True)


class DatosDeConsumoForm(forms.Form):
    numero_de_paneles = forms.DecimalField(label=_("Cantidad de paneles necesarios"), widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'True'}), decimal_places=2)
    consumo_kw_mes = forms.DecimalField(label=_("Consumo mensual promedio de Kw"), widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'True'}), decimal_places=2)
    importe_promedio = forms.DecimalField(label=_("Importe mensual promedio"), widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'True'}), decimal_places=2)
    credito = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(attrs={'class': 'form-control', 'readonly': 'True'}), decimal_places=2)


class CotizacionForm(forms.Form):
    plan_cot = forms.CharField(label=_("Nombre"), widget=forms.TextInput(
        attrs={'readonly': 'True', 'type': 'hidden'}), max_length=30, required=True)

    numero_de_paneles_cot = forms.IntegerField(label=_("Número de paneles"), widget=forms.TextInput(
        attrs={'readonly': 'True', 'type': 'hidden'}))

    consumo_kw_mes_cot = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(
        attrs={'readonly': 'True', 'type': 'hidden'}), decimal_places=2)

    importe_mensual_cfe_cot = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(
        attrs={'readonly': 'True', 'type': 'hidden'}), decimal_places=2)

    credito_cot = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(
        attrs={'readonly': 'True', 'type': 'hidden'}), decimal_places=2)

    enganche_cot = forms.DecimalField(label=_("Enganche"), widget=forms.TextInput(
        attrs={'readonly': 'True', 'type': 'hidden'}), decimal_places=2)

    numero_de_pagos_cot = forms.IntegerField(label=_("Número de pagos mensuales"), widget=forms.TextInput(
        attrs={'readonly': 'True'}))

    pago_mensual_cot = forms.DecimalField(label=_("Pago mensual"),
                                          widget=forms.TextInput(attrs={'readonly': 'True'}),
                                          decimal_places=2)


class CalculadoraForm(forms.Form):
    ahorro_previo = forms.NumberInput()
    numero_de_pagos = forms.IntegerField(label=_("Número de pagos mensuales"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}))
    pago_mensual = forms.DecimalField(label=_("Pago mensual"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), decimal_places=2)


class PlanForm(forms.Form):
    plan = forms.CharField(label=_("Plan elegido"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), max_length=20)
    monto_credito = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), decimal_places=2)
    eng = forms.DecimalField(label=_("Enganche"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), decimal_places=2)
    paneles = forms.DecimalField(label=_("Número de paneles"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), decimal_places=0)
    mensualidad = forms.DecimalField(label=_("Pago mensual"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), decimal_places=2)
    num_pagos = forms.DecimalField(label=_("Número de mensualidades"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True'}), decimal_places=2)
    consumo_kw = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True', 'type': 'hidden'}), decimal_places=2)
    importe_mens_cfe = forms.DecimalField(label=_("Monto del crédito"), widget=forms.TextInput(attrs={'class': 'form-control', 'disabled': 'True', 'type': 'hidden'}), decimal_places=2)