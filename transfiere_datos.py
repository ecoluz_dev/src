from ecoclientes.models import Equipo, Inventario

#with connection.schema_editor() as schema_editor:
 #    schema_editor.remove_field('ecoclientes', 'marca')

eqps = Equipo.objects.all()
for equipo in eqps:
    inv, created = Inventario.objects.get_or_create(modelo=equipo.modelo_equipo, )
    setattr(equipo, 'modelo_eqp', inv)
    equipo.save()
    inv.save()


