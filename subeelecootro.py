import os, sys
import pandas as pd
import numpy as np
import math
import io
from ecoclientes.models import Eco, Equipo, Inventario

xls = pd.ExcelFile('ECO20171.xlsx')
s1 = xls.parse(4, header=2, skiprows=[0,1,2])
s2 = xls.parse(5, header=2, skiprows=[0,1,2])
s3 = xls.parse(6, header=2, skiprows=[0,1,2])
s4 = xls.parse(7, header=2, skiprows=[0,1,2])
s5 = xls.parse(8, header=2, skiprows=[0,1,2])
s6 = xls.parse(9, header=2, skiprows=[0,1,2])
s7 = xls.parse(10, header=2, skiprows=[0,1,2])
s8 = xls.parse(11, header=2, skiprows=[0,1,2])
s9 = xls.parse(12, header=2, skiprows=[0,1,2])
s10 = xls.parse(13, header=2, skiprows=[0,1,2])
xls1 = pd.ExcelFile('ECO20181.xlsx')
s11 = pd.read_csv('s11.csv')
s12 = xls1.parse(5, header=2, skiprows=[0,1,2])
s13 = xls1.parse(6, header=2, skiprows=[0,1,2])
s14 = xls1.parse(7, header=2, skiprows=[0,1,2])
s15 = xls1.parse(8, header=2, skiprows=[0,1,2])
s16 = xls1.parse(9, header=2, skiprows=[0,1,2])
s17 = xls1.parse(10, header=2, skiprows=[0,1,2])
s18 = xls1.parse(11, header=2, skiprows=[0,1,2])
s19 = xls1.parse(12, header=2, skiprows=[0,1,2])
s20 = xls1.parse(13, header=2, skiprows=[0,1,2])

todas = [s1, s2,s3,s4,s5,s6,s7,s8,s9,s10, s12,s13,s14,s15,s16,s17,s18,s19,s20]

i = 0
j = 0
for s in todas:
    del s['Unnamed: 0']
    for index, row in s.iterrows():
        if len(unicode(row[0]))<4 or row[0] == 'MCM-SAL-131B':
            0
        else:
            yastaba = Eco.objects.filter(fxf=row[0])
            if yastaba:
                equipo = Equipo.objects.create(fxf_eqp=Eco.objects.get(fxf=row[0]))
                try:
                    setattr(equipo, 'modelo_eqp', Inventario.objects.get(modelo=row[4]))
                except:
                    try:
                        setattr(equipo, 'modelo_eqp', Inventario.objects.filter(modelo__icontains=row[4])[0])
                    except:
                        i += 1
                        print row[0], row[4], row[6]
                setattr(equipo, 'cantidad', 0 if math.isnan(float(row[5])) or len(unicode(row[5]))<1 else row[5] )
                setattr(equipo, 'subtotal_equipo', 0 if len(unicode(row[6]))<1 or len(unicode(row[6]))>20 else row[6])
                setattr(equipo, 'cayd_cobrado_e', 0 if len(unicode(row[7]))<1 else row[7])
                setattr(equipo, 'numero_serie', 'CHECALO' if len(unicode(row[13]))> 99 else row[13] )
                setattr(equipo, 'eqp_e', 0 if len(unicode(row[14]))<1 else row[14])
                setattr(equipo, 'reguladores_e', 0 if len(unicode(row[15]))<1 else row[15])
                setattr(equipo, 'mdm_e', 0 if len(unicode(row[16]))<1 else row[16])
                setattr(equipo, 'com_e', 0 if len(unicode(row[17]))<1 else row[17])
                setattr(equipo, 'man_e', 0 if len(unicode(row[18]))<1 else row[18])
                setattr(equipo, 'cayd_pagado_e', 0 if len(unicode(row[19]))<1 else row[19])
                setattr(equipo, 'subtotal_operacion_e', 0 if len(unicode(row[20]))<1 else row[20])
                setattr(equipo, 'financiamiento_e', 0 if len(unicode(row[21]))<1 else row[21])
                setattr(equipo, 'costo_total_financiamiento_e', 0 if len(unicode(row[22]))<1 else row[22])
                setattr(equipo, 'pago_total_e', 0 if len(unicode(row[23]))<1 else row[23])
                setattr(equipo, 'numero_dias_e', 0 if pd.isnull(row[24]) or len(unicode(row[24]))>4 else row[24] )
                setattr(equipo, 'com_dar_e', 0 if len(unicode(row[25]))<1 else row[25] )
                setattr(equipo, 'adeudos_eq', 0 if len(unicode(row[26]))<1 else row[26])
                setattr(equipo, 'factura_fab_n', 'CHECALO' if len(unicode(row[10])) < 1 else row[10])
                equipo.save()
            else:
                j +=1
                print 'no hubo folio' + row[0]
print i, j