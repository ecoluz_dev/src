# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from clientes.models import Cliente

# Create your views here.

#Aquí se indican las diferentes páginas que se muestran en las secciones de la página principal

def inicio(request):
    return render(request, 'index.html', {'authenticated': request.user.is_authenticated()})


def proyectos(request):
    return render(request, 'proyectos.html', {'authenticated': request.user.is_authenticated()})


def que_es_ecoluz(request):
    return render(request, 'que-es-ecoluz.html', {'authenticated': request.user.is_authenticated()})


def conoce_tu_ahorro(request):
    return render(request, 'conoce-ahorro.html', {'authenticated': request.user.is_authenticated()})


def aviso_de_privacidad(request):
    return render(request, 'aviso-privacidad.html', {'authenticated': request.user.is_authenticated()})


def terminos_y_condiciones(request):
    return render(request, 'terminos-condiciones.html', {'authenticated': request.user.is_authenticated()})


def contacto(request):
    return render(request, 'contacto.html', {'authenticated': request.user.is_authenticated()})
