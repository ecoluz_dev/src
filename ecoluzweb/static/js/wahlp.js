<!-- WhatsHelp.io widget -->
    (function () {
        var options = {
            facebook: "248428562177763", // Facebook page ID
            whatsapp: "+5215540648271", // WhatsApp number
            company_logo_url: "https://ecoluz-assets.s3.amazonaws.com/static/images/ecoluz-logo.png", // URL of company logo (png, jpg, gif)
            greeting_message: "Bienvenida/o.\nQueremos ayudarte a ahorrar.", // Text of greeting message
            call_to_action: "Escríbenos, con gusto te atendemos.", // Call to action
            button_color: "#FF6550", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,whatsapp" // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
<!-- /WhatsHelp.io widget -->