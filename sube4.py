import os, sys
import pandas as pd
import numpy as np
import math
import io
from ecoclientes.models import Eco, Equipo, Inventario


xls = pd.ExcelFile('eco2018-1.xlsx')
s11 = xls.parse(4, header=2, skiprows=[0,1,2])
s12 = xls.parse(5, header=2, skiprows=[0,1,2])
s13 = xls.parse(6, header=2, skiprows=[0,1,2])
s14 = xls.parse(7, header=2, skiprows=[0,1,2])
s15 = xls.parse(8, header=2, skiprows=[0,1,2])
s16 = xls.parse(9, header=2, skiprows=[0,1,2])
s17 = xls.parse(10, header=2, skiprows=[0,1,2])
s18 = xls.parse(11, header=2, skiprows=[0,1,2])
s19 = xls.parse(12, header=2, skiprows=[0,1,2])
s20 = xls.parse(13, header=2, skiprows=[0,1,2])

todas = [ s12]

i = 0
for s in todas:
    del s['Unnamed: 0']
    for index, row in s.iterrows():
        if len(unicode(row[0]))<4:
            0
        elif row[0][:8] == 'PRF-GDL-':
            yastaba = Eco.objects.filter(fxf=row[0][:8]+row[0][-3:])
            if yastaba:
                equipo = Equipo.objects.create(fxf_eqp=Eco.objects.get(fxf=row[0][:8]+row[0][-3:]))
                try:
                    inv = Inventario.objects.get(modelo=row[4])
                except:
                    try:
                        inv = Inventario.objects.filter(modelo__icontains=row[4])[0]
                    except:
                        inv = Inventario.objects.create(modelo=row[4])
                        print 'Creé ' + str(inv)
                setattr(equipo, 'modelo_eqp', inv)
                setattr(equipo, 'cantidad', 0 if math.isnan(float(row[5])) or len(unicode(row[5]))<1 else row[5] )
                setattr(equipo, 'subtotal_equipo', 0 if len(unicode(row[6]))<1 or len(unicode(row[6]))>20 else row[6])
                setattr(equipo, 'cayd_cobrado_e', 0 if len(unicode(row[7]))<1 else row[7])
                setattr(equipo, 'numero_serie', 'CHECALO' if len(unicode(row[13]))> 99 else row[13] )
                setattr(equipo, 'eqp_e', 0 if len(unicode(row[14]))<1 else row[14])
                setattr(equipo, 'reguladores_e', 0 if len(unicode(row[15]))<1 else row[15])
                setattr(equipo, 'mdm_e', 0 if len(unicode(row[16]))<1 else row[16])
                setattr(equipo, 'com_e', 0 if len(unicode(row[17]))<1 else row[17])
                setattr(equipo, 'man_e', 0 if len(unicode(row[18]))<1 else row[18])
                setattr(equipo, 'cayd_pagado_e', 0 if len(unicode(row[19]))<1 else row[19])
                setattr(equipo, 'subtotal_operacion_e', 0 if len(unicode(row[20]))<1 else row[20])
                setattr(equipo, 'financiamiento_e', 0 if len(unicode(row[21]))<1 else row[21])
                setattr(equipo, 'costo_total_financiamiento_e', 0 if len(unicode(row[22]))<1 else row[22])
                setattr(equipo, 'pago_total_e', 0 if len(unicode(row[23]))<1 else row[23])
                setattr(equipo, 'com_dar_e', 0 if len(unicode(row[25]))<1 else row[25] )
                setattr(equipo, 'adeudos_eq', 0 if len(unicode(row[26]))<1 else row[26])
                setattr(equipo, 'descuentos', 0 if len(unicode(row[27])) < 1 else float(row[27]))
                setattr(equipo, 'obs_e', row[28])
                setattr(equipo, 'atrasos', row[29])
                setattr(equipo, 'factura_fab_n', 'CHECALO' if len(unicode(row[10]))> 60 else row[10])
                setattr(equipo, 'fecha_compra_e', None if len(unicode(row[11]))<6 or row[11]=='FAC - 0370044' or row[11] == 'F185378' or row[11] == '408 Facturama' else row[11])
                setattr(equipo, 'independiente', True)
                setattr(equipo, 'numero_dias_e', 0 if pd.isnull(row[24]) or len(unicode(row[24])) > 4 else row[24])
                equipo.save()
            else:
                eco = Eco.objects.create()
                setattr(eco, 'fxf', row[0][:8]+row[0][-3:])
                setattr(eco, 'numero_credito', 'CHECALO' if len(unicode(row[1]))> 20 else row[1])
                setattr(eco, 'tipo_venta', 'PAEEEM' if unicode(row[1])[:5] == 'PAEEEM' else row[1])
                setattr(eco, 'nombre_beneficiario', row[2])
                setattr(eco, 'estatus', unicode(row[3]).upper())
                setattr(eco, 'fecha_pago', None if len(unicode(row[9]))<6 or  row[9]=='FAC - 0370044'  or row[9] == 'F185378' or row[9] == '408 Facturama'  else row[9])
                setattr(eco, 'fecha_compra', None if len(unicode(row[11]))<6 or row[11]=='FAC - 0370044' or row[11] == 'F185378' or row[11] == '408 Facturama' else row[11])
                setattr(eco, 'com_dar', 0 if len(unicode(row[25])) < 1 else row[25])
                setattr(eco, 'obs', row[27])
                setattr(eco, 'numero_dias', 0 if pd.isnull(row[24]) or len(unicode(row[24]))>4 else row[24] )
                setattr(eco, 'factura_ecoluz_n', 'CHECALO' if len(unicode(row[8]))> 60 else row[8])
                setattr(eco, 'factura_costos_dar_n', 'CHECALO' if len(unicode(row[12]))> 60 else row[12])
                setattr(eco, 'solar', True)
                eco.save()
                equipo = Equipo.objects.create(fxf_eqp=eco)
                try:
                    setattr(equipo, 'modelo_eqp', Inventario.objects.get(modelo=row[4]))
                except:
                    try:
                        setattr(equipo, 'modelo_eqp', Inventario.objects.filter(modelo__icontains=row[4])[0])
                    except:
                        i += 1
                        print row[0], row[4], row[6]
                setattr(equipo, 'cantidad', 0 if math.isnan(float(row[5])) or len(unicode(row[5]))<1 else row[5] )
                setattr(equipo, 'subtotal_equipo', 0 if len(unicode(row[6]))<1 or len(unicode(row[6]))>20 else row[6])
                setattr(equipo, 'cayd_cobrado_e', 0 if len(unicode(row[7]))<1 else row[7])
                setattr(equipo, 'numero_serie', 'CHECALO' if len(unicode(row[13]))> 150 else row[13] )
                setattr(equipo, 'eqp_e', 0 if len(unicode(row[14]))<1 else row[14])
                setattr(equipo, 'reguladores_e', 0 if len(unicode(row[15]))<1 else row[15])
                setattr(equipo, 'mdm_e', 0 if len(unicode(row[16]))<1 else row[16])
                setattr(equipo, 'com_e', 0 if len(unicode(row[17]))<1 else row[17])
                setattr(equipo, 'man_e', 0 if len(unicode(row[18]))<1 else row[18])
                setattr(equipo, 'cayd_pagado_e', 0 if len(unicode(row[19]))<1 else row[19])
                setattr(equipo, 'subtotal_operacion_e', 0 if len(unicode(row[20]))<1 else row[20])
                setattr(equipo, 'financiamiento_e', 0 if len(unicode(row[21]))<1 else row[21])
                setattr(equipo, 'costo_total_financiamiento_e', 0 if len(unicode(row[22]))<1 else row[22])
                setattr(equipo, 'pago_total_e', 0 if len(unicode(row[23]))<1 else row[23])
                setattr(equipo, 'com_dar_e', 0 if len(unicode(row[25]))<1 else row[25] )
                setattr(equipo, 'adeudos_eq', 0 if len(unicode(row[26]))<1 else float(row[26]))
                setattr(equipo, 'descuentos', 0 if len(unicode(row[27]))<1 else float(row[27]))
                setattr(equipo, 'obs_e', row[28])
                setattr(equipo, 'atrasos', row[29])
                setattr(equipo, 'fecha_compra_e',
                        None if len(unicode(row[11])) < 6 or
                                row[11] == 'FAC - 0370044' or row[11] == 'F185378' or row[11] == '408 Facturama'
                        else row[11])
                setattr(equipo, 'independiente', True)
                setattr(equipo, 'factura_fab_n', 'CHECALO' if len(unicode(row[10]))> 60 else row[10] )
                setattr(equipo, 'numero_dias_e', 0 if pd.isnull(row[24]) or len(unicode(row[24])) > 4 else row[24])
                equipo.save()
print i
