# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext as _


# Create your models here.
def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'clientes/user_{0}/{1}'.format(instance.id, filename)


class UserManager(BaseUserManager):
    """Define a model manager for Cliente model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a Cliente with the given email and password."""
        if not email:
            raise ValueError('Correo electrónico obligatorio')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular Cliente with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Cliente(AbstractUser):
    ESTATUS_FINANCIAMIENTO = (

        ('APROBADO', 'APROBADO'),
        ('RECHAZADO', 'RECHAZADO'),
        ('VALIDANDO', 'VALIDANDO'),
        ('PENDIENTE', 'PENDIENTE'),
    )
    ESTADO_CIVIL = (

        ('SOLTERA/O', 'SOLTERA/O'),
        ('CASADA/O', 'CASADA/O'),
    )
    REGIMEN = (
        ('NO APLICA', 'NO APLICA'),
        ('SEPARACIÓN DE BIENES', 'SEPARACIÓN DE BIENES'),
        ('SOCIEDAD CONYUGAL', 'SOCIEDAD CONYUGAL'),
    )


    # Datos del cliente
    username = None
    email = models.EmailField('Correo electrónico', unique=True)
    foto_file = models.FileField(_('Foto del cliente'), upload_to=user_directory_path, blank=True)
    telefono = models.CharField(_('Teléfono'), max_length=10, blank=True)
    calle = models.CharField(_('Calle'), max_length=50, blank=True)
    numero_exterior = models.CharField(_('Número exterior'), max_length=10, blank=True)
    numero_interior = models.CharField(_('Número interior'), max_length=10, blank=True)
    colonia = models.CharField(_('Colonia'), max_length=50, blank=True)
    delegacion_municipio = models.CharField(_('Delegación o Municipio'), max_length=50, blank=True)
    ciudad = models.CharField(_('Ciudad'), max_length=50, blank=True)
    estado = models.CharField(_('Estado'), max_length=50, blank=True)
    codigo_postal = models.IntegerField(_('Código postal'), blank=True, default=0)
    estado_civil = models.CharField(_('Estado civil'), max_length=10, choices=ESTADO_CIVIL, blank=True)
    regimen_conyugal = models.CharField(_('Régimen conyugal'), max_length=20, choices=REGIMEN, blank=True)
    rfc = models.CharField(_('RFC'), max_length=20, blank=True)
    razon_social = models.CharField(_('Razón social'), max_length =50, blank=True)
    domicilio_fiscal = models.CharField(_('Domicilio fiscal'), max_length=50, blank=True)
    curp = models.CharField(_('CURP'), max_length=30, blank=True)
    # Documentos del cliente
    identificacion_file = models.FileField(_('Identificación (INE ambos lados/Pasaporte)'), upload_to=user_directory_path, blank=True)
    curp_file = models.FileField(_('Cédula CURP'), upload_to=user_directory_path, blank=True)
    cedula_rfc_file = models.FileField(_('Cédula RFC'), upload_to=user_directory_path, blank=True)
    recibo_luz_file = models.FileField(_('Recibo de luz'), upload_to=user_directory_path, blank=True)
    comprobante_ingresos_file = models.FileField(_('Comprobante de ingresos (último año)'), upload_to=user_directory_path, blank=True)
    comprobante_domicilio = models.FileField(_('Comprobante de domicilio'), upload_to=user_directory_path, blank=True)
    # Datos y documentos del obligado solidario
    obligado_nombre = models.CharField(_('Nombre'), max_length=20, blank=True)
    obligado_apellido = models.CharField(_('Apellido'), max_length=20, blank=True)
    obligado_relacion = models.CharField(_('Relación'), max_length=20, blank=True)
    obligado_rfc = models.CharField(_('RFC'), max_length=20, blank=True)
    obligado_curp = models.CharField(_('CURP'), max_length=30, blank=True)
    obligado_identificacion_file = models.FileField(_('Identificación (INE ambos lados/Pasaporte)'), upload_to=user_directory_path, blank=True)
    obligado_rfc_file = models.FileField(_('Cédula RFC'), upload_to=user_directory_path, blank=True)
    obligado_curp_file = models.FileField(_('Cédula CURP'), upload_to=user_directory_path, blank=True)
    obligado_recibo_luz = models.FileField(_('Recibo de luz'), upload_to=user_directory_path, blank=True)
    obligado_comprobante_domicilio = models.FileField(_('Comprobante de domicilio'), upload_to=user_directory_path, blank=True)
    #Datos del plan de finaciamiento
    plan_elegido = models.CharField(_('Plan de financiamiento elegido'), max_length=20, blank=True)
    monto_credito = models.CharField(_('Monto del crédito'), max_length=20, blank=True)
    numero_de_paneles = models.CharField(_('Cantidad de paneles'), max_length=10, blank=True)
    enganche = models.CharField(_('Enganche'), max_length=20, blank=True)
    numero_de_pagos = models.CharField(_('Cantidad de pagos'), max_length=5, blank=True)
    pago_mensual = models.CharField(_('Pago mensual'), max_length=20, blank=True)
    estado_del_financiamiento = models.CharField(_('Estatus del financiamiento'), max_length=9, choices=ESTATUS_FINANCIAMIENTO, default='PENDIENTE')
    consumo_kw_promedio = models.CharField(_('Consumo promedio Kw'), max_length=20, blank=True)
    importe_promedio_cfe = models.CharField(_('Importe promedio CFE'), max_length=20, blank=True)
    terms = models.BooleanField(_('Acepto los términos y condiciones'),
        error_messages={'required': 'Debes aceptar los términos y condiciones'}, blank=True, default=False
    )
    #Campos de verificación de cuenta, perfil completo y autorización de consulta en buró de crédito
    mail_confirmed = models.BooleanField(_('Correo electrónico confirmado'), blank=True, default=False)
    perfil_completo = models.BooleanField(_('Información de perfil completa'), blank=True, default=False)
    autorizacion_buro_credito = models.BooleanField(_('Autorizo a Eco Luz a consultar mi información crediticia'), error_messages={'required': 'Debes marcar esta casilla para poder continuar'}, blank=True, default=False)
    fecha_autorizacion_de_consulta = models.DateField(_('Fecha de autorización de consulta de información crediticia'), blank=True, null=True)
    hora_autorizacion_de_consulta = models.TimeField(_('Hora de autorización de consulta de información creditica'), blank=True, null=True)
    fecha_de_consulta_buro = models.DateField(_('Fecha de consulta de información crediticia'), blank=True, null=True)
    hora_de_consulta_buro = models.TimeField(_('Hora de consulta de información crediticia'), blank=True, null=True)
    tipo_usuario = models.CharField(_('tipo usuario'), max_length=12, blank=True, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()



