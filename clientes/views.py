# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import PasswordChangeForm
from clientes.forms import FormaDeRegistro, FormaEditarPerfil, PINForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from .tokens import account_activation_token
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib import messages
from .models import Cliente
from django.shortcuts import render, get_object_or_404
from twilio.rest import Client
import random
from decouple import config
from django.core.cache import cache
from django.urls import reverse
from django.utils import timezone
from datetime import date, time, datetime
from django.core.mail import send_mail
import requests

from django.core.cache import caches

# Create your views here.


def registro(request):
    if request.method == 'POST':
        form = FormaDeRegistro(request.POST)
        if form.is_valid():
            user = form.save()
            current_site = get_current_site(request)
            enviar_confirmacion_mail(user, form.cleaned_data['email'], current_site)
            new_user = authenticate(email=form.cleaned_data['email'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            return redirect('bots:index-calc')
    else:
        form = FormaDeRegistro()
    return render(request, 'clientes/registro.html', {'form': form})


def enviar_confirmacion_mail(user, to_mail, domain):
    subject = 'Confirma tu dirección de correo'
    message = render_to_string('clientes/confirmar-correo.html', {
        'user': user,
        'domain': domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    return requests.post(
        "https://api.mailgun.net/v3/mail.ecoluz.mx/messages",
        auth=("api", config('EMAIL_API_KEY')),
        data={"from": "Ecoluz <contacto@ecoluz.mx>",
              "to": to_mail,
              "subject": subject,
              "html": message})


def activate(request, uidb64, token):
    msg = messages.get_messages(request)
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = Cliente.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, Cliente.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.mail_confirmed = True
        user.save()
        login(request, user)
        messages.success(request, 'Tu correo ha sido confirmado ¡Gracias!')
        args = {'cliente_profile': request.user, 'messages': msg, 'authenticated': request.user.is_authenticated()}
        return render(request, 'clientes/profile.html', args)
    else:
        return render(request, 'clientes/confirmacion-correo-error.html')


@login_required(login_url='clientes:acceso')
def perfil(request):
    msg = messages.get_messages(request)
    cliente_actual = get_object_or_404(Cliente, pk=request.user.id)
    if not cliente_actual.perfil_completo:
        messages.warning(request, 'La información de su perfil está incompleta. Por favor, asegúrese de llenar toda la información para poder continuar con su financiamiento Eco Luz.')

    args = {'cliente_profile': request.user, 'authenticated': request.user.is_authenticated(), 'completar_perfil_message': msg}
    return render(request, 'clientes/profile.html', args)


@login_required(login_url='clientes:acceso')
def editar_perfil(request):
    if request.method == 'POST':
        cliente_form = FormaEditarPerfil(request.POST, request.FILES, instance=request.user)
        if cliente_form.is_valid():
            cliente_form.save()
            cliente_actual = get_object_or_404(Cliente, pk=request.user.id)
            if cliente_actual.perfil_completo:
                request.session['authorized'] = True
                return redirect('/clientes/autorizar-consulta-sic')
            else:
                return redirect('/clientes/perfil')
    else:
            cliente_form = FormaEditarPerfil(instance=request.user)
            cliente_form.fields['foto_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['foto_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['foto_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['identificacion_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['identificacion_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['identificacion_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['curp_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['curp_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['curp_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['cedula_rfc_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['cedula_rfc_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['cedula_rfc_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['recibo_luz_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['recibo_luz_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['recibo_luz_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['comprobante_ingresos_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['comprobante_ingresos_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['comprobante_ingresos_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['comprobante_domicilio'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['comprobante_domicilio'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['comprobante_domicilio'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['obligado_identificacion_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['obligado_identificacion_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['obligado_identificacion_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['obligado_rfc_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['obligado_rfc_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['obligado_rfc_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['obligado_curp_file'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['obligado_curp_file'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['obligado_curp_file'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['obligado_recibo_luz'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['obligado_recibo_luz'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['obligado_recibo_luz'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            cliente_form.fields['obligado_comprobante_domicilio'].widget.attrs['class'] = 'filestyle'
            cliente_form.fields['obligado_comprobante_domicilio'].widget.attrs['data-placeholder'] = 'No se ha agregado'
            cliente_form.fields['obligado_comprobante_domicilio'].widget.attrs['data-buttonText'] = 'Seleccionar archivo'

            args = {'cliente_form': cliente_form, 'authenticated': request.user.is_authenticated()}
            return render(request, 'clientes/edit_profile.html', args)


@login_required(login_url='clientes:acceso')
def cambiar_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('/clientes/perfil')
        else:
            return redirect('clientes/cambiar-contrasena')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form, 'authenticated': request.user.is_authenticated()}
        return render(request, 'clientes/change_password.html', args)


def gracias(request):
    thanks = False
    thanks = request.session.pop('thanks', None)
    if not thanks:
        return redirect('/')
    else:
        return render(request, 'clientes/gracias.html', {'authenticated': request.user.is_authenticated()})


def _get_pin(length=5):
    """ Return a numeric PIN with length digits """
    return random.sample(range(10**(length-1), 10**length), 1)[0]


def _verify_pin(mobile_number, pin):
    """ Verify a PIN is correct """
    return pin == cache.get(mobile_number)


def ajax_send_pin(request):
    """ Sends SMS PIN to the specified number """
    authorized = request.POST.get('authorized', "")
    if authorized != "True":
        return redirect('/')
    mobile_number = request.POST.get('mobile_number', "")
    if not mobile_number:
        return HttpResponse("Error: No se recibió número", content_type='text/plain', status=403)

    pin = _get_pin()
    # store the PIN in the cache for later verification.
    cache.set(mobile_number, pin, 300)  # válido por 5 minutos
    caches['default'].set(mobile_number, pin, 300)
    client = Client(config('TWILIO_ACCOUNT_SID'), config('TWILIO_AUTH_TOKEN'))
    message = client.api.messages.create(
                        body="Tu NIP de verificación Eco Luz es %s" % pin,
                        to=mobile_number,
                        from_=config('TWILIO_FROM_NUMBER'),
                    )
    return HttpResponse("Mensaje %s enviado" % message.sid, content_type='text/plain', status=200)


def autorizar_consulta_sic(request):
    """ Process web form verified by SMS PIN. """
    authorized = request.session.pop('authorized', None)
    if not authorized:
        return redirect('/')
    # else:
    form_class = PINForm
    if request.method == 'POST':
        form = form_class(data=request.POST)
        cliente_actual = get_object_or_404(Cliente, pk=request.user.id)
        if form.is_valid():
            pin = request.POST.get('pin')
            num = '+521' + request.POST.get('numero_cel')
            vpin = cache.get(num)
            if str(pin) == str(vpin): #_verify_pin(num, pin):
                cliente_actual.autorizacion_buro_credito = True
                cliente_actual.fecha_autorizacion_de_consulta = date.today()
                cliente_actual.hora_autorizacion_de_consulta = datetime.now().strftime("%X")
                cliente_actual.save()
                request.session['thanks'] = True
                return HttpResponseRedirect(reverse('clientes:gracias'))
            else:
                messages.error(request, "NIP Incorrecto")
        else:
            form = PINForm(request.GET or None)
            form.fields['numero_cel'].widget.attrs['placeholder'] = 'Celular a 10 dígitos'
            #form.fields.widget.attrs['pattern'] = '^[0-9]+'
    else:
        form = form_class()
    return render(request, 'clientes/autorizacion_consulta_sic.html', {'form': form}, {'authenticated': request.user.is_authenticated()})
