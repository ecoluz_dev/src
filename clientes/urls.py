# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.views import login, logout

from . import views
app_name = 'clientes'
urlpatterns = [

    url(r'^acceso/$', login, {'template_name': 'clientes/login.html'}, name='acceso'),
    url(r'^adios/$', logout, {'template_name': 'clientes/logout.html'}, name='adios'),
    url(r'^registro/$', views.registro, name='registro'),
    url(r'^perfil/$', views.perfil, name='perfil'),
    url(r'^perfil/editar/$', views.editar_perfil, name='editar_perfil'),
    url(r'^cambiar-contrasena/$', views.cambiar_password, name='cambiar_contrasena'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    url(r'^autorizar-consulta-sic/$', views.autorizar_consulta_sic, name='autorizar-consulta-sic'),
    url(r'^gracias/$', views.gracias, name='gracias'),
    url(r'^ajax_send_pin/$', views.ajax_send_pin, name='send_pin'),
]
