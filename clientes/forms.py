
# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from clientes.models import Cliente
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Button
from crispy_forms.bootstrap import TabHolder, Tab
from django.utils.translation import gettext_lazy as _


class FormaDeRegistro(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Campo obligatorio.', label="Nombre")
    last_name = forms.CharField(max_length=30, required=True, help_text='Campo obligatorio.', label="Apellidos")
    email = forms.EmailField(max_length=254,required=True, help_text='Ingrese su dirección de correo electrónico.')
    terms = forms.BooleanField(
            error_messages={'required': 'Debes aceptar los términos y condiciones'},
            label="Acepto los términos y condiciones"
            )

    class Meta:
        model = Cliente
        fields = (
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
            'terms'
        )

    # def save(self, commit=True):
    #     user = super(FormaDeRegistro, self).save(commit=False)
    #     user.email = self.cleaned_data['email']
    #     if commit:
    #         user.save()
    #     return user


class FormaEditarUser(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = ('first_name', 'last_name', 'email')




class FormaEditarPerfil(forms.ModelForm):
    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        TabHolder(
            Tab(
                'Mis datos',
                Field('first_name', css_class='form-control-white'),
                Field('last_name', css_class='form-control-white'),
                'foto_file',
                Field('telefono', css_class='form-control-white'),
                Field('estado_civil', css_class='form-control-white'),
                Field('regimen_conyugal', css_class='form-control-white'),
                Field('calle', css_class='form-control-white'),
                Field('numero_exterior', css_class='form-control-white'),
                Field('numero_interior', css_class='form-control-white'),
                Field('colonia', css_class='form-control-white'),
                Field('delegacion_municipio', css_class='form-control-white'),
                Field('ciudad', css_class='form-control-white'),
                Field('estado', css_class='form-control-white'),
                Field('codigo_postal', css_class='form-control-white'),
                Field('rfc', css_class='form-control-white', pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z0-9]{3}$", title="Debe escribir un RFC con el formato válido."),
                Field('razon_social', css_class='form-control-white'),
                Field('domicilio_fiscal', css_class='form-control-white'),
                Field('curp', css_class='form-control-white', pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z]{5}[a-zA-Z0-9]{3}$", title="Debe escribir una CURP con el formato válido."),
            # Documentos del cliente
                'identificacion_file',
                'curp_file',
                'cedula_rfc_file',
                'recibo_luz_file',
                'comprobante_ingresos_file',
                'comprobante_domicilio',
                Field('perfil_completo', type="hidden"),
                Button('SIGUIENTE', 'SIGUIENTE', css_class="btnNext btn btn-flat btn-success"),
            ),
            Tab(
                'Datos del obligado solidario',
                # Datos y documentos del obligado solidario
                Field('obligado_nombre', css_class='form-control-white'),
                Field('obligado_apellido', css_class='form-control-white'),
                Field('obligado_relacion', css_class='form-control-white'),
                Field('obligado_rfc', css_class='form-control-white', pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z0-9]{3}$", title="Debe escribir un RFC con el formato válido."),
                Field('obligado_curp', css_class='form-control-white', pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z]{5}[a-zA-Z0-9]{3}$", title="Debe escribir una CURP con el formato válido."),
                'obligado_identificacion_file',
                'obligado_rfc_file',
                'obligado_curp_file',
                'obligado_recibo_luz',
                'obligado_comprobante_domicilio',
                Button('ANTERIOR', 'ANTERIOR', css_class="btnPrevious btn btn-flat btn-success"),
            )
        )
    )

    class Meta:
        model = Cliente
        fields = [
            'first_name',
            'last_name',
            'foto_file',
            'telefono',
            'estado_civil',
            'regimen_conyugal',
            'rfc',
            'razon_social',
            'domicilio_fiscal',
            'calle',
            'numero_exterior',
            'numero_interior',
            'colonia',
            'delegacion_municipio',
            'ciudad',
            'estado',
            'codigo_postal',
            'curp',
            # Documentos del cliente
            'identificacion_file',
            'curp_file',
            'cedula_rfc_file',
            'recibo_luz_file',
            'comprobante_ingresos_file',
            'comprobante_domicilio',
            # Datos y documentos del obligado solidario
            'obligado_nombre',
            'obligado_apellido',
            'obligado_relacion',
            'obligado_rfc',
            'obligado_curp',
            'obligado_identificacion_file',
            'obligado_rfc_file',
            'obligado_curp_file',
            'obligado_recibo_luz',
            'obligado_comprobante_domicilio',
            'perfil_completo',
        ]


class FormaVerPerfil(forms.ModelForm):
    helper = FormHelper()
    helper.form_tag = False
    helper.layout = Layout(
        TabHolder(
            Tab(
                'Mis datos',
                'first_name',
                'last_name',
                'foto_file',
                'telefono',
                'rfc',
                'razon_social',
                'domicilio_fiscal',
                'curp',
                # Documentos del cliente
                'identificacion_file',
                'curp_file',
                'cedula_rfc_file',
                'recibo_luz_file',
                'comprobante_ingresos_file',
                'comprobante_domicilio',
            ),
            Tab(
                'Datos del obligado solidario',
                # Datos y documentos del obligado solidario
                'obligado_nombre',
                'obligado_apellido',
                'obligado_relacion',
                'obligado_rfc',
                'obligado_curp',
                'obligado_identificacion_file',
                'obligado_rfc_file',
                'obligado_curp_file',
                'obligado_recibo_luz',
                'obligado_comprobante_domicilio',
            ),
            Tab(
                'Mi financiamiento EcoLuz',
                'estado_del_financiamiento',
                'plan_elegido',
                'monto_credito',
                'numero_de_paneles',
                'enganche',
                'numero_de_pagos',
                'pago_mensual',
            )
        )
    )

    class Meta:
        model = Cliente
        fields = [
            'first_name',
            'last_name',
            'foto_file',
            'telefono',
            'rfc',
            'razon_social',
            'domicilio_fiscal',
            'curp',
            # Documentos del cliente
            'identificacion_file',
            'curp_file',
            'cedula_rfc_file',
            'recibo_luz_file',
            'comprobante_ingresos_file',
            'comprobante_domicilio',
            # Datos y documentos del obligado solidario
            'obligado_nombre',
            'obligado_apellido',
            'obligado_relacion',
            'obligado_rfc',
            'obligado_curp',
            'obligado_identificacion_file',
            'obligado_rfc_file',
            'obligado_curp_file',
            'obligado_recibo_luz',
            'obligado_comprobante_domicilio',
            'estado_del_financiamiento',
            'plan_elegido',
            'monto_credito',
            'numero_de_paneles',
            'enganche',
            'numero_de_pagos',
            'pago_mensual',
        ]


class PINForm(forms.Form):
    autorizar_consulta_sic = forms.BooleanField(
            error_messages={'required': 'Debes dar tu autorización e ingresar el NIP para poder continuar.'},
            label="Estoy de acuerdo y doy mi autorización"
            )
    pin = forms.IntegerField(label=_("PIN"), required=True, widget=forms.TextInput())
    numero_cel = forms.CharField(label=_("Celular a 10 dígitos"), required=True)
