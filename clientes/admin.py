# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
# from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from .models import Cliente


#from clientes.models import Cliente
# Register your models here.

# class ProfileInline(admin.StackedInline):
#     model = Cliente
#     can_delete = False
#     verbose_name_plural = 'Cliente'
#     fk_name = 'user'
#
# class CustomUserAdmin(UserAdmin):
#     inlines = (ProfileInline, )
#
#     def get_inline_instances(self, request, obj=None):
#         if not obj:
#             return list()
#         return super(CustomUserAdmin, self).get_inline_instances(request, obj)


#admin.site.register(Cliente)
#admin.site.unregister()
#admin.site.register(User, CustomUserAdmin)


@admin.register(Cliente)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom Cliente model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Datos del cliente'), {'fields': ('first_name', 'last_name',
            'telefono',
            'foto_file',
            'calle',
            'numero_exterior',
            'numero_interior',
            'colonia',
            'delegacion_municipio',
            'ciudad',
            'estado',
            'codigo_postal',
            'estado_civil',
            'regimen_conyugal',
            'rfc',
            'razon_social',
            'domicilio_fiscal',
            'curp',
            'identificacion_file',
            'curp_file',
            'cedula_rfc_file',
            'recibo_luz_file',
            'comprobante_ingresos_file',
            'comprobante_domicilio',
            'terms',
            'mail_confirmed',
            'perfil_completo',
            'tipo_usuario'
                                             )}),
        (_('Datos de autorización de consulta de buró de crédito'), {'fields': ('autorizacion_buro_credito',
                                                                                'fecha_autorizacion_de_consulta',
                                                                                'hora_autorizacion_de_consulta',
                                                                                'fecha_de_consulta_buro',
                                                                                'hora_de_consulta_buro')}),
        (_('Obligado solidario'), {'fields': ('obligado_nombre',
                                              'obligado_apellido',
                                              'obligado_relacion',
                                              'obligado_rfc',
                                              'obligado_curp',
                                              'obligado_identificacion_file',
                                              'obligado_rfc_file',
                                              'obligado_curp_file',
                                              'obligado_recibo_luz',
                                              'obligado_comprobante_domicilio')}),
        (_('Datos del financiamiento'), {'fields': ('plan_elegido', 'monto_credito', 'numero_de_paneles', 'enganche', 'numero_de_pagos', 'pago_mensual', 'estado_del_financiamiento')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'first_name', 'last_name', 'telefono', 'estado_del_financiamiento', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name', 'telefono')
    ordering = ('email',)