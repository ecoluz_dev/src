$(document).ready(function () {

    $("#profile").on("submit", function (e) {
        //e.preventDefault();
        $file_field_is_empty = false ;
        $field_is_empty = false;

        $('.controls').each(function() {
            $a = $(this).find('a');
            $txt = $($a).text();
            if ($txt == ""){
                $obj = $(this).find('input');
                if(typeof($obj[0]) != 'undefined'){
                    if ($obj[0].type == 'file'){
                        if($obj[0].files.length == 0){
                            $file_field_is_empty = true;
                        }
                    }
                }
            }
        }) ;

        $('input:text').each(function() {
            $obj = $(this).val();
            if ($obj == ""){
                $field_is_empty = true;
            }
        });

        $('input[type=checkbox]').each(function() {
        var len = $('input[type=checkbox]:checked').length;
            if (len > 0) {
                $file_field_is_empty = true;
            }
        });

        if ($field_is_empty || $file_field_is_empty) {
            document.getElementById("id_perfil_completo").value = "False";
        }
        else {
            document.getElementById("id_perfil_completo").value = "True";
        }
    });

});