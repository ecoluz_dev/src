from ecoclientes.models import Inventario, Eco

lista_eqps = [('AIRE ACONDICIONADO TIPO MINISPLIT INVERTER SOLO FRIO, CAP. 1 TR', 'AIRE ACONDICIONADO TIPO MINISPLIT INVERTER SOLO FRIO, CAP. 1 TR'),
        ('ASW-12A2INV/SS', 'ASW-12A2INV/SS'),
        ('ASW-12A2INV/SS WI-FI', 'ASW-12A2INV/SS WI-FI'),
        ('ASW-18B2INV/SS WI-FI', 'ASW-18B2INV/SS WI-FI'),
        ('ASW-24B2INV/SS', 'ASW-24B2INV/SS'),
        ('ASW-H12A2INV/SS', 'ASW-H12A2INV/SS'),
        ('ASW-H12A2INV/SS WI-FI', 'ASW-H12A2INV/SS WI-FI'),
        ('ASW-H18B2INV/SS', 'ASW-H18B2INV/SS'),
        ('ASW-H18B2INV/SS WI-FI', 'ASW-H18B2INV/SS WI-FI'),
        ('ASW-H24B2INV/SS', 'ASW-H24B2INV/SS'),
        ('AXOL CONCEPT 150', 'AXOL CONCEPT 150'),
        ('BHS20-F2 E1C115BSN', 'BHS20-F2 E1C115BSN'),
        ('BHS30-F2 E1C115BSN', 'BHS30-F2 E1C115BSN'),
        ('CAT-05', 'CAT-05'),
        ('CBH-060', 'CBH-060'),
        ('CBH-150', 'CBH-150'),
        ('CCH-08', 'CCH-08'),
        ('CCH-15', 'CCH-15'),
        ('CFC-6P 216', 'CFC-6P 216'),
        ('CFC-6P080', 'CFC-6P080'),
        ('CFX-08', 'CFX-08'),
        ('CFX-17', 'CFX-17'),
        ('CFX-19', 'CFX-19'),
        ('CFX-24', 'CFX-24'),
        ('CFX-24 2P', 'CFX-24 2P'),
        ('CFX-30 3P', 'CFX-30 3P'),
        ('CFX-37 2P', 'CFX-37 2P'),
        ('CFX-37 4P', 'CFX-37 4P'),
        ('CFX-42 2P', 'CFX-42 2P'),
        ('CFX-42 4P', 'CFX-42 4P'),
        ('CFX-64 3P', 'CFX-64 3P'),
        ('CFX-SL', 'CFX-SL'),
        ('CSC-4M 216', 'CSC-4M 216'),
        ('CTCC-10', 'CTCC-10'),
        ('CTCC-15', 'CTCC-15'),
        ('CTCC-15 AI', 'CTCC-15 AI'),
        ('CTCC-25', 'CTCC-25'),
        ('CTCC-25 AI', 'CTCC-25 AI'),
        ('G342 D BMAD 3PC', 'G342 D BMAD 3PC'),
        ('LED SUPERSTAR CLASSIC A60 8.5W/865 G2', 'LED SUPERSTAR CLASSIC A60 8.5W/865 G2'),
        ('OSEL PLATA 1622', 'OSEL PLATA 1622'),
        ('PR-GB-APV-2X18-T8-LED', 'PR-GB-APV-2X18-T8-LED'),
        ('PR-GB-APV-2X54-T5', 'PR-GB-APV-2X54-T5'),
        ('PR-GB-CNL-2X28-T5', 'PR-GB-CNL-2X28-T5'),
        ('PR-LED-A19-10W-6500K-127V-E26-ECO-LB', 'PR-LED-A19-10W-6500K-127V-E26-ECO-LB'),
        ('PR-LED-CAN-100W-6000K-90-305V-EMP', 'PR-LED-CAN-100W-6000K-90-305V-EMP'),
        ('PR-LED-DWL-8W-4000K-100-277V-ECO', 'PR-LED-DWL-8W-4000K-100-277V-ECO'),
        ('PR-LED-DWL-8W-4000K-100-277V-UNI', 'PR-LED-DWL-8W-4000K-100-277V-UNI'),
        ('PR-LED-PAN-SLM-36W-6500-100-277V-60X60', 'PR-LED-PAN-SLM-36W-6500-100-277V-60X60'),
        ('PR-LED-SUB-37W-6500K-100-240V-FTC', 'PR-LED-SUB-37W-6500K-100-240V-FTC'),
        ('PR-REF-FL-150W-6500K-100-240V-ECO', 'PR-REF-FL-150W-6500K-100-240V-ECO'),
        ('PR-T8-LED 18W 6500K-100-277V-FR GL', 'PR-T8-LED 18W 6500K-100-277V-FR GL'),
        ('Pintura Emulsionada Hipoteca Verde SC 2691', 'Pintura Emulsionada Hipoteca Verde SC 2691'),
        ('Refacciones', 'Refacciones'),
        ('Sistema Solar Eco Luz 8', 'Sistema Solar Eco Luz 8'),
        ('THERMOSTAT CONTROL', 'THERMOSTAT CONTROL'),
        ('VCC-120', 'VCC-120'),
        ('VCC-200', 'VCC-200'),
        ('VCR-200', 'VCR-200'),
        ('VFS 24', 'VFS 24'),
        ('VFS16 C BMAB R4', 'VFS16 C BMAB R4'),
        ('VFS16C', 'VFS16C'),
        ('VFS24 C BMAD R4', 'VFS24 C BMAD R4'),
        ('VIT VLH17 C BMAE EST', 'VIT VLH17 C BMAE EST'),
        ('VL140 D BMAD', 'VL140 D BMAD'),
        ('VLH 20', 'VLH 20'),
        ('VLH20 C BMAE', 'VLH20 C BMAE'),
        ('VR 35', 'VR 35'),
        ('VR26 D BMAD', 'VR26 D BMAD'),
        ('VR35 4PC', 'VR35 4PC'),
        ('VR35 D BMAD 4PC', 'VR35 D BMAD 4PC'),
        ('VL480 C BMAD','VL480 C BMAD'),
        ('VL735 C BMAD','VL735 C BMAD')]

for x in lista_eqps:
    nombre = x[0]
    Inventario.objects.create(modelo=nombre)



contados = Eco.objects.filter(tipo_venta__icontains='contado')

a = 1
for k in contados:
    setattr(k, 'numero_credito',  'CC' + '-' + "%04d" % (a,))
    k.save()
    a += 1



pcs = Eco.objects.filter(tipo_venta__icontains='provecapital')

num = 1
for k in pcs:
    setattr(k, 'numero_credito',  'PC' + '-' + "%04d" % (num,))
    k.save()
    num += 1