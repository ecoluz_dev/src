from django.core.management.base import BaseCommand, CommandError
from ecoclientes.models import Eco, Equipo
from ecoclientes.views import actualiza_fecha

class Actualiza(BaseCommand):
    help = 'Actualiza el numero de dias de cada folio'

    def actual(self):
        actualiza_fecha()
        print "actualice fecha"