# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect, HttpResponse, FileResponse, Http404
from django import forms
from ecoclientes.forms import RegistroEco, EditarRegistroEco, EditarDescuentos, PreRegistroEco, NuevaPlaza, PreRegistroEquipos, DatosFiscales, NuevoModelo, EditarEquipos, EditarInversionista
import datetime
import pytz
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .models import Eco, Facturas, Concepto, Equipo, Plaza, Inventario, Inversionista
import xml.etree.ElementTree as ET
import unidecode
import re
from re import sub
from string import letters
from bots.views import initiate_webdriver, fide_login, consulta_datos_credito
from django.contrib.auth.decorators import login_required
import requests
from requests.auth import HTTPBasicAuth
import json
import services
import base64
from django.contrib import messages
from django.core.files import File
import collections
from django.db.models import Count, Sum, Avg, Q
import pandas as pd
from celery.schedules import crontab
from celery.task import periodic_task
import pytz
from django.contrib.admin.views.decorators import staff_member_required

utc=pytz.UTC

from decimal import *
from decimal import Decimal as D


# Create your views here.

def nombre(self):
    return ''.join([self.last_name, ' ', self.last_last_name, ', ', self.first_name])  # Atributos de Cliente

def genera_fxf(self):
    mismos = Eco.objects.filter(DAR=self.DAR)
    nums = []
    for k in mismos:
        if k.fxf:
            st = str(k.fxf[8:])
            if st[0]==0:
                st= str(k.fxf[9:])
            cc = st.translate(None, letters)
            nums += [float(cc)]
    if len(nums) > 0:
        a = max(nums) + 1
        a = int(a)
    else:
        a = 1
    print nums
    return self.DAR + '-' + "%04d" % (a,)


def genera_num_credito(self):
    mismos = Eco.objects.filter(numero_credito=self.numero_credito)
    nums = []
    for k in mismos:
        if k.numero_credito:
            st = str(k.numero_credito[3:])
            cc = st.translate(None, letters)
            nums += [float(cc)]
    if len(nums) > 0:
        a = max(nums) + 1
        a = int(a)
    else:
        a = 1
    if self.tipo_venta == 'Credijusto':
        pre = 'CJ'
    elif self.tipo_venta == 'ProveCapital':
        pre = 'PC'
    elif self.tipo_venta == 'Afirme':
        pre = 'AF'
    elif self.tipo_venta == 'Contado':
        pre = 'CC'
    elif self.tipo_venta == 'CONAVI':
        pre = 'CO'
    return pre + '-' + "%04d" % (a,)



@staff_member_required
def nuevo_registro(request):
    if request.method == 'POST':
        eco_registro = RegistroEco(request.POST )
        if eco_registro.is_valid():
            eco_registro = eco_registro.save(commit=False)
            setattr(eco_registro, 'DAR', request.session.pop('dar'))
            setattr(eco_registro, 'tipo_venta', request.session.pop('tipoventa'))
            setattr(eco_registro, 'fxf', genera_fxf(eco_registro))
            setattr(eco_registro, 'numero_credito', genera_num_credito(eco_registro))
            setattr(eco_registro, 'estatus', 'EN VALIDACIÓN')
            eco_registro.save()
            request.session['fxf'] = eco_registro.fxf
            nvo_equipo = PreRegistroEquipos()
            return render(request, 'ecoclientes/nuevo_equipo.html' , {'nvo_equipo': nvo_equipo} )
    else:
        eco_registro = RegistroEco(request.GET or None)
    return render(request, 'ecoclientes/nuevo_registro.html', {'eco_registro': eco_registro})


@staff_member_required
def preregistro(request):
    # try:
    actualiza_fecha()
    # except:
    #     print 'No se pudo actualizar fecha'
    if request.method == 'POST':
        prereg = PreRegistroEco(request.POST)
        if prereg.is_valid():
            prereg = prereg.save(commit=False)
            if prereg.tipo_venta == 'PAEEEM':
                driver = initiate_webdriver()
                fide_login(driver)
                data = consulta_datos_credito(driver, prereg.numero_credito)
                datos = data['datos_solicitante']
                equiposip = data['equipos']
                numeqp = 0
                for equipo in equiposip:
                    numeqp += int(equiposip[equipo]['cantidad'])
                presupuesto = data['presupuesto']
                dom_fis = data['domicilio_fiscal']
                contacto = data['contacto']
                setattr(prereg, 'fxf', genera_fxf(prereg))
                try:
                    setattr(prereg, 'first_name', datos['nombre'])
                    setattr(prereg, 'last_name', datos['apellido1'])
                    setattr(prereg, 'last_last_name', datos['apellido2'])
                    setattr(prereg, 'nombre_beneficiario', nombre(prereg))
                except:
                    setattr(prereg, 'nombre_beneficiario', datos['razon_social'])
                setattr(prereg, 'cayd_cobrado', float(sub(r'[^\d.]', '', presupuesto['cayd'])))
                setattr(prereg, 'estatus', 'EN VALIDACIÓN')
                setattr(prereg, 'rfc', datos['rfc'])
                if 'NOHAY' not in contacto['email']:
                    setattr(prereg, 'email', contacto['email'])
                if 'AÑADIR' not in contacto['telefono']:
                    setattr(prereg, 'telefono', contacto['telefono'])
                if 'AÑADIR VALOR' not in dom_fis['calle']:
                    setattr(prereg, 'calle', dom_fis['calle'])
                if 'AÑADIR' not in dom_fis['num_ext']:
                    setattr(prereg, 'numero_exterior', dom_fis['num_ext'])
                if 'AÑADIR' not in dom_fis['num_int']:
                    setattr(prereg, 'numero_interior', dom_fis['num_int'])
                if 'AÑADIR VALOR' not in dom_fis['colonia']:
                    setattr(prereg, 'colonia ', dom_fis['colonia'])
                if 'AÑADIR VALOR' not in dom_fis['deleg_municipio']:
                    setattr(prereg, 'delegacion_municipio', dom_fis['deleg_municipio'])
                if 'AÑADIR VALOR' not in dom_fis['estado']:
                    setattr(prereg, 'estado', dom_fis['estado'])
                setattr(prereg, 'codigo_postal', dom_fis['cp'])
                prereg.save()
                for equipo in equiposip:
                    eqp = Equipo(fxf_eqp=prereg)
                    invent, created = Inventario.objects.get_or_create(modelo=equiposip[equipo]['modelo'])
                    if created:
                        setattr(invent, 'modelo', equiposip[equipo]['modelo'])
                        setattr(invent, 'precio_lista', float(sub(r'[^\d.]', '', equiposip[equipo]['precio_unitario'])))
                        setattr(invent, 'fabricante', equiposip[equipo]['marca'])
                        setattr(invent, 'descripcion', equiposip[equipo]['tipo_producto'])
                        invent.save()
                    else:
                        print 'ya existia'
                        if invent.precio_lista is None:
                            print 'no tiene precio'
                            setattr(invent, 'precio_lista', float(sub(r'[^\d.]', '', equiposip[equipo]['precio_unitario'])))
                        if invent.fabricante is None:
                            setattr(invent, 'fabricante', equiposip[equipo]['marca'])
                        if invent.descripcion is None:
                            setattr(invent, 'descripcion', equiposip[equipo]['tipo_producto'])
                        invent.save()
                    setattr(eqp, 'modelo_equipo', equiposip[equipo]['modelo'])
                    setattr(eqp, 'modelo_eqp', invent)
                    setattr(eqp, 'cantidad', equiposip[equipo]['cantidad'])
                    setattr(eqp, 'subtotal_equipo', float(sub(r'[^\d.]', '', equiposip[equipo]['precio_unitario']))*float(equiposip[equipo]['cantidad'])*1.16)
                    setattr(eqp, 'cayd_cobrado_e', float(prereg.cayd_cobrado)**float(equiposip[equipo]['cantidad'])/numeqp)
                    setattr(eqp, 'cayd_pagado_e', float(prereg.cayd_cobrado)**float(equiposip[equipo]['cantidad'])/numeqp)
                    setattr(eqp, 'gastos_inst', float(sub(r'[^\d.]', '', equiposip[equipo]['gastos_instalacion'])))
                    setattr(eqp, 'com_e', float(sub(r'[^\d.]', '', equiposip[equipo]['precio_unitario']))*float(equiposip[equipo]['cantidad'])*1.16*0.1)
                    eqp.save()
                return redirect('ecoclientes:ver_registro', prereg.fxf)
            else:
                request.session['tipoventa'] = prereg.tipo_venta
                request.session['dar'] = prereg.DAR
                return redirect(reverse('ecoclientes:nuevo_registro'))
    else:
        prereg = PreRegistroEco(request.GET or None)
    return render(request, 'ecoclientes/preregistro.html', {'prereg': prereg})


@staff_member_required
def nuevo_equipo(request):
    if request.method == 'POST':
        fxf = request.session.pop('fxf')
        a = Eco.objects.get(fxf=fxf)
        nvo_equipo = PreRegistroEquipos(request.POST )
        if nvo_equipo.is_valid():
            bbb = nvo_equipo.save(commit=False)
            bbb.fxf_eqp = a
            bbb.save()
            request.session['fxf'] = fxf
            if 'enviar' in request.POST:
                return redirect('ecoclientes:ver_registro', a.fxf)
            elif 'registrar_otro' in request.POST:
                nvo_equipo = PreRegistroEquipos()
                return render(request, 'ecoclientes/nuevo_equipo.html', {'nvo_equipo': nvo_equipo} )
    else:
        nvo_equipo = PreRegistroEquipos(request.GET or None)
    return render(request, 'ecoclientes/nuevo_equipo.html', {'nvo_equipo': nvo_equipo})



def calcula_pago_total(self):
    return self.subtotal_equipo + self.cayd_cobrado_e + float(self.gastos_inst)

def calcula_costo_total_financiamiento(self):
    return self.subtotal_operacion_e + self.financiamiento_e

def calcula_com_dar(self):
    if self.fxf_eqp.estatus == 'CANCELADO':
        if self.pago_total_e > self.costo_total_financiamiento_e:
            aa = (-1)*float(self.pago_total_e - self.costo_total_financiamiento_e)  # AVISA SI ES NEGATIVA!!
        else:
            aa = self.pago_total_e - self.costo_total_financiamiento_e  # AVISA SI ES NEGATIVA!!
    else:
        aa = self.pago_total_e - self.costo_total_financiamiento_e  # AVISA SI ES NEGATIVA!!
    return aa


def calcular_com_solar(self):
    sum_costotal = 0
    for eqp in self.equipo_set.all():
        sum_costotal += eqp.costo_total_financiamiento_e
    return self.pago_total - sum_costotal


def calcula_numero_dias(self):
    delta = self.fecha_pago - self.fecha_compra
    return delta.days

def calcula_numero_dias_e(self):
    delta = self.fxf_eqp.fecha_pago - self.fecha_compra_e
    return delta.days

def nombre(self):
    return ''.join([self.last_name, ' ', self.last_last_name, ', ', self.first_name])  # Atributos de Cliente

def calcula_financiamiento(self):
    if self.fxf_eqp.numero_dias < 31:
        return self.subtotal_operacion_e * 0.11
    elif self.fxf_eqp.numero_dias < 52:
        return self.subtotal_operacion_e * 0.187
    elif self.fxf_eqp.numero_dias < 61:
        return self.subtotal_operacion_e * 0.22
    else:
        return self.subtotal_operacion_e * (0.22 + 0.0014 * (self.fxf_eqp.numero_dias - 60))

def calcula_financiamiento_e(self):
    if self.numero_dias_e < 31:
        return self.subtotal_operacion_e * 0.11
    elif self.numero_dias_e < 52:
        return self.subtotal_operacion_e * 0.187
    elif self.numero_dias_e < 61:
        return self.subtotal_operacion_e * 0.22
    else:
        return self.subtotal_operacion_e * (0.22 + 0.0014 * (self.numero_dias_e - 60))






def asigna_totales(eco):
    equipos = Equipo.objects.filter(fxf_eqp=eco)
    sp = 0.
    cc = 0.
    eq = 0.
    mon = 0.
    mnt = 0.
    sumi = 0.
    inve = 0.
    micr = 0.
    rg = 0.
    md = 0.
    co = 0.
    ma = 0.
    cp = 0.
    so = 0.
    fi = 0.
    cf = 0.
    pt = 0.
    cd = 0.
    ad = 0.
    dc = 0.
    gi = 0.
    for equipo in equipos:
        sp += float(str(equipo.subtotal_equipo))
        cc += float(str(equipo.cayd_cobrado_e))
        eq += float(equipo.eqp_e)
        try:
            mon += float(equipo.monitoreo_e)
            mnt += float(equipo.montaje_e)
            sumi += float(equipo.suministro_instalacion_e)
            inve += float(equipo.inversor_e)
            micr += float(equipo.microinversor_e)
        except:
            pass
        rg += float(equipo.reguladores_e)
        md += float(equipo.mdm_e)
        co += float(equipo.com_e)
        ma += float(equipo.man_e)
        cp += float(equipo.cayd_pagado_e)
        so += float(equipo.subtotal_operacion_e)
        fi += float(equipo.financiamiento_e)
        cf += float(equipo.costo_total_financiamiento_e)
        pt += float(equipo.pago_total_e)
        cd += float(equipo.com_dar_e)
        if equipo.descuentos:
            dc = dc + float(equipo.descuentos)
        if equipo.gastos_inst:
            gi += float(equipo.gastos_inst)
        if equipo.adeudos_eq:
            ad = ad + float(equipo.adeudos_eq)
    setattr(eco, 'subtotal_pago', sp)
    setattr(eco, 'cayd_cobrado', cc)
    setattr(eco, 'eqp', eq)
    setattr(eco, 'monitoreo', mon)
    setattr(eco, 'montaje', mnt)
    setattr(eco, 'suministro_instalacion', sumi)
    setattr(eco, 'inversor', inve)
    setattr(eco, 'microinversor', micr)
    setattr(eco, 'reguladores', rg)
    setattr(eco, 'mdm', md)
    setattr(eco, 'com', co)
    setattr(eco, 'man', ma)
    setattr(eco, 'cayd_pagado', cp)
    setattr(eco, 'subtotal_operacion', so)
    setattr(eco, 'financiamiento', fi)
    setattr(eco, 'costo_total_financiamiento', cf)
    setattr(eco, 'pago_total', pt)
    if not eco.descuento_f:
        setattr(eco, 'descuento_f',  dc)
    setattr(eco, 'gastos_inst', gi )
    if eco.solar:
        setattr(eco, 'com_dar', calcular_com_solar(eco))
        if calcular_com_solar(eco) < 0:
            setattr(eco, 'com_dar', calcular_com_solar(eco))
            if eco.descuento_f <0:
                setattr(eco, 'adeudos', (-1)*calcular_com_solar(eco) + eco.descuento_f)
            else:
                setattr(eco, 'adeudos', (-1)*calcular_com_solar(eco) )

        else:
            if ad > 0:
                setattr(eco, 'com_dar', calcular_com_solar(eco) - ad - eco.descuento_f )
            else:
                setattr(eco, 'com_dar', calcular_com_solar(eco) - eco.descuento_f)
    else:
        if cd < 0:
            ad += (-1)*cd
            # setattr(eco, 'adeudos', ad + (-1) * cd )
            setattr(eco, 'com_dar', cd)
            if eco.descuento_f <0:
                setattr(eco, 'adeudos', ad + eco.descuento_f)
            else:
                setattr(eco, 'adeudos', ad )

        else:
            if ad > 0:
                setattr(eco, 'com_dar', cd - ad - eco.descuento_f )
            else:
                setattr(eco, 'com_dar', cd - eco.descuento_f)
    eco.save()


def actualiza_fecha():
    todos = Eco.objects.exclude(estatus='PAGADO')

    for eco in todos:
        try:
            setattr(eco, 'fecha_pago', datetime.date.today())
            setattr(eco, 'numero_dias', calcula_numero_dias(eco))
            eco.save()
        except:
            print 'Falta fecha en ' + str(eco)
        for equipo in Equipo.objects.filter(fxf_eqp=eco):
            try:
                setattr(equipo, 'financiamiento_e', calcula_financiamiento_e(equipo))
                setattr(equipo, 'pago_total_e', calcula_pago_total(equipo))
                setattr(equipo, 'costo_total_financiamiento_e', calcula_costo_total_financiamiento(equipo))
                setattr(equipo, 'com_dar_e', calcula_com_dar(equipo))
                setattr(equipo, 'numero_dias_e', calcula_numero_dias_e(equipo))
                equipo.save()
            except:
                print 'no pude actualizar datos ' + str(equipo)
            try:
                asigna_totales(eco)
            except:
                print 'No se asignar totales ' +  str(eco)
        if eco.solar:
            setattr(eco, 'com_dar', calcular_com_solar(eco))
            eco.save()


@periodic_task(run_every=crontab(hour=22, minute=03,))
def every_morning():
    actualiza_fecha()
    print "actualicé fecha"



@staff_member_required
def editar_equipo(request, pk):
    equipo = Equipo.objects.get(pk=pk)
    vienede = equipo.fxf_eqp
    request.session['pk'] = pk
    if request.method == 'POST':
        form = EditarEquipos(request.POST, instance=equipo)
        if form.is_valid():
            equipo = form.save()
            if not equipo.independiente and equipo.fxf_eqp.fecha_compra:
                setattr(equipo, 'fecha_compra_e', equipo.fxf_eqp.fecha_compra)
                setattr(equipo, 'numero_dias_e', equipo.fxf_eqp.numero_dias)
                equipo.save()
            try:
                sbttop = float(equipo.eqp_e) + float(equipo.reguladores_e) + float(equipo.mdm_e) + float(equipo.com_e) \
                         + float(equipo.man_e) + float(equipo.cayd_pagado_e)
                try:
                    sbttop += equipo.montaje_e + equipo.monitoreo_e + equipo.suministro_instalacion_e + \
                              equipo.estructura_e + equipo.inversor_e + equipo.microinversor_e
                except:
                    pass
                if equipo.fxf_eqp.fxf[:3] == 'PRF' and equipo.fxf_eqp.fecha_creacion.replace(tzinfo=utc) < datetime.datetime(2017,12,14, 0,1).replace(tzinfo=utc):
                    0
                else:
                    setattr(equipo, 'subtotal_operacion_e', sbttop)
                    setattr(equipo, 'financiamiento_e', calcula_financiamiento_e(equipo))
                    setattr(equipo, 'pago_total_e', calcula_pago_total(equipo))
                    setattr(equipo, 'costo_total_financiamiento_e', calcula_costo_total_financiamiento(equipo))
                    setattr(equipo, 'com_dar_e', calcula_com_dar(equipo))
                    equipo.save()
            except:
                equipo = Equipo.objects.get(pk=pk)
                form = EditarEquipos(request.GET or None, instance=equipo)
                return render(request, 'ecoclientes/editar_equipo.html',
                              {'form': form, 'equipo': equipo, 'vienede': vienede})

            return redirect('ecoclientes:ver_registro', vienede.fxf)
    else:
        equipo = Equipo.objects.get(pk=pk)
        form = EditarEquipos(request.GET or None, instance=equipo)
        return render(request, 'ecoclientes/editar_equipo.html', {'form': form, 'equipo':equipo,  'vienede':vienede})



@staff_member_required
def editar_inversionista(request, PRLV):
    pk=PRLV
    inver = Inversionista.objects.get(PRLV=pk)
    request.session['pk'] = pk
    if request.method == 'POST':
        form = EditarInversionista(request.POST, instance=inver)
        if form.is_valid():
            form.save()
            return redirect('ecoclientes:flujos', )
    else:
        equipo = Inversionista.objects.get(PRLV=pk)
        form = EditarInversionista(request.GET or None, instance=equipo)
        return render(request, 'ecoclientes/edita_inversionista.html', {'form': form, 'equipo':equipo,}) #  'vienede':vienede})


@staff_member_required
def nuevo_inversionista(request):
    if request.method == 'POST':
        form = EditarInversionista(request.POST, )
        if form.is_valid():
            form.save()
            return redirect('ecoclientes:flujos', )
    else:
       # equipo = Inversionista.objects.get(PRLV=pk)
        form = EditarInversionista(request.GET or None, )
        return render(request, 'ecoclientes/nuevo_inversionista.html', {'form': form, })



def archivar_inversionista(request, PRLV):
    pk = PRLV
    inver = Inversionista.objects.get(PRLV=pk)
    setattr(inver, 'archivado', 1)
    inver.save()
    return HttpResponseRedirect(reverse('ecoclientes:flujos'))


@staff_member_required
def ver_registros(request):
    request.session.set_expiry(5000)
    registros = Equipo.objects.all()
    for eco_registro in registros:
        eco_regis = Eco.objects.get(fxf=eco_registro.fxf_eqp)
        eco_registro.fxf_eqp = eco_regis
        eco_registro.numero_credito = eco_regis.numero_credito
        eco_registro.nombre_beneficiario = eco_regis.nombre_beneficiario
        #eco_registro.patrocinado = eco_regis.patrocinado
        eco_registro.estatus = eco_regis.estatus
        eco_registro.fecha_compra = eco_regis.fecha_compra
        eco_registro.obs = eco_regis.obs
    args = {'registros': registros, 'eco_regis' : eco_regis}
    return render(request, 'ecoclientes/ver_registros.html', args)


@staff_member_required
def ver_registrosdar(request, DAR):
    registros = Equipo.objects.filter(fxf_eqp__fxf__startswith=DAR)
    for eco_registro in registros:
        eco_regis = Eco.objects.get(fxf=eco_registro.fxf_eqp)
        eco_registro.fxf_eqp = eco_regis
        eco_registro.numero_credito = eco_regis.numero_credito
        eco_registro.nombre_beneficiario = eco_regis.nombre_beneficiario
        #eco_registro.patrocinado = eco_regis.patrocinado
        eco_registro.estatus = eco_regis.estatus
        eco_registro.fecha_compra = eco_regis.fecha_compra
        eco_registro.obs = eco_regis.obs
    args = {'registros': registros, 'eco_regis' : eco_regis}
    return render(request, 'ecoclientes/ver_registros.html', args)


@staff_member_required
def ver_registros_liberados(request, fxf):
    dar = request.session.pop('dar')
    #a = request.session.pop('fxfu')
    request.session['fxfi'] = str(fxf)
    print dar
    registros = Equipo.objects.filter(fxf_eqp__fxf__startswith=dar, fxf_eqp__estatus='LIBERADO')
    for eco_registro in registros:
        eco_regis = eco_registro.fxf_eqp
        eco_registro.numero_credito = eco_regis.numero_credito
        eco_registro.nombre_beneficiario = eco_regis.nombre_beneficiario
        #eco_registro.patrocinado = eco_regis.patrocinado
        eco_registro.estatus = eco_regis.estatus
        eco_registro.fecha_compra = eco_regis.fecha_compra
        eco_registro.obs = eco_regis.obs
    args = {'registros': registros, 'DAR' : dar, }
    return render(request, 'ecoclientes/ver_registros_liberados.html', args)


@staff_member_required
def editar_descuento(request, pk):
    fxfo = request.session.pop('fxfi')
    vienede = Eco.objects.get(fxf=fxfo)
    adeudo_total = vienede.adeudos
    equipo = Equipo.objects.get(pk=pk)
    if equipo.descuentos:
        a = float(equipo.descuentos)
    else:
        a = 0.
    if request.method == 'POST':
        form = EditarDescuentos(request.POST, instance=equipo)
        print 'entraste?'
        if form.is_valid():
            print 'es valida'
            equipo = form.save()
            setattr(vienede, 'obs',
                    vienede.obs + ' . Adeudo transferido a ' + equipo.fxf_eqp.fxf + ' por ' + equipo.descuentos + ' el ' + unicode(
                        datetime.date.today()) + '.')
            setattr(vienede, 'descuento_f', (-1) * float(equipo.descuentos) + vienede.descuento_f if vienede.descuento_f else (-1) * float(equipo.descuentos))
            setattr(vienede, 'adeudos', (-1) * float(equipo.descuentos) + vienede.adeudos)
            setattr(equipo, 'obs_e', equipo.obs_e + ' ' + 'Descuento aplicado de ' + vienede.fxf + ' por ' + equipo.descuentos + ' el ' + unicode(datetime.date.today())+'.' )
            setattr(equipo, 'descuentos', float(equipo.descuentos) + a )
            equipo.save()
            vienede.save()
            return redirect('ecoclientes:ver_registro', equipo.fxf_eqp.fxf)
    else:
        equipo = Equipo.objects.get(pk=pk)
        form = EditarDescuentos(request.GET or None, )
        form.fields['modelo_eqp'].initial = equipo.modelo_eqp
        form.fields['cantidad'].initial = equipo.cantidad
        form.fields['com_dar_e'].initial = equipo.com_dar_e
        form.fields['adeudos_eq'].initial = equipo.adeudos_eq
        form.fields['descuentos'].initial = '0'
        request.session['fxfi'] = fxfo
        return render(request, 'ecoclientes/edita_descuentos.html', {'form': form, 'equipo':equipo,  'vienede':vienede})


@staff_member_required
def ver_todos(request):
    date_t = datetime.datetime.now()
    if date_t.time() < datetime.time(10):
        actualiza_fecha()
    request.session.set_expiry(5000)
    registros = Equipo.objects.all()
    for eco_registro in registros:
        eco_regis = Eco.objects.get(fxf=eco_registro.fxf_eqp)
        eco_registro.fxf_eqp = eco_regis
        eco_registro.numero_credito = eco_regis.numero_credito
        eco_registro.nombre_beneficiario = eco_regis.nombre_beneficiario
        #eco_registro.patrocinado = eco_regis.patrocinado
        eco_registro.estatus = eco_regis.estatus
        eco_registro.fecha_compra = eco_regis.fecha_compra
        eco_registro.fecha_pago = eco_regis.fecha_pago
        eco_registro.factura_ecoluz_n = eco_regis.factura_ecoluz_n
        eco_registro.factura_costos_dar_n = eco_regis.factura_costos_dar_n
        eco_registro.obs = eco_regis.obs
    args = {'registros': registros, 'eco_regis' : eco_regis}
    return render(request, 'ecoclientes/ver_todo.html', args)


@staff_member_required
def editar_registro(request, fxf):
    vienede = Eco.objects.get(fxf=fxf)
    if request.method == 'POST':
        form = EditarRegistroEco(request.POST, instance=vienede)
        if request.FILES:
            form = EditarRegistroEco(request.POST, request.FILES, instance=vienede, )
        if form.is_valid():
            form.save()
            if vienede.first_name and vienede.last_name and hasattr(vienede, 'nombre_beneficiario')==False:
                setattr(vienede, 'nombre_beneficiario', nombre(vienede))
            if vienede.fecha_compra and vienede.fecha_pago:
                delta = vienede.fecha_pago- vienede.fecha_compra
                setattr(vienede, 'numero_dias', delta.days)
            if vienede.estatus == 'PENDIENTE':
                setattr(vienede, 'fecha_pago', datetime.date.today())
            vienede.save()
            return redirect('ecoclientes:ver_registro', fxf)
    else:
        vienede = Eco.objects.get(fxf=fxf)
        form = EditarRegistroEco(request.GET or None, instance=vienede)
        return render(request, 'ecoclientes/editar_registro.html', {'form': form})


@staff_member_required
def ver_registro(request, fxf):
    eco_regis = Eco.objects.get(fxf=fxf)
    equiposaeco = Equipo.objects.filter(fxf_eqp=eco_regis)
    fac = eco_regis.facturas_set.filter(tipo='ecoluz').first()
    eqps = []
    for eco_registro in equiposaeco:
        eco_registro.numero_credito = eco_regis.numero_credito
        eco_registro.nombre_beneficiario = eco_regis.nombre_beneficiario
        # eco_registro.patrocinado = eco_regis.patrocinado
        eco_registro.estatus = eco_regis.estatus
        eco_registro.fecha_compra = eco_regis.fecha_compra
        eco_registro.obs = eco_regis.obs
        eqps += [eco_registro]
    try:
        asigna_totales(eco_regis)
    except:
        print 'Faltan datos para aplicar totales'
    # print 'No se aplicaron totales'
    request.session['dar'] = fxf[:3]
    request.session['fxfu'] = fxf
    request.session['fxf'] = fxf
    # request.session['registro'] = eco_regis
    args = {'eqps': eqps, 'eco_regis' : eco_regis, 'fac':fac,   'DAR': eco_regis.DAR[:3]}
   # print eco_regis.facturas_set.first().pdf.url
    return render(request, 'ecoclientes/ver_registro.html', args)


@staff_member_required
def agrega_plaza(request):
    if request.method == 'POST':
        plaza = NuevaPlaza(request.POST)
        if plaza.is_valid():
            plaza.save()
            return redirect(reverse('ecoclientes:pre_registro'))
    else:
        plaza = NuevaPlaza(request.GET or None)
    return render(request, 'ecoclientes/agregar_plaza.html', {'nueva_plaza': plaza})



@staff_member_required
def edita_inventario(request, pk):
    inventario = Inventario.objects.get(pk=pk)
    fxf = request.session.pop('fxfu')
    if request.method == 'POST':
        modelo = NuevoModelo(request.POST, instance=inventario)
        if modelo.is_valid():
            modelo.save()
            return redirect('ecoclientes:ver_registro', fxf)
    else:
        inventario = Inventario.objects.get(pk=pk)
        modelo = NuevoModelo(request.GET or None, instance=inventario)
        request.session['fxfu'] = fxf
    return render(request, 'ecoclientes/agregar_modelo.html', {'nuevo_modelo': modelo})


@staff_member_required
def agrega_modelo(request):
    if request.method == 'POST':
        modelo = NuevoModelo(request.POST)
        if modelo.is_valid():
            modelo.save()
            return redirect(reverse('ecoclientes:nuevo_equipo'))
    else:
        modelo = NuevoModelo(request.GET or None)
    return render(request, 'ecoclientes/agregar_modelo.html', {'nuevo_modelo': modelo})


@staff_member_required
def agrega_modeloe(request, pk):
    if request.method == 'POST':
        modelo = NuevoModelo(request.POST)
        if modelo.is_valid():
            m = modelo.save()
            #modeloe = Inventario.objects.last()
            eqp = Equipo.objects.get(pk=pk)
            setattr(eqp, 'modelo_eqp', m)
            eqp.save()
            return redirect('/ecoclientes/editar_equipo/' + pk)
    else:
        modelo = NuevoModelo(request.GET or None)
    return render(request, 'ecoclientes/agregar_modelo.html', {'nuevo_modelo': modelo})


@staff_member_required
def facturacion(request, fxf):

    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    url = "https://apisandbox.facturama.mx/api/2/cfdis"
    services.nvo_diccionario_prod(fxf)
    services.nvo_diccionario_cli(fxf)
    dict = services.crea_diccionario(fxf)
    r = requests.post(url, data=json.dumps(dict), headers=head)
    print r, r.text.encode('utf-8')
    json_data = r.json()
    if r.status_code == 201:
        urf = "https://apisandbox.facturama.mx/api/Cfdi/pdf/issued/" + r.json()["Id"]
        urx = "https://apisandbox.facturama.mx/api/Cfdi/xml/issued/" + r.json()["Id"]
        urd = "https://apisandbox.facturama.mx/api/Cfdi/" + r.json()["Id"]
        sp = requests.get(urf, headers=head)
        sx = requests.get(urx, headers=head)
        nomp = r.json()["Id"]+'.pdf'
        nomx = r.json()["Id"]+'.xml'
        stp = sp.json()["Content"]
        stx = sx.json()["Content"]
        facturap = open(nomp, 'w+b')
        facturax = open(nomx, 'w+b')
        facturap.write(base64.decodestring(stp))
        facturax.write(base64.decodestring(stx))
        djangofilep = File(facturap)
        djangofilex = File(facturax)
        #facturap.close()
        #facturax.close()
        # djangofilep.close()
        # djangofilex.close()
        a = Eco.objects.get(fxf=fxf)
        fac = Facturas.objects.create(fxf_fac=a)
        id_fac = r.json()["Id"]
        ff =  sub(r'[\W_]+', '', r.json()["Complement"]["TaxStamp"]["Uuid"] )
        tot = r.json()["Total"]
        setattr(a, 'factura_ecoluz_n', id_fac)
      #  setattr(a, 'factura_ecoluz', djangofilep)
        setattr(fac, 'fxf_fac', a)
        setattr(fac, 'tipo', 'ecoluz')
        setattr(fac, 'folio_fiscal', ff)
        setattr(fac, 'total', tot)
        setattr(fac, 'pdf', djangofilep)
        setattr(fac, 'xml', djangofilex)
        # setattr(fac, 'xml', facturax)
        a.save()
        fac.save()
        facturap.close()
        facturax.close()
        #request.session['pdf'] = facturap
        request.session['id_factura'] = r.json()["Id"]
        request.session['url_del'] = urd
        request.session['fxf'] = fxf
        print a.factura_ecoluz
        return render(request, 'ecoclientes/factura.html', {'data': json_data, 'fac':fac,  'link': urf, 'eco' : a})  # 'dict':data})
    else:
        raise BaseException
        return render(request, 'ecoclientes/ver-registro.html', {'fxf': fxf, } )  # 'dict':data})

# @login_required(login_url='usuarios:acceso')
# def admon_facturacion(request, fxf):
#
#     head = {
#         'Content-Type': "application/json; charset=utf-8",
#         'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
#         'Cache-Control': "no-cache"
#     }
#     url = "https://apisandbox.facturama.mx/api/2/cfdis"
#     services.nvo_diccionario_prod(fxf)
#     services.nvo_diccionario_cli(fxf)
#     dict = services.crea_diccionario(fxf)
#     r = requests.post(url, data=json.dumps(dict), headers=head)
#     print r, r.text.encode('utf-8')
#     json_data = r.json()
#     if r.status_code == 201:
#         urf = "https://apisandbox.facturama.mx/api/Cfdi/pdf/issued/" + r.json()["Id"]
#         urx = "https://apisandbox.facturama.mx/api/Cfdi/xml/issued/" + r.json()["Id"]
#         urd = "https://apisandbox.facturama.mx/api/Cfdi/" + r.json()["Id"]
#         sp = requests.get(urf, headers=head)
#         sx = requests.get(urx, headers=head)
#         nomp = r.json()["Id"]+'.pdf'
#         nomx = r.json()["Id"]+'.xml'
#         stp = sp.json()["Content"]
#         stx = sx.json()["Content"]
#         facturap = open(nomp, 'w+b')
#         facturax = open(nomx, 'w+b')
#         facturap.write(base64.decodestring(stp))
#         facturax.write(base64.decodestring(stx))
#         djangofilep = File(facturap)
#         djangofilex = File(facturax)
#         #facturap.close()
#         #facturax.close()
#         # djangofilep.close()
#         # djangofilex.close()
#         a = Eco.objects.get(fxf=fxf)
#         fac = Facturas.objects.create(fxf_fac=a)
#         id_fac = r.json()["Id"]
#         ff =  sub(r'[\W_]+', '', r.json()["Complement"]["TaxStamp"]["Uuid"] )
#         tot = r.json()["Total"]
#         setattr(a, 'factura_ecoluz_n', id_fac)
#       #  setattr(a, 'factura_ecoluz', djangofilep)
#         setattr(fac, 'fxf_fac', a)
#         setattr(fac, 'tipo', 'ecoluz')
#         setattr(fac, 'folio_fiscal', ff)
#         setattr(fac, 'total', tot)
#         setattr(fac, 'pdf', djangofilep)
#         setattr(fac, 'xml', djangofilex)
#         # setattr(fac, 'xml', facturax)
#         a.save()
#         fac.save()
#         facturap.close()
#         facturax.close()
#         #request.session['pdf'] = facturap
#         request.session['id_factura'] = r.json()["Id"]
#         request.session['url_del'] = urd
#         request.session['fxf'] = fxf
#         print a.factura_ecoluz
#         return render(request, 'ecoclientes/factura.html', {'data': json_data, 'fac':fac,  'link': urf, 'eco' : a})  # 'dict':data})
#     else:
#         raise BaseException
#         return render(request, 'ecoclientes/ver-registro.html', {'fxf': fxf, } )  # 'dict':data})
#




@staff_member_required
def pdf_view(request):
    pdf = request.session.pop('pdf')
    try:
        return FileResponse(open(pdf, 'rb'), content_type='application/pdf')
    except FileNotFoundError:
        raise Http404()


@staff_member_required
def cancela_factura(request, fxf):
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    eco = Eco.objects.get(fxf=fxf)
    fac = eco.facturas_set.filter(tipo='ecoluz').first()
    try:
        url = request.session.pop('url_del')
    except:
        url = "https://apisandbox.facturama.mx/api/Cfdi/" + eco.factura_ecoluz_n + "?type=issued"
        print eco.factura_ecoluz_n
    #fxf = request.session.pop('fxf')
    r = requests.delete(url, headers=head)
    print r, r.text.encode('utf-8')
    if r.status_code == 200:
        messages.success(request, 'Factura cancelada')
        setattr(eco, 'factura_ecoluz', None)
        setattr(eco, 'factura_ecoluz_n', '')
        fac.delete()
        eco.save()
    else:
        messages.warning(request, 'No se pudo cancelar la factura.')
    return redirect('ecoclientes:ver_registro', fxf)


@staff_member_required
def captura_datos_fiscales(request, fxf):
    eco = Eco.objects.get(fxf=fxf)
    if request.method == 'POST':
        eco_registro = DatosFiscales(request.POST, instance=eco)
        if eco_registro.is_valid():
            eco_registro.save()
            return redirect('ecoclientes:ver_registro', eco.fxf)
    else:
        eco_registro = DatosFiscales(request.GET or None, instance=eco)
    return render(request, 'ecoclientes/captura_datos_fiscales.html', {'eco_registro': eco_registro})







###################################### A CONTINUACIÓN SE ENCUENTRAN LOS MÉTODOS PARA OBTENER INFORMACIÓN DE LAS FACTURAS.


def info_factura(llave, facturax, tipo):
    raiz = ET.parse(facturax).getroot()
    factura = Facturas()
    setattr(factura, 'fxf_fac', llave)
    setattr(factura, 'tipo', tipo)
    setattr(factura, 'folio', raiz.get('folio'))
    setattr(factura, 'folio_fiscal', raiz[4][0].get('UUID'))
    setattr(factura, 'total', raiz.get('total'))
    setattr(factura, 'subtotal', raiz.get('subTotal'))
    factura.save()
    conceptos = raiz[2].findall('{http://www.sat.gob.mx/cfd/3}Concepto')
    if factura.tipo == 'ecoluz':
        for concepto in conceptos:
            u = concepto.get('unidad').lower()
            if u == 'pieza' or u == 'pz' or u == 'pza':
                equipo = Equipo(fxf_eqp=llave)
                setattr(equipo, 'cantidad', concepto.get('cantidad'))
                setattr(equipo, 'modelo_equipo', concepto.get('noIdentificacion'))
                setattr(equipo, 'subtotal_equipo', float(concepto.get('importe')) * 1.16)
                r = re.compile('(No Serie)')
                setattr(equipo, 'numero_serie', r.split(concepto.get('descripcion'))[2])
                setattr(equipo, 'no_items', conceptos.count())
                equipo.save()
            if concepto.get('noIdentificacion').lower() == 'cayd' or unidecode.unidecode(concepto.get('descripcion')).lower() == 'centro de acopio y destruccion':
                setattr(llave, 'cayd_cobrado', float(concepto.get('importe')) * 1.16)
                llave.save()
    else:
        for concepto in conceptos:
            for eqp in Equipo.objects.filter(fxf_eqp=llave):
                if unidecode.unidecode(eqp.modelo_equipo).lower() in unidecode.unidecode(concepto.get('descripcion')).lower():
                    un_concepto = Concepto(equipo=eqp)
                    setattr(un_concepto, 'factura', factura)
                    setattr(un_concepto, 'noIdentificacion', concepto.get('noIdentificacion') or None)
                    setattr(un_concepto, 'cantidad', int(float(concepto.get('cantidad'))) or None )
                    setattr(un_concepto, 'descripcion', concepto.get('descripcion') or None)
                    setattr(un_concepto, 'unidad', concepto.get('unidad') or None)
                    setattr(un_concepto, 'valorUnitario', concepto.get('valorUnitario') or None)
                    setattr(un_concepto, 'importe', float(concepto.get('importe'))*1.16 or None)
                    un_concepto.save()


def datos_factura_ecoluz(objeto_eco):
    facturaEL= Facturas.objects.get(fxf_fac=objeto_eco, tipo='ecoluz')
    conceptos_fac = Concepto.objects.filter(factura=facturaEL)
    for concepto in conceptos_fac:
        if lower(concepto.unidad) == 'pieza' or lower(concepto.unidad) == 'pz' or lower(concepto.unidad) == 'pza':
            equipo = Equipo(fxf_eqp=objeto_eco)
            setattr(equipo, 'cantidad', concepto.cantidad)
            setattr(equipo, 'modelo_equipo', concepto.noIdentificacion)
            setattr(equipo, 'subtotal_equipo', concepto.importe*1.16)
            r = re.compile('(No Serie)')
            setattr(equipo, 'numero_serie', r.split(concepto.descripcion)[2] )
        if lower(concepto.noIdentificacion) == 'cayd' or lower(unidecode.unidecode(concepto.descripcion)) == 'centro de acopio y destruccion':
            setattr(objeto_eco, 'cayd_cobrado', concepto.importe * 1.16)
            equipo.save()
            objeto_eco.save()


def datos_factura_dar(objeto_eco):
    facturaD = Facturas.objects.get(fxf_fac=objeto_eco, tipo='dar')
    conceptos_fac = Concepto.objects.filter(factura=facturaD)
    for concepto in conceptos_fac:
        if lower(unidecode.unidecode(concepto.descripcion))[:3] == 'com ':
            setattr(objeto_eco, 'com', concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:4] == 'inst ':   ###### UPDATE ATTRIB
            setattr(objeto_eco, 'man', concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:3] == 'man ':
            objeto_eco.update('man', objeto_eco.man + concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:3] == 'mdm ': # and  :
            setattr(objeto_eco, 'mdm', concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:4] == 'cayd ':
            setattr(objeto_eco, 'cayd_pagado', concepto.importe * 1.16)


def datos_factura_fab(objeto_eco):
    facturaF = Facturas.objects.get(fxf_fac=objeto_eco, tipo='fab')
    conceptos_fac = Concepto.objects.filter(factura=facturaF)
    for concepto in conceptos_fac:
        if lower(concepto.descripcion).contains(objeto_eco.modelo_equipo):
            setattr(objeto_eco, 'eqp', concepto.valorUnitario * 1.16 * objeto_eco.cantidad)
        if lower(unidecode.unidecode(concepto.descripcion))[:1] == 'mm' or lower(unidecode.unidecode(concepto.descripcion))[:2] == 'mdm':
            setattr(objeto_eco, 'mdm', concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:4] == 'inst ':
            setattr(objeto_eco, 'mdm', concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:3] == 'mdm ':
            objeto_eco.update('mdm', objeto_eco.mdm + concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:3] == 'man ':
            setattr(objeto_eco, 'man', concepto.importe * 1.16)
        if lower(unidecode.unidecode(concepto.descripcion))[:4] == 'cayd ':
            setattr(objeto_eco, 'cayd_pagado', concepto.importe * 1.16)





    # if request.method == "POST":
    #     form = NuevaFactura(request.POST)
    #     if form.is_valid():
    #         "Authorization: Basic 718e3978516d387924d91980a7e21af2f434de445731951a6585bda2eacef046"

    #        # r = requests.get(' ' + settings.EMBEDLY_KEY + '&url=' + url)
    #        # json = r.json()
    #        # serializer = EmbedSerializer(data=json)
    #        # if serializer.is_valid():
    #         #    embed = serializer.save()
    #          #   return render(request, 'embeds.html', {'embed': embed})
    #
    # else:
    #     form = NuevaFactura()
    # return render(request, 'ecoclientes/genera_factura.html', {'form': form})
    #


@staff_member_required
def adeudos(request):
    dars = ['MCM', 'MFV', 'GMC', 'SOP', 'TCU', 'EMR', 'BPC', 'AVA', 'PRF', 'BHA', 'JCB'  # 'HCC',
            'VNT', ]
    adeudos_dars = {}
    for dar in dars:
        dt = Eco.objects.filter(fxf__startswith=dar)
        if dt.aggregate(Sum('adeudos'))['adeudos__sum'] > 1:
            adeudos_dars[dar] = []
            for aa in dt:
                if aa.adeudos > 1:
                    adeudos_dars[dar] += [aa]
                if aa.adeudos and aa.adeudos < -1:
                    adeudos_dars[dar] += [aa]
    args = {'dars': adeudos_dars, }
    return render(request, 'ecoclientes/adeudos.html', args)


@staff_member_required
def resumen_eco(request):
    date_t = datetime.datetime.now()
    if date_t.time() < datetime.time(11):
        actualiza_fecha()
    todos = {}
    cuentas = {}

    dars = ['MCM', 'MFV', 'GMC', 'SOP', 'TCU', 'EMR', 'BPC', 'AVA', 'PRF', 'BHA', 'JCB',  # 'HCC',
            'VNT', ]
    todos = collections.OrderedDict(sorted(todos.items(), key=lambda i: dars.index(i[0])))

    cuentas = collections.OrderedDict(sorted(cuentas.items(), key=lambda i: dars.index(i[0])))
    vrt = Eco.objects.filter(
        Q(estatus='PAGADO') | Q(estatus='LIBERADO') | Q(estatus='PENDIENTE') |
        Q(estatus='POR FONDEAR')| Q(estatus='EN VALIDACION') |
        Q(estatus='CANCELADO')).aggregate(Sum('pago_total'))['pago_total__sum']
    vpt = Eco.objects.filter(Q(estatus='PAGADO')).aggregate(Sum('pago_total'))['pago_total__sum']
    nft = Eco.objects.all().count()
    net = Equipo.objects.all().count()
    dft = Equipo.objects.all().aggregate(Avg('numero_dias_e'))['numero_dias_e__avg']
    mft = vrt / float(nft)
    rft = Eco.objects.all().aggregate(Avg('financiamiento'))['financiamiento__avg']
    fet = Eco.objects.all().aggregate(Sum('financiamiento'))['financiamiento__sum']
    dit = {'01-vr': vrt,
           '02-vrp': 100.00,
           '03-vp': vpt,
           '04-vpp': 100.00,
           '05-nf': nft,
           '06-ne': net,
           '07-df': dft,
           '08-mf': mft,
           '09-rf': rft,
           '10-fe': fet,
           '11-fep': 100.00,
           }
    cuentas['TOTAL'] = collections.OrderedDict(sorted(dit.items(), key=lambda t: t[0]))

    for a in dars:
        suma_pendientes = 0
        suma_liberados = 0
        suma_cancelados = 0
        financiamiento_ = 0
        financiamiento_cancelado = 0
        com_dar_cxp = 0
        com_dar_cxp_hoy = 0
        adeud = 0
        ventas = 0
        vr, vp = 0, 0
        for self in Eco.objects.filter(fxf__startswith=a):

            if self.estatus == 'PENDIENTE' and self.subtotal_operacion:
                suma_pendientes += self.subtotal_operacion


            if self.estatus == 'LIBERADO' and self.subtotal_operacion:
                suma_liberados += self.subtotal_operacion


            if self.estatus == 'CANCELADO' and self.subtotal_operacion:
                suma_cancelados += self.subtotal_operacion

            tot_inversion = suma_pendientes + suma_liberados + suma_cancelados

            subtot_operacion = suma_pendientes + suma_liberados

            if self.estatus in ['LIBERADO', 'PENDIENTE'] and self.financiamiento:
                financiamiento_ += self.financiamiento

            costo_financiamiento = tot_inversion + financiamiento_

            if self.estatus == 'CANCELADO' and self.financiamiento:
                financiamiento_cancelado += self.financiamiento

            if self.estatus in ['LIBERADO', 'PENDIENTE', 'POR ENTREGAR'] and self.com_dar:
                com_dar_cxp += self.com_dar

            if self.estatus == 'COM DAR CxP a hoy' and self.com_dar:
                com_dar_cxp_hoy += self.com_dar

            if self.adeudos:
                adeud += self.adeudos

            if self.estatus in ['LIBERADO', 'PENDIENTE', 'POR ENTREGAR'] and self.pago_total:
                ventas += self.pago_total

            if self.estatus in ['PAGADO', 'LIBERADO', 'PENDIENTE', 'POR FONDEAR', 'EN VALIDACION', 'CANCELADO'] and self.pago_total:
                vr += self.pago_total
            if self.estatus == 'PAGADO' and self.pago_total:
                vp += self.pago_total

        d= {'01-pen': suma_pendientes,
                '02-lib': suma_liberados,
                '03-can': suma_cancelados,
                '04-tci': tot_inversion,
                '05-sbt': subtot_operacion,
                '06-fin': financiamiento_,
                '07-fic': financiamiento_cancelado,
                '08-ctf': costo_financiamiento,
                '09-cxp': com_dar_cxp,
                '10-ade': adeud,
                '11-hoy': com_dar_cxp_hoy,
                '12-ven': ventas
        }
        todos[a] = collections.OrderedDict(sorted(d.items(), key=lambda t: t[0]))
        dt = {'01-pen': 0,
             '02-lib': 0,
             '03-can': 0,
             '04-tci': 0,
             '05-sbt': 0,
             '06-fin': 0,
             '07-fic': 0,
             '08-ctf': 0,
             '09-cxp': 0,
             '10-ade': 0,
             '11-hoy': 0,
             '12-ven': 0
             }
        for key, value in todos.iteritems():
            print key
            if key != 'TOTAL':
                for x, val in value.iteritems():
                   #a = sum(item[x] for item in value)
                    #dt[x] = 0
                    dt[x] = dt[x] + val

        todos['TOTAL'] = collections.OrderedDict(sorted(dt.items(), key=lambda t: t[0]))
        #print todos['TOTAL']
        vrp = vr / vrt * 100
        vpp = vp / vpt * 100
        nf = Eco.objects.filter(fxf__startswith=a).count()
        ne = Equipo.objects.filter(fxf_eqp__fxf__startswith=a).count()
        df = Equipo.objects.filter(fxf_eqp__fxf__startswith=a).aggregate(Avg('numero_dias_e'))['numero_dias_e__avg']
        mf = vr / ((nf) + 0.000000001)
        rf = Eco.objects.filter(fxf__startswith=a).aggregate(Avg('financiamiento'))['financiamiento__avg']
        fe = Eco.objects.filter(fxf__startswith=a).aggregate(Sum('financiamiento'))['financiamiento__sum'] or 0.0
        fep = fe / fet * 100
        dii = {'01-vr': vr,
               '02-vrp': vrp,
               '03-vp': vp,
               '04-vpp': vpp,
               '05-nf': nf,
               '06-ne': ne,
               '07-df': df,
               '08-mf': mf,
               '09-rf': rf,
               '10-fe': fe,
               '11-fep': fep,
               }
        cuentas[a] = collections.OrderedDict(sorted(dii.items(), key=lambda t: t[0]))
    return render(request, 'ecoclientes/resumen.html', {'todos':todos, 'cuentas':cuentas, 'l':dars} )


@staff_member_required
def resumen_ecop():
    todos = {}
    cuentas = {}
    vrt = Eco.objects.filter(
        Q(estatus='PAGADO') | Q(estatus='LIBERADO') | Q(estatus='PENDIENTE') |
        Q(estatus='POR FONDEAR')| Q(estatus='EN VALIDACION') |
        Q(estatus='CANCELADO')).aggregate(Sum('pago_total'))['pago_total__sum']
    vpt = Eco.objects.filter(Q(estatus='PAGADO')).aggregate(Sum('pago_total'))['pago_total__sum']
    nft = Eco.objects.all().count()
    net = Equipo.objects.all().count()
    dft = Equipo.objects.all().aggregate(Avg('numero_dias_e'))['numero_dias_e__avg']
    mft = vrt / float(nft)
    rft = Eco.objects.all().aggregate(Avg('financiamiento'))['financiamiento__avg']
    fet = Eco.objects.all().aggregate(Sum('financiamiento'))['financiamiento__sum']
    dit = {'01-vr': vrt,
           '02-vrp': 100.00,
           '03-vp': vpt,
           '04-vpp': 100.00,
           '05-nf': nft,
           '06-ne': net,
           '07-df': dft,
           '08-mf': mft,
           '09-rf': rft,
           '10-fe': fet,
           '11-fep': 100.00,
           }
    cuentas['TOTAL'] = collections.OrderedDict(sorted(dit.items(), key=lambda t: t[0]))
    for a in ['AVA', 'BPC','EMR', 'GMC', 'MCM', 'MFV', 'PRF', 'SOP', 'TCU', 'JCB', 'VNT']:
        suma_pendientes = 0
        suma_liberados = 0
        suma_cancelados = 0
        financiamiento_ = 0
        financiamiento_cancelado = 0
        com_dar_cxp = 0
        com_dar_cxp_hoy = 0
        adeud = 0
        ventas = 0
        vr, vp = 0, 0
        for self in Eco.objects.filter(fxf__startswith=a):

            if self.estatus == 'PENDIENTE' and self.subtotal_operacion:
                suma_pendientes += self.subtotal_operacion


            if self.estatus == 'LIBERADO' and self.subtotal_operacion:
                suma_liberados += self.subtotal_operacion


            if self.estatus == 'CANCELADO' and self.subtotal_operacion:
                suma_cancelados += self.subtotal_operacion

            tot_inversion = suma_pendientes + suma_liberados + suma_cancelados

            subtot_operacion = suma_pendientes + suma_liberados

            if self.estatus in ['LIBERADO', 'PENDIENTE'] and self.financiamiento:
                financiamiento_ += self.financiamiento

            costo_financiamiento = tot_inversion + financiamiento_

            if self.estatus == 'CANCELADO' and self.financiamiento:
                financiamiento_cancelado += self.financiamiento

            if self.estatus in ['LIBERADO', 'PENDIENTE', 'POR ENTREGAR'] and self.com_dar:
                com_dar_cxp += self.com_dar

            if self.estatus == 'COM DAR CxP a hoy' and self.com_dar:
                com_dar_cxp_hoy += self.com_dar

            if self.adeudos:
                adeud += self.adeudos

            if self.estatus in ['LIBERADO', 'PENDIENTE', 'POR ENTREGAR'] and self.pago_total:
                ventas += self.pago_total

            if self.estatus in ['PAGADO', 'LIBERADO', 'PENDIENTE', 'POR FONDEAR', 'EN VALIDACION', 'CANCELADO'] and self.pago_total:
                vr += self.pago_total
            if self.estatus == 'PAGADO' and self.pago_total:
                vp += self.pago_total

        d= {'01-pen': suma_pendientes,
                '02-lib': suma_liberados,
                '03-can': suma_cancelados,
                '04-tci': tot_inversion,
                '05-sbt': subtot_operacion,
                '06-fin': financiamiento_,
                '07-fic': financiamiento_cancelado,
                '08-ctf': costo_financiamiento,
                '09-cxp': com_dar_cxp,
                '10-ade': adeud,
                '11-hoy': com_dar_cxp_hoy,
                '12-ven': ventas
        }
        todos[a] = collections.OrderedDict(sorted(d.items(), key=lambda t: t[0]))

        vrp = vr / vrt * 100
        vpp = vp / vpt * 100
        nf = Eco.objects.filter(fxf__startswith=a).count()
        ne = Equipo.objects.filter(fxf_eqp__fxf__startswith=a).count()
        df = Equipo.objects.filter(fxf_eqp__fxf__startswith=a).aggregate(Avg('numero_dias_e'))['numero_dias_e__avg']
        mf = vr / float(nf)
        rf = Eco.objects.filter(fxf__startswith=a).aggregate(Avg('financiamiento'))['financiamiento__avg']
        fe = Eco.objects.filter(fxf__startswith=a).aggregate(Sum('financiamiento'))['financiamiento__sum'] or 0.0
        fep = fe / fet * 100
        dii = {'01-vr': vr,
               '02-vrp': vrp,
               '03-vp': vp,
               '04-vpp': vpp,
               '05-nf': nf,
               '06-ne': ne,
               '07-df': df,
               '08-mf': mf,
               '09-rf': rf,
               '10-fe': fe,
               '11-fep': fep,
               }
        cuentas[a] = collections.OrderedDict(sorted(dii.items(), key=lambda t: t[0]))

    return todos, cuentas



@staff_member_required
def flujos(request):
    date_t = datetime.datetime.now()
    if date_t.time() < datetime.time(12):
        actualiza_fecha()
    if request.method == 'POST':
        if 'semana' in request.POST:
            return redirect('ecoclientes:editar_flujos', request.POST.get('semana', '') )
        else:
            request.session['bc'] = request.POST.get('banco', '')
            request.session['cxpfab'] = request.POST.get('cxpfab', '')
            request.session['bbva'] = request.POST.get('bancoBBVA', '')
            request.session['mercapago'] = request.POST.get('mercadoPago', '')
            request.session['deudaSH'] = request.POST.get('deudaSERHUB', '')
            request.session['deudaCH'] = request.POST.get('deudaCoinHub', '')

            print request.POST.get('banco', '')
            return HttpResponseRedirect(reverse('ecoclientes:flujos'))
    else:
        try:
            aaa = request.session.pop('bc')
            bbb = request.session.pop('cxpfab')
            bbv = request.session.pop('bbva')
            mrp = request.session.pop('mercapago')
            dsh = request.session.pop('deudaSH')
            dch = request.session.pop('deudaCH')
            print aaa
        except:
            aaa = 0
            bbb = 0
            bbv = 0
            mrp = 0
            dsh = 0
            dch = 0
        suma_pendientes = 0
        suma_liberados = 0
        suma_cancelados = 0
        financiamiento_ = 0
        financiamiento_cancelado = 0
        com_dar_cxp = 0
        com_dar_cxp_hoy = 0
        adeud = 0
        ventas = 0
        vr, vp = 0, 0
        for self in Eco.objects.all():
            if self.estatus == 'PENDIENTE' and self.subtotal_operacion:
                suma_pendientes += self.subtotal_operacion
            if self.estatus == 'LIBERADO' and self.subtotal_operacion:
                suma_liberados += self.subtotal_operacion
            if self.estatus == 'CANCELADO' and self.subtotal_operacion:
                suma_cancelados += self.subtotal_operacion
            tot_inversion = suma_pendientes + suma_liberados + suma_cancelados
            subtot_operacion = suma_pendientes + suma_liberados
            if self.estatus in ['LIBERADO', 'PENDIENTE'] and self.financiamiento:
                financiamiento_ += self.financiamiento
            costo_financiamiento = tot_inversion + financiamiento_
            if self.estatus == 'CANCELADO' and self.financiamiento:
                financiamiento_cancelado += self.financiamiento
            if self.estatus in ['LIBERADO', 'PENDIENTE', 'POR ENTREGAR'] and self.com_dar:
                com_dar_cxp += self.com_dar
            if self.estatus == 'COM DAR CxP a hoy' and self.com_dar:
                com_dar_cxp_hoy += self.com_dar
            if self.adeudos:
                adeud += self.adeudos
            if self.estatus in ['LIBERADO', 'PENDIENTE', 'POR ENTREGAR'] and self.pago_total:
                ventas += self.pago_total
            if self.estatus in ['PAGADO', 'LIBERADO', 'PENDIENTE', 'POR FONDEAR', 'EN VALIDACION', 'CANCELADO'] and self.pago_total:
                vr += self.pago_total
            if self.estatus == 'PAGADO' and self.pago_total:
                vp += self.pago_total

        invs = Inversionista.objects.filter(archivado=0)
        for inv in invs:
            print inv, inv.capital
            setattr(inv, 'capital', float(inv.capital))
            if not inv.tasa_efectiva_real:
                inv.tasa_efectiva_real = inv.tasa_efectiva
            setattr(inv, 'interes', float(inv.capital) * float(inv.tasa_anual) / 100.)
            setattr(inv, 'monto_deuda', float(inv.capital) * float(inv.tasa_anual) / 100. + float(inv.capital))
            if inv.corte:
                setattr(inv, 'dias', -1*(inv.corte - datetime.date.today()).days)
                inv.diasi = -1*(inv.inicio - datetime.date.today()).days
                inv.monti = inv.interes
                inv.porcdi = inv.diasi/360. * 100
                inv.inti = inv.interes * inv.diasi/360.
            else:
                setattr(inv, 'dias', -1*(inv.inicio - datetime.date.today()).days)
            setattr(inv, 'intereses_hoy', inv.interes * inv.dias/360.)
            inv.porc = float(inv.capital) / Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Sum('capital'))['capital__sum']*100
            inv.hoy = datetime.date.today()
            inv.porcd = inv.dias/360. * 100
            inv.save()

        tots = {}
        tots['capital'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Sum('capital'))['capital__sum']
        tots['tasaef'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Avg('tasa_efectiva'))['tasa_efectiva__avg']
        tots['tasaefr'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Avg('tasa_efectiva_real'))['tasa_efectiva_real__avg']
        tots['tasaan'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Avg('tasa_anual'))['tasa_anual__avg']
        tots['deuda'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Sum('monto_deuda'))['monto_deuda__sum']
        tots['interes'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Sum('interes'))['interes__sum']
        tots['interesah'] = Inversionista.objects.exclude(persona='Grupo SER').filter(archivado=0).aggregate(Sum('intereses_hoy'))['intereses_hoy__sum']
        totscapital = tots['capital']
        totsdeuda = tots['deuda']
        totsinteres = tots['interesah']
        dcan = suma_cancelados
        dfic = financiamiento_cancelado
        dcxp = com_dar_cxp
        dade = adeud
        dhoy = com_dar_cxp_hoy
        dven = ventas

        dd = {
            'vcci' : dven - dcan - dfic ,
            'bc' : float(aaa),
            'bbva' : float(bbv),
            'mercapago' : float(mrp),
            'deudaSH' : float(dsh),
            'deudaCH' : float(dch),
            'tde' : dven - dcan - dfic + float(aaa) + float(bbv) + float(dch),
            'dc' :  totscapital,
            'iah' :  totsinteres,
            'mdh' :  totscapital + totsinteres,
            'cpccd' :  dcxp - dade + dhoy,
            'cxpfab' : float(bbb),
            'shdh' : dven - dcan - dfic + float(aaa) - totscapital - totsinteres - dcxp + dade - dhoy - float(bbb),
            'shdt' :   dven - dcan - dfic + float(aaa) - totsdeuda - dcxp + dade - dhoy - float(bbb)
        }

        s1 = pd.read_csv('fl17.csv', encoding='utf-8', skiprows=[])#
        s2 = pd.read_csv('fl18a.csv', encoding='utf-8', skiprows=[])#.fillna(' ', inplace=True)
        s1.fillna(' ', inplace=True)
        s2.fillna(' ', inplace=True)

        if datetime.datetime.now() < datetime.datetime(2018,04,14, 0,1):
            n = 15
        elif datetime.datetime(2018,04,14, 0,1) < datetime.datetime.now() < datetime.datetime(2018,04,18, 0,1):
            n = 16
        elif datetime.datetime(2018,04,18, 0,1) < datetime.datetime.now() < datetime.datetime(2018,04,25, 0,1):
            n = 17
        elif datetime.datetime(2018,04,25, 0,1) < datetime.datetime.now() < datetime.datetime(2018, 05, 2, 0, 1):
            n = 18
        elif datetime.datetime(2018, 05, 2, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 05, 9, 0, 1):
            n = 19
        elif datetime.datetime(2018, 05, 9, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 05, 16, 0, 1):
            n = 20
        elif datetime.datetime(2018, 05, 16, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 05, 23, 0, 1):
            n = 21
        elif datetime.datetime(2018, 05, 23, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 05, 30, 0, 1):
            n = 22
        elif datetime.datetime(2018, 05, 30, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 06, 6, 0, 1):
            n = 23
        elif datetime.datetime(2018, 06, 6, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 06, 13, 0, 1):
            n = 24
        elif datetime.datetime(2018, 06, 13, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 06, 20, 0, 1):
            n = 25
        elif datetime.datetime(2018, 06, 20, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 06, 27, 0, 1):
            n = 26
        elif datetime.datetime(2018, 06, 27, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 07, 4, 0, 1):
            n = 27
        elif datetime.datetime(2018, 07, 4, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 07, 11, 0, 1):
            n = 28
        elif datetime.datetime(2018, 07, 11, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 07, 18, 0, 1):
            n = 29
        elif datetime.datetime(2018, 07, 18, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 07, 25, 0, 1):
            n = 30
        elif datetime.datetime(2018, 07, 25, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 8, 5, 0, 1):
            n = 31
        elif datetime.datetime(2018, 8, 6, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 8, 13, 0, 1):
            n = 32
        elif datetime.datetime(2018, 8, 13, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 8, 20, 0, 1):
            n = 33
        elif datetime.datetime(2018, 8, 20, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 8, 27, 0, 1):
            n = 34
        elif datetime.datetime(2018, 8, 27, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 9, 3, 0, 1):
            n = 35
        elif datetime.datetime(2018, 9, 3, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 9, 10, 0, 1):
            n = 36
        elif datetime.datetime(2018, 9, 10, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 9, 17, 0, 1):
            n = 37
        elif datetime.datetime(2018, 9, 17, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 9, 24, 0, 1):
            n = 38
        elif datetime.datetime(2018, 9, 24, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 10, 1, 0, 1):
            n = 39
        elif datetime.datetime(2018, 10, 1, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 10, 8, 0, 1):
            n = 40
        elif datetime.datetime(2018, 10, 8, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 10, 15, 0, 1):
            n = 41
        elif datetime.datetime(2018, 10, 15, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 10, 22, 0, 1):
            n = 42
        elif datetime.datetime(2018, 10, 22, 0, 1) < datetime.datetime.now() < datetime.datetime(2018, 10, 29, 0, 1):
            n = 43
        if aaa != 0:
            s2.set_value(2, n, '$' + str("{0:.2f}".format(dd['vcci'])), takeable=True)
            s2.set_value(3, n,  '$' + str("{0:.2f}".format(dd['bc'])), takeable=True)
            s2.set_value(4, n, '$' + str("{0:.2f}".format(dd['bbva'])), takeable=True)
            s2.set_value(5, n, '$' + str("{0:.2f}".format(dd['mercapago'])), takeable=True)
            s2.set_value(6, n, '$' + str("{0:.2f}".format(dd['deudaSH'])), takeable=True)
            s2.set_value(7, n, '$' + str("{0:.2f}".format(dd['deudaCH'])), takeable=True)
            s2.set_value(8, n,  '$' + str("{0:.2f}".format(dd['tde'])), takeable=True)
            s2.set_value(9, n,  '$' + str("{0:.2f}".format(dd['dc'])), takeable=True)
            s2.set_value(10, n,  '$' + str("{0:.2f}".format(dd['iah'])), takeable=True)
            s2.set_value(11, n,  '$' + str("{0:.2f}".format(dd['mdh'])), takeable=True)
            s2.set_value(12, n,  '$' + str("{0:.2f}".format(dd['cpccd'])), takeable=True)
            s2.set_value(13, n,  '$' + str("{0:.2f}".format(dd['cxpfab'])), takeable=True)
            s2.set_value(14, n, '$' + str("{0:.2f}".format(dd['shdh'])), takeable=True)
            s2.set_value(15, n, '$' + str("{0:.2f}".format(dd['shdt'])), takeable=True)
            s2.to_csv('fl18a.csv', encoding='utf-8', index=False)

        for jj in xrange(32,52):
            if len(s2.iat[6, jj]) > 3:
                m = jj
            else:
                m = 30
        anterior = {}
        for i in xrange(2, 16):
            anterior[i] = s2.iat[i, m]
        for x in [s1,s2]:
            for y in x.columns:
                if y[:5] in 'Unnamed':
                    x.rename(columns={y:' '}, inplace=True)
        d17 = s1.to_html(index=False, bold_rows=True, border=2, )
        d18 = s2.to_html(index=False, )
        d17 = d17.replace('<td>', '<td align="right">')
        d17 = d17.replace('$ ', '$')
       # d17 = d17.replace('<table border="2" class="dataframe">', '<table border="2" id="example" class="display" cellpadding="5" width="auto" style="margin-right: 3px; margin-left: 3%;">')
        d18 = d18.replace('<td>', '<td align="right">')
        d18 = d18.replace('$ ', '$')
        fec = datetime.date.today()
        args = {'ac': d17, 'b': d18, 'invs': invs, 'tots':tots, 'dd': dd, 'fecha':fec, 'ant':anterior}
        return render(request, 'ecoclientes/flujos.html', args )


@staff_member_required
def editar_flujos(request, semana):
    if request.method == 'POST':
        n = int(semana)
        aaa = request.POST.get('banco', '')
        bbb = request.POST.get('cxpfab', '')
        bbv = request.POST.get('bancoBBVA', '')
        mrp = request.POST.get('mercadoPago', '')
        dsh = request.POST.get('deudaSERHUB', '')
        dch = request.POST.get('deudaCoinHub', '')
        dd = {
            'vcci' : request.POST.get('vcci', ''),
            'bc' : aaa,
            'bbva' : bbv,
            'mercapago' : mrp,
            'deudaSH' : dsh,
            'deudaCH' : dch,
            'tde' : request.POST.get('tde', ''),
            'dc' :  request.POST.get('dc', ''),
            'iah' :  request.POST.get('iah', ''),
            'mdh' :  request.POST.get('mdh', ''),
            'cpccd' :  request.POST.get('cpccd', ''),
            'cxpfab' : bbb,
            'shdh' : request.POST.get('shdh', ''),
            'shdt' :   request.POST.get('shdt', ''),
        }
        s2 = pd.read_csv('fl18a.csv', encoding='utf-8', skiprows=[])  # .fillna(' ', inplace=True)
        s2.fillna(' ', inplace=True)
        s2.set_value(2, n, dd['vcci'], takeable=True)
        s2.set_value(3, n, dd['bc'], takeable=True)
        s2.set_value(4, n, dd['bbva'], takeable=True)
        s2.set_value(5, n, dd['mercapago'], takeable=True)
        s2.set_value(6, n, dd['deudaSH'], takeable=True)
        s2.set_value(7, n, dd['deudaCH'], takeable=True)
        s2.set_value(8, n, dd['tde'], takeable=True)
        s2.set_value(9, n, dd['dc'], takeable=True)
        s2.set_value(10, n, dd['iah'], takeable=True)
        s2.set_value(11, n, dd['mdh'], takeable=True)
        s2.set_value(12, n, dd['cpccd'], takeable=True)
        s2.set_value(13, n, dd['cxpfab'], takeable=True)
        s2.set_value(14, n, dd['shdh'], takeable=True)
        s2.set_value(15, n, dd['shdt'], takeable=True)
        s2.to_csv('fl18a.csv', encoding='utf-8', index=False)
        print request.POST.get('banco', '')
        return HttpResponseRedirect(reverse('ecoclientes:flujos'))
    else:
        n = int(semana)
        s2 = pd.read_csv('fl18a.csv', encoding='utf-8', skiprows=[])  # .fillna(' ', inplace=True)
        s2.fillna(' ', inplace=True)
        dd = {
            'vcci': s2.iat[2, n] if len(s2.iat[2, n])>1 else 0,
            'bc': s2.iat[3, n] if len(s2.iat[3, n])>1 else 0,
            'bbva': s2.iat[4, n] if len(s2.iat[4, n])>1 else 0,
            'mercapago': s2.iat[5, n] if len(s2.iat[5, n])>1 else 0,
            'deudaSH': s2.iat[6, n] if len(s2.iat[6, n])>1 else 0,
            'deudaCH': s2.iat[7, n] if len(s2.iat[7, n])>1 else 0,
            'tde': s2.iat[8, n] if len(s2.iat[8, n])>1 else 0,
            'dc': s2.iat[9, n] if len(s2.iat[9, n])>1 else 0,
            'iah': s2.iat[10, n] if len(s2.iat[10, n])>1 else 0,
            'mdh': s2.iat[11, n] if len(s2.iat[11, n])>1 else 0,
            'cpccd': s2.iat[12, n] if len(s2.iat[12, n])>1 else 0,
            'cxpfab': s2.iat[13, n] if len(s2.iat[13, n])>1 else 0,
            'shdh': s2.iat[14, n] if len(s2.iat[14, n])>1 else 0,
            'shdt': s2.iat[15, n] if len(s2.iat[15, n])>1 else 0,
        }
        return render(request, 'ecoclientes/editar_flujo.html',
                      {'dd': dd, })


#Ventas Realizadas
# 	%
#Ventas pagadas
# %
# FXF
#  EQP
# Días X FXF
# Monto X FXF
# Renta X FXF
# Finan EcoLuz
# %



#     #
#     # Pendiente
#     # Liberado
#     # Cancelado
#     # Total costos invertidos
#     # Subtotal Operación
#     # Financiamiento
#     # Financimiento cancelado
#     # Costo Total Financiamiento
#     # COM DAR CxP
#     # Adeudos
#     # COM DAR CxP a hoy
#     # Ventas
#     #








######################################### MÉTODOS PARA REALIZAR LAS OPERACIONES INTERNAS DEL ECO


def dame_opciones():
    nuevas_opciones = []
    for x in Plaza.objects.all():
        nuevas_opciones += [x.DAR+'-'+x.ciudad]
    return nuevas_opciones

#
# class CUENTAS(models.Eco):
#
#     # @property
#     # def obten_numero_serie(self):
#
#
#     @property
#     def calcula_subtotal_op(self):
#         return self.eqp + self.reguladores + self.mdm + self.com + self.man + self.cayd_pagado
#
#
#     def calcula_financiamiento(self):
#         if self.numero_dias_e < 31:
#             return self.subtotal_operacion_e * 0.11
#         elif self.numero_dias < 52:
#             return self.subtotal_operacion_e * 0.187
#         elif self.numero_dias < 61:
#             return self.subtotal_operacion_e * 0.22
#         else:
#             return self.subtotal_operacion_e * (0.22 + 0.0014 * (self.numero_dias_e - 60))
#
#
#     @property
#     def calcula_pago_total(self):
#         return self.subtotal_pago + self.cayd_cobrado
#
#
#     @property
#     def calcula_costo_total_financiamiento(self):
#         return self.subtotal_operacion + self.financiamiento
#
#
#     @property
#     def calcula_com_dar(self):
#         return self.pago_total - self.costo_total_financiamiento   #  AVISA SI ES NEGATIVA!!
#
#
#     @property
#     def calcula_numero_dias(self):
#         delta = self.fecha_pago - self.fecha_compra
#         return delta.days
#
#
#     @property
#     def nombre(self):
#         return ''.join([self.last_name, ', ', self.first_name]) # Atributos de Cliente
#
#
#
#
#
