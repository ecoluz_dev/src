# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from ecoclientes.models import Eco, Facturas, Equipo, Plaza, Inventario, Inversionista
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator


Estatus_CHOICES = (
    ('en_validacion', 'EN VALIDACION'),
    ('pendiente' ,  'PENDIENTE'),
    ('pagado' , 'PAGADO'),
    ('traspaso' , 'TRASPASO'),
    ('cancelado_sf' , 'CANCELADO SIN FONDEO'),
    ('cancelado', 'CANCELADO'),
    ('por_fondear', 'POR FONDEAR'),
    ('liberado', 'LIBERADO'),
     )


class RegistroEco(forms.ModelForm):
    class Meta:
        model = Eco
        fields = [
            'nombre_beneficiario',
            'fecha_compra',
            'obs',
        ]

        widgets = {'patrocinado': forms.CheckboxInput,
                   'fecha_pago': forms.SelectDateWidget(years=range(2014, 2025)),
                   'fecha_compra': forms.SelectDateWidget(years=range(2014, 2025)),
                   'factura_ecoluz': forms.ClearableFileInput(attrs={'multiple': True}),
                   'factura_costos_dar': forms.ClearableFileInput(attrs={'multiple': True}),
                   }


class DatosFiscales(forms.ModelForm):
    rfc_valido = RegexValidator(r"^[a-zA-Z]{4}\d{6}[a-zA-Z0-9]{3}$",  message="Debe escribir un RFC con el formato válido.")
    rfc = forms.CharField(label='RFC', required=True, validators=[rfc_valido],)
    class Meta:
        model = Eco
        fields = [
            'nombre_beneficiario',
            'rfc',
            'calle',
            'numero_exterior',
            'numero_interior',
            'colonia',
            'codigo_postal',
            'delegacion_municipio',
            'estado',
        ]

        widgets = {'patrocinado': forms.CheckboxInput,
                   'fecha_pago': forms.SelectDateWidget(years=range(2014, 2025)),
                   'fecha_compra': forms.SelectDateWidget(years=range(2014, 2025)),
                   'factura_ecoluz': forms.ClearableFileInput(attrs={'multiple': True}),
                   'factura_costos_dar': forms.ClearableFileInput(attrs={'multiple': True}),
                   }




class PreRegistroEco(forms.ModelForm):

    DAR = forms.ModelChoiceField(label='DAR', queryset=Plaza.objects.order_by('DAR'),
                                 empty_label='Seleccione uno')
    class Meta:
        model = Eco
        fields = [
            'tipo_venta',
            'numero_credito',
            'DAR',
            'tecnologia'
        ]
        widgets = { }



    def clean(self):
        data = self.cleaned_data.get('tipo_venta')
        d = self.cleaned_data.get('numero_credito')
        if data == 'PAEEEM' and d == '':
            raise forms.ValidationError('Si el tipo de venta es PAEEEM debes ingresar el número de crédito.')




Modelos_CHOICES = (
    ('en_validacion', 'EN VALIDACION'),
    ('pendiente', 'PENDIENTE'),
    ('pagado', 'PAGADO'),
    ('traspaso', 'TRASPASO'),
    ('cancelado_sf', 'CANCELADO SIN FONDEO'),
    ('cancelado', 'CANCELADO'),
    ('por_fondear', 'POR FONDEAR'),
    ('liberado', 'LIBERADO'),
)

class PreRegistroEquipos(forms.ModelForm):

    modelo_eqp = forms.ModelChoiceField(label='Modelo equipo', queryset=Inventario.objects.order_by('modelo'),
                                 empty_label='Seleccione uno')

    class Meta:
        model = Equipo
        fields = [
            'modelo_eqp',
            'cantidad',
            'subtotal_equipo',
            'gastos_inst',
        ]



class RegistroEquipos(forms.ModelForm):
    modelo_eqp = forms.ModelChoiceField(label='Modelo equipo', queryset=Inventario.objects.order_by('modelo'),
                                 empty_label='Selecciona uno')

    class Meta:
        model = Equipo
        fields = [
            'modelo_eqp',
            'patrocinado',
            'cantidad',
            'independiente',
            'fecha_compra_e',
            # 'factura_ecoluz_ne',
            # 'factura_ecoluz_e',
            'subtotal_equipo',
            'gastos_inst',
            'cayd_cobrado_e',
            'numero_serie',

            'eqp_e',
            'mdm_e',
            'man_e',
            'cayd_pagado_e',
            'com_e',

            'montaje_e',
            'monitoreo_e',
            'reguladores_e',
            'suministro_instalacion_e',
            #  'estructura_e',
            'inversor_e',
            'microinversor_e',

            'adeudos_eq',
            'descuentos',
            'factura_fab_n',
            'factura_fab',
            'factura_reg_n',
            'factura_reg',
            'obs_e',
        ]

        widgets = {'patrocinado': forms.CheckboxInput,
                   'factura_fab': forms.ClearableFileInput(attrs={'multiple': True}),
                   'factura_reg': forms.ClearableFileInput(attrs={'multiple': True}),
                   }


class EditarDescuentos(forms.ModelForm):
   # descuentos = forms.CharField(initial=0.0)
    class Meta:
        model = Equipo
        fields = [
            'modelo_eqp',
            'cantidad',
            'com_dar_e',
            'adeudos_eq',
            'descuentos'
        ]
    #
    # def clean_descuentos(self):
    #     email = self.cleaned_data['email']
    #     if User.objects.filter(email=email).exists():
    #         raise ValidationError("Email already exists")
    #     return email


class EditarEquipos(forms.ModelForm):

    class Meta:
        model = Equipo
        fields = [
            'modelo_eqp',
            'patrocinado',
            'cantidad',
            'independiente',
            'fecha_compra_e',
            #'factura_ecoluz_ne',
            #'factura_ecoluz_e',
            'subtotal_equipo',
            'gastos_inst',
            'cayd_cobrado_e',
            'numero_serie',

            'eqp_e',
            'mdm_e',
            'man_e',
            'cayd_pagado_e',
            'com_e',

            'montaje_e',
            'monitoreo_e',
            'reguladores_e',
            'suministro_instalacion_e',
          #  'estructura_e',
            'inversor_e',
            'microinversor_e',

            'adeudos_eq',
            'descuentos',
            'factura_fab_n',
            'factura_fab',
            'factura_reg_n',
            'factura_reg',
            'obs_e',
        ]


        widgets = {'patrocinado': forms.CheckboxInput,
                   'independiente': forms.CheckboxInput,
                   'factura_fab': forms.ClearableFileInput(attrs={'multiple': True}),
                   'factura_reg': forms.ClearableFileInput(attrs={'multiple': True}),
                   'fecha_compra_e' : forms.SelectDateWidget(years=range(2014, 2025)),
                   }


class EditarInversionista(forms.ModelForm):

    class Meta:
        model = Inversionista
        fields = [
            'persona',
            'PRLV',
            'capital',
            'tasa_efectiva',
            'tasa_efectiva_real',
            'tasa_anual',
            'interes',
            'monto_deuda',
            'inicio',
            'termino',
            'corte',
            'corte1',
            'corte2',
            'dias',
            'intereses_hoy',
            'folio_fiscales',
            'estatus_PRLV',
        ]

        widgets = {'inicio': forms.SelectDateWidget(years=range(2014, 2025)),
                   'termino': forms.SelectDateWidget(years=range(2014, 2025)),
                   'corte': forms.SelectDateWidget(years=range(2014, 2025)),
                   'corte1': forms.SelectDateWidget(years=range(2014, 2025)),
                   'corte2': forms.SelectDateWidget(years=range(2014, 2025)),
                   }



class EditarRegistroEco(forms.ModelForm):

    class Meta:
        model = Eco
        fields = [
            'fxf',
            'numero_credito',
            'tecnologia',
            'nombre_beneficiario',
            'estatus',
            'fecha_compra',
            'fecha_pago',
            'factura_ecoluz_n',
            'factura_ecoluz',
            'factura_costos_dar_n',
            'factura_costos_dar',
            'obs',
        ]

        widgets = {'fecha_pago': forms.SelectDateWidget(years=range(2014, 2025)),
                   'fecha_compra': forms.SelectDateWidget(years=range(2014, 2025)),
                   'factura_ecoluz': forms.ClearableFileInput(attrs={'multiple': True}),
                   'factura_costos_dar': forms.ClearableFileInput(attrs={'multiple': True}),
                   }

class NuevaPlaza(forms.ModelForm):

    class Meta:
        model = Plaza
        fields = [
            'DAR',
            'ciudad',
             ]


class NuevoModelo(forms.ModelForm):

    class Meta:
        model = Inventario
        fields = [
            'modelo',
            'precio_lista',
            'fabricante',
            'descripcion',
            'codigo_producto',
             ]
        fields_required = [
            'modelo',
            'precio_lista',
            'fabricante',
            'codigo_producto'
        ]



def opciones_dar(x):
    if x == 'AVA':
        return ['MET']
    if x == 'BHA':
        return ['XAL']
    if x == 'BPC':
        return ['HGO']
    if x == 'CNG':
        return ['TAM']
    if x == 'EMR':
        return ['NAY']
    if x == 'FIT':
        return ['MTY']
    if x == 'GMC':
        return ['SIN']
    if x == 'JAM':
        return ['TOL']
    if x == 'MCM':
        return ['MTY', 'SAL']
    if x == 'MFV':
        return ['MTY']
    if x == 'POM':
        return ['GTO']
    if x == 'PRF':
        return ['IRA', 'PUE', 'SLP']
    if x == 'SOP':
        return ['ZAC', 'DUR']
    if x == 'TCU':
        return ['MER']
    if x == 'VTN':
        return ['MET']



class NuevaFactura(forms.Form):

    first_name = forms.CharField(label=_("Nombre"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)    
    last_name = forms.CharField(label=_("Apellido Paterno"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)    
    last_last_name = forms.CharField(label=_("Apellido Materno"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)    
    calle = forms.CharField(label=_("Calle"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    numero_exterior = forms.CharField(label=_("Número Ext"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    numero_interior = forms.CharField(label=_("Número Int"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    colonia = forms.CharField(label=_("Colonia"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    delegacion_municipio = forms.CharField(label=_("Delegación/Municipio"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    ciudad = forms.CharField(label=_("Ciudad"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    estado = forms.CharField(label=_("Estado"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    cp = forms.CharField(label=_("C.P."), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    rfc = forms.CharField(label=_("RFC"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True, ) # pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z0-9]{3}$", title="Debe escribir un RFC con el formato válido.")


class ItemsFactura(forms.Form):

    concepto = forms.CharField(label=_("Concepto"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    codigo_producto = forms.CharField(label=_("Prod ID"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=False)
    num_identificacion = forms.CharField(label=_("Num ID"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=False)
    unidad = forms.CharField(label=_("Unidad"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    cod_unidad = forms.CharField(label=_("Código Unidad"), widget=forms.TextInput(attrs={'class': 'form-control'}), max_length=30, required=True)
    precio_unitario = forms.DecimalField(label=_("Precio Unitario"), widget=forms.TextInput(attrs={'class': 'form-control'}),  required=True)
    cantidad = forms.IntegerField(label=_("Cantidad"), widget=forms.TextInput(attrs={'class': 'form-control'}), required=True)
    subtotal = forms.DecimalField(label=_("Subtotal"), widget=forms.TextInput(attrs={'class': 'form-control'}),  required=False)



