# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Eco, Facturas, Concepto, Equipo, Plaza, Inventario, Inversionista

# Register your models here.

#admin.site.register(Eco)
admin.site.register(Facturas)
admin.site.register(Concepto)
admin.site.register(Plaza)
admin.site.register(Inversionista)



class EquipoInline(admin.TabularInline):
    model = Equipo
    max_num = 1
    exclude = ['modelo_equipo']



class EquipoAdmin(admin.ModelAdmin):
    list_display = ('fxf_eqp', 'modelo_eqp', 'subtotal_equipo', 'cayd_cobrado_e')
    exclude = ['modelo_equipo']
    search_fields = ('fxf_eqp', 'modelo_eqp')


class InventarioAdmin(admin.ModelAdmin):
    list_display = ('modelo', 'descripcion', 'codigo_producto', 'fabricante', 'id_facturama', 'precio_lista')
    search_fields = ('modelo',  'codigo_producto')


class EcoAdmin(admin.ModelAdmin):
    list_display = ('fxf',  'numero_credito', 'nombre_beneficiario', 'estatus', 'fecha_pago', )
    list_filter = ()
    search_fields = ('fxf', 'nombre_beneficiario', 'numero_credito')
    inlines = [
        EquipoInline
    ]
    exclude = [
        'modelo_equipo',
        'eqp',
        'reguladores',
        'mdm',
        'com',
        'man',
        'cayd_pagado',
        'subtotal_operacion',
        'financiamiento',
        'costo_total_financiamiento'
        ]

admin.site.register(Equipo, EquipoAdmin)

admin.site.register(Inventario, InventarioAdmin)

admin.site.register(Eco, EcoAdmin )
