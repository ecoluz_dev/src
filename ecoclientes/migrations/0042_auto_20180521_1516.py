# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-05-21 20:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0041_auto_20180520_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eco',
            name='obs',
            field=models.TextField(blank=True, max_length=600, verbose_name='Observaciones'),
        ),
        migrations.AlterField(
            model_name='equipo',
            name='obs_e',
            field=models.CharField(blank=True, max_length=600, verbose_name='Observaciones del equipo'),
        ),
    ]
