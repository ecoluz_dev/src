# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-07-24 16:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0045_auto_20180610_1734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipo',
            name='fecha_compra_e',
            field=models.DateField(blank=True, null=True, verbose_name='Fecha de compra'),
        ),
        migrations.AlterField(
            model_name='equipo',
            name='numero_dias_e',
            field=models.IntegerField(blank=True, null=True, verbose_name='# Dias eqp'),
        ),
        migrations.AlterField(
            model_name='equipo',
            name='numero_serie',
            field=models.CharField(blank=True, max_length=200, verbose_name='# de serie '),
        ),
    ]
