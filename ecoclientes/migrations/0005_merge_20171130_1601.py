# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-11-30 22:01
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0004_merge_20171129_1830'),
        ('ecoclientes', '0004_auto_20171130_1556'),
    ]

    operations = [
    ]
