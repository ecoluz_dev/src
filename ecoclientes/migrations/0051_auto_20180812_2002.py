# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-08-13 01:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0050_auto_20180808_2139'),
    ]

    operations = [
        migrations.AddField(
            model_name='eco',
            name='estructura',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Estructura'),
        ),
        migrations.AddField(
            model_name='eco',
            name='inversor',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Inversor'),
        ),
        migrations.AddField(
            model_name='eco',
            name='microinversor',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Microinversor'),
        ),
        migrations.AddField(
            model_name='eco',
            name='suministro_instalacion',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Suministro de Instalaci\xf3n'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='estructura_e',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Estructura'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='inversor_e',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Inversor'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='microinversor_e',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Microinversor'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='suministro_instalacion_e',
            field=models.FloatField(blank=True, default=0.0, max_length=15, null=True, verbose_name='Suministro de Instalaci\xf3n'),
        ),
        migrations.AlterField(
            model_name='eco',
            name='tecnologia',
            field=models.CharField(blank=True, choices=[('Refrigerci\xf3n Comercial', 'Refrigerci\xf3n Comercial'), ('C\xe1maras de Refrigeraci\xf3n', 'C\xe1maras de Refrigeraci\xf3n'), ('Sistemas Fotovoltaicos', 'Sistemas Fotovoltaicos'), ('Aires Acondicionados', 'Aires Acondicionados'), ('Luminarias', 'Luminarias'), ('Calentadores Solares', 'Calentadores Solares')], max_length=50, null=True, verbose_name='Tecnolog\xeda'),
        ),
    ]
