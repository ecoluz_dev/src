# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-05-23 15:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0042_auto_20180521_1516'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eco',
            name='factura_ecoluz1',
        ),
        migrations.RemoveField(
            model_name='eco',
            name='factura_ecoluz_n1',
        ),
        migrations.AddField(
            model_name='equipo',
            name='independiente',
            field=models.BooleanField(default=0, verbose_name='Facturaci\xf3n independiente'),
        ),
    ]
