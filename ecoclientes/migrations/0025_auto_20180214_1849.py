# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-02-15 00:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0024_auto_20180207_2325'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventario',
            name='id_facturama',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='ID facturama'),
        ),
        migrations.AddField(
            model_name='inventario',
            name='litros_utiles',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Litros \xfatiles'),
        ),
        migrations.AddField(
            model_name='inventario',
            name='modalidad',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Modalidad'),
        ),
    ]
