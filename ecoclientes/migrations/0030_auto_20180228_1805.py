# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2018-03-01 00:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ecoclientes', '0029_auto_20180221_2018'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inventario',
            name='litros_utiles',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Litros \xfatiles'),
        ),
        migrations.AlterField(
            model_name='inventario',
            name='modalidad',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='Modalidad'),
        ),
        migrations.AlterField(
            model_name='inventario',
            name='unit_code',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='C\xf3digo de unidad'),
        ),
    ]
