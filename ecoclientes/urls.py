
from django.conf.urls import url

from . import views

app_name = 'ecoclientes'
urlpatterns = [
    url(r'^nuevo-registro/$', views.nuevo_registro, name='nuevo_registro'),
    url(r'^pre-registro/$', views.preregistro, name='pre_registro'),
    url(r'^registrar-equipo/$', views.nuevo_equipo, name='nuevo_equipo'),
    url(r'^ver-registro/(?P<fxf>[^/]+)$', views.ver_registro, name='ver_registro'),
    url(r'^editar/(?P<fxf>[^/]+)$', views.editar_registro, name='editar_registro'),
    url(r'^editar_equipo/(?P<pk>[^/]+)$', views.editar_equipo, name='editar_equipo'),
    url(r'^edita_inventario/(?P<pk>[^/]+)$', views.edita_inventario, name='edita_inventario'),
    url(r'^edita_descuentos/(?P<pk>[^/]+)$', views.editar_descuento, name='editar_descuentos'),
    url(r'^ver-registros/$', views.ver_registros, name='ver_registros'),
    url(r'^ver-registros/(?P<DAR>[^/]+)$', views.ver_registrosdar, name='ver_registrosdar'),
    url(r'^ver-registros-liberados/(?P<fxf>[^/]+)$', views.ver_registros_liberados, name='ver_registros_liberados'),
    url(r'^ver-todo/$', views.ver_todos, name='ver_todo'),
    url(r'^agrega-plaza/$', views.agrega_plaza, name='agrega_plaza'),
    url(r'^agrega-modelo/$', views.agrega_modelo, name='agrega_modelo'),
    url(r'^agrega-modeloe/(?P<pk>[^/]+)$', views.agrega_modeloe, name='agrega_modeloe'),
    url(r'^ver-registro/(?P<fxf>[^/]+)/genera-factura/$', views.facturacion, name='genera_factura'),
  #  url(r'^ver-registro/(?P<fxf>[^/]+)/administra-factura/$', views.admon_facturacion, name='administra_factura'),
    url(r'^ver-registro/(?P<fxf>[^/]+)/cancela-factura/$', views.cancela_factura, name='cancela_factura'),
    url(r'^ver-registro/(?P<fxf>[^/]+)/captura-datos-fiscales/$', views.captura_datos_fiscales, name='captura_datos_fiscales'),
    url(r'^resumen/$', views.resumen_eco, name='resumen'),
    url(r'^flujos/$', views.flujos, name='flujos'),
    url(r'^adeudos/$', views.adeudos, name='adeudos'),
    url(r'^flujos/nuevo-inversionista$', views.nuevo_inversionista, name='nuevo_inversionista'),
    url(r'^flujos/(?P<PRLV>[^/]+)$', views.editar_inversionista, name='inversionista'),
    url(r'^flujos/(?P<PRLV>[^/]+)/archivar$', views.archivar_inversionista, name='archivar_inversionista'),
    url(r'^editar-flujos/(?P<semana>[^/]+)$', views.editar_flujos, name='editar_flujos'),
]