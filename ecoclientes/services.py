# -*- coding: utf-8 -*-

import requests
import json
from .models import Eco, Inventario, Equipo, Facturas
import datetime
from decimal import *
from decimal import Decimal as D
#from celery import Celery
#from celery.schedules import crontab

#app = Celery('settings')

# def
#
#     app.conf.beat_schedule = {
#         # Executes everyday's midnight
#         'add-everyday-midnight': {
#             'task': 'tasks.add',
#             'schedule': crontab(minute=0, hour=0),
#             'args': (16, 16),
#         },
#     }





def crea_diccionario(fxf):
    acreedor = Eco.objects.get(fxf=fxf)
    eqps = Equipo.objects.filter(fxf_eqp=acreedor)
    facs = Facturas.objects.last()
    items = []
    total_equipos = 0
    cayds = 0
    for equipo in eqps:
        if not equipo.patrocinado:
            cayds = cayds + equipo.cantidad
        total_equipos = total_equipos + equipo.cantidad
        invent = equipo.modelo_eqp
        TP = D('0.01')
        base = D(equipo.subtotal_equipo/1.16).quantize(TP)
        ti = (base * D('0.16')).quantize(TP)
        tt = base + ti
        concepto = {
          "ProductCode": str(invent.codigo_producto)[:8],
          "IdentificationNumber": str(equipo.modelo_eqp),
          "Unit": "PIEZA",
          "UnitCode": "H87",
          "UnitPrice": str((base/equipo.cantidad).quantize(TP)),
          "Quantity":  equipo.cantidad,
          "Subtotal":  str(base),
          "Taxes": [
            {
              "Total": str(ti.quantize(TP)),
              "Name": "IVA",
              "Base":  str(base),
              "Rate": 0.16,
              "IsRetention": "False"
            }
          ],
          "Total":  str(tt)
        }
        if equipo.numero_serie:
            concepto["Description"] = invent.descripcion + ' ' + 'No. de Serie: ' + equipo.numero_serie
        else:
            concepto["Description"] = invent.descripcion
        items += [concepto]
        if equipo.gastos_inst and equipo.gastos_inst > 0:
            TP = D('0.01')
            base = (equipo.gastos_inst / D(1.16)).quantize(TP)
            ti = (base * D('0.16')).quantize(TP)
            tt = base + ti
            up = (base / equipo.cantidad).quantize(Decimal('1.00'))
            stt = base
            inst = {
                "ProductCode": "72151207",
                "Description": "Gastos de instalación",
                "IdentificationNumber": "Gastos de instalación.",
                "Unit": "SERVICIO",
                "UnitCode": "E48",
                "UnitPrice": str( up ),
                "Quantity": equipo.cantidad,
                "Subtotal": str( stt ),
                "Taxes": [
                    {
                        "Total": str(ti),
                        "Name": "IVA",
                        "Base": str(base),
                        "Rate": 0.16,
                        "IsRetention": "False"
                    }
                ],
                "Total": str(tt)
            }
            items += [inst]
    if cayds > 0:
        TP = D('0.01')
        base = D((425 / 1.16) * cayds).quantize(TP)
        ti = (base * D('0.16')).quantize(TP)
        tt = base + ti
        cayd = {
            "ProductCode": "76121501",
            "Description": "Centro de Acopio y Destrucción",
            "IdentificationNumber": "CAYD 2018",
            "Unit": "SERVICIO",
            "UnitCode": "E48",
            "UnitPrice": str(D((425 / 1.16)).quantize(TP)),
            "Quantity": cayds,
            "Subtotal": str(D((425/1.16) * cayds).quantize(TP)),
            "Taxes": [
                {
                    "Total": str(ti),
                    "Name": "IVA",
                    "Base": str(base),
                    "Rate": 0.16,
                    "IsRetention": "False"
                }
            ],
            "Total": str(tt)
            }
        base = D((250 / 1.16) * cayds).quantize(TP)
        ti = (base * D('0.16')).quantize(TP)
        tt = base + ti
        transporte = {
            "ProductCode": "80161600",
            "Description": "Transporte",
            "IdentificationNumber": "T 2018",
            "Unit": "SERVICIO",
            "UnitCode": "E48",
            "UnitPrice": str(D((250 / 1.16)).quantize(TP)),
            "Quantity": cayds,
            "Subtotal": str(D((250/1.16) * cayds).quantize(TP)),
            "Taxes": [
                {
                    "Total": str(ti),
                    "Name": "IVA",
                    "Base": str(base),
                    "Rate": 0.16,
                    "IsRetention": "False"
                }
            ],
            "Total": str(tt)
            }
        items += [cayd, transporte]
    receptor = {
        "Rfc": acreedor.rfc,
        "Name": acreedor.nombre_beneficiario,
        "CfdiUse": "G03",
        "Address": {
              "Street": acreedor.calle,
              "ExteriorNumber": acreedor.numero_exterior,
              "ZipCode": acreedor.codigo_postal,
              "State": acreedor.estado,
              "Country": "MEXICO"
        }
    }
    if acreedor.numero_interior:
        receptor["Address"]["InteriorNumber"] = acreedor.numero_interior
    if acreedor.colonia and acreedor.colonia != 'AÑADIR VALOR':
        receptor["Address"]["Neighborhood"] =  acreedor.colonia,
    if acreedor.delegacion_municipio and acreedor.delegacion_municipio != 'AÑADIR VALOR':
        receptor["Address"]["Locality"] = acreedor.delegacion_municipio


    diccionario_cfdi_lite = {
          "NameId": 1,
          "Date": unicode(datetime.date.today()),
          "Serie": "ECOLUZ18",
          "Folio": "1",
          "PaymentAccountNumber": "0533",
          "PaymentBankName": "HSBC",
          "Currency": "MXN",
          "ExpeditionPlace": "06700",
          "PaymentConditions": "CONTADO",
          "CfdiType": "I",
          "PaymentForm": "03",
          "PaymentMethod": "PUE",
          # "Issuer": {
          #   "FiscalRegime": "601",
          #   "Rfc": "SER140425PN3",
          #   "Name": "Soluciones Empresariales Rentables S.A.P.I. de C.V.",
          #   "TaxAddress": {   "Street": "Puebla",
          #                     "ExteriorNumber": "387",
          #                     "InteriorNumber": "Piso 2",
          #                     "Neighborhood": "Roma Norte",
          #                     "ZipCode": "06700",
          #                     "Municipality": "CUAUHTÉMOC",
          #                     "State": "CIUDAD DE MÉXICO",
          #                     "Country": "MEXICO"
          #                 },
          #   },
          "Receiver": receptor,
          "Items": items,
          "Observations": "Programa PAEEEM. Forma de pago: Transferencia Cuenta Bancaria: 0533 " + '\n FXF: ' + acreedor.fxf,
    }
    return diccionario_cfdi_lite

def factu2():
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic R3NlcmZpbmFuemFzOkVjb0RhcjE0Lg==",
        'Cache-Control': "no-cache"
    }
    url_prods = "https://apisandbox.facturama.mx/api/Product"
    dict = services.nvo_diccionario(fxf)
    r = requests.post(url_prods, data=json.dumps(dict), headers=head)
    print r, r.text.encode('utf-8')
    json_data = r.json()


def nvo_diccionario_prod(fxf):
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    acreedor = Eco.objects.get(fxf=fxf)
    eqps = Equipo.objects.filter(fxf_eqp=acreedor)
    items = []
    total_equipos = 0
    cayds = 0
    TP = D('0.01')
    for equipo in eqps:
        if not equipo.patrocinado:
            cayds = cayds + 1
        total_equipos = total_equipos + 1
        invent = equipo.modelo_eqp
        TP = D('0.01')
        base = D(equipo.subtotal_equipo / 1.16).quantize(TP)
        ti = (base * D('0.16')).quantize(TP)
        tt = base + ti
        if invent.id_facturama is None:
            xxx = {
                "UnitCode": "H87",
                "Unit": "Pieza",
                "IdentificationNumber": str(equipo.modelo_eqp),
                "Name": str(equipo.modelo_eqp),
              #  "Description": equipo.modelo_eqp.descripcion,
                "Price": str(D(equipo.modelo_eqp.precio_lista).quantize(TP)),
                "CodeProdServ": str(equipo.modelo_eqp.codigo_producto),
                "Taxes": [
                    {
                        "Total": str(ti.quantize(TP)),
                        "Name": "IVA",
                        "Base": str(base),
                        "Rate": 0.16,
                        "IsRetention": "False"
                    }
                ],
                "Total": str(tt)}
            if equipo.numero_serie:
                xxx["Description"] = invent.descripcion + ' ' + 'No. de Serie: ' + equipo.numero_serie
            else:
                xxx["Description"] = invent.descripcion
            # items += [concepto]
            print equipo.modelo_eqp.codigo_producto
            url_prods = "https://apisandbox.facturama.mx/api/Product"
            r = requests.post(url_prods, data=json.dumps(xxx), headers=head)
        else:
            urlget = 'https://apisandbox.facturama.mx/api/Product/' + invent.id_facturama
            r = requests.get(urlget, headers=head)
        if r.status_code == 200:
            setattr(invent, 'id_facturama', r['Id'])
            invent.save()
            print 200, r.text.encode('utf-8')
        else:
            print r.text.encode('utf-8')
    return r.json()


def nvo_diccionario_cli(fxf):
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    acreedor = Eco.objects.get(fxf=fxf)
    receptor = {
        "Rfc": acreedor.rfc,
        "Name": acreedor.nombre_beneficiario,
        "Email": acreedor.email,
        "CfdiUse": "G03",
        "Address": {
            "Street": acreedor.calle,
            "ExteriorNumber": acreedor.numero_exterior,
            "InteriorNumber": acreedor.numero_interior,
            "Neighborhood": acreedor.colonia,
            "ZipCode": acreedor.codigo_postal,
            "Locality": acreedor.delegacion_municipio,
            "State": acreedor.estado,
            "Country": "MEXICO"
            }
    }
    url_cli = 'https://apisandbox.facturama.mx/api/Client'
    rc = requests.post(url_cli, data=json.dumps(receptor), headers=head)
    print rc.text.encode('utf-8') # pk = 3791
    return rc.json()




def nvo_diccionario(fxf):
    acreedor = fxf
    diccionario_cfdi_lite = {
          "NameId": 1,
          "Date": unicode(datetime.date.today()),
          "Serie": "ECOLUZ18",
          "Folio": 1,
        #  "Folio": float(facs.folio) + 1,
          "PaymentAccountNumber": "0533",
          "Currency": "MXN",
          "ExpeditionPlace": "06700",
          "PaymentConditions": "CONTADO",
          "CfdiType": "I",
          "PaymentForm": "03",
          "PaymentMethod": "PUE",
          # "Issuer": {
          #   "FiscalRegime": "601",
          #   "Rfc": "SER140425PN3",
          #   "Name": "Soluciones Empresariales Rentables S.A.P.I. de C.V.",
          #   "TaxAddress": {   "Street": "Puebla",
          #                     "ExteriorNumber": "387",
          #                     "InteriorNumber": "Piso 2",
          #                     "Neighborhood": "Roma Norte",
          #                     "ZipCode": "06700",
          #                     "Municipality": "CUAUHTÉMOC",
          #                     "State": "CIUDAD DE MÉXICO",
          #                     "Country": "MEXICO"
          #                 },
          #  },
          "Receiver": nvo_diccionario_cli(fxf),
          "Items": nvo_diccionario_prod(fxf),
          "Observations": "Programa PAEEEM. Forma de pago: Transferencia Cuenta Bancaria: 0533 " + '\n FXF: ' + acreedor.fxf,
    }
    cayd_id = 'ZDl2-7du261yRskva091YA2'
    transp_id = '3m9asoF6XsFQgpqRYU9Pbg2'
    instala = {
        "Id": "mP1iSMt0QcsJjnJISRrcZg2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "Gastos",
        "Name": "Gastos de Instalacion",
        "Description": "Gastos de Instalacion",
        "Price": 27000,
        "CodeProdServ": "80161600",
        "NameCodeProdServ": "Supervisión de instalaciones de negocios",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    }

    return diccionario_cfdi_lite


def crea_factura(fxf):
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    url = 'https://apisandbox.facturama.mx/api/2/cfdis'
    dict = crea_diccionario(fxf)
    r = requests.post(url, data=json.dumps(dict), headers=head)
    cfdi = r.json()
    print cfdi
    return cfdi


def crea_factura11(dict):
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    url = 'https://apisandbox.facturama.mx/api/2/cfdis'
    r = requests.post(url, data=json.dumps(dict), headers=head)
    cfdi = r.json()
    print cfdi
    return cfdi


def dict_suc():
    head = {
        'Content-Type': "application/json; charset=utf-8",
        'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
        'Cache-Control': "no-cache"
    }
    url = 'https://apisandbox.facturama.mx/api/BranchOffice'
    dict = {
      "Name": "Grupo SER",
      "Description": "TORRE MAYOR",
      "Address": {
      "Street": "Puebla",
       "ExteriorNumber": "387",
       "InteriorNumber": "Piso 2",
       "Neighborhood": "Roma Norte",
       "ZipCode": "06700",
       "Municipality": "CUAUHTÉMOC",
       "State": "CIUDAD DE MÉXICO",
       "Country": "MEXICO"
                       }
}
    r = requests.post(url, data=json.dumps(dict), headers=head)
    cfdi = r.json()
    print r, r.text.encode('utf-8'), cfdi
    ser = {
        "IdBranchOffice": "iuWolZjkM7uDTF4zfGa0SA2",
        "Name": "ECOLUZ18",
        "Description": "Primera serie ECO en línea",
        "Folio": 1
    }
    url = 'https://apisandbox.facturama.mx/api/serie/iuWolZjkM7uDTF4zfGa0SA2'
    t = requests.post(url, data=json.dumps(ser), headers=head)
    cfdis = t.json()
    print t, t.text.encode('utf-8'), cfdis







########  EJEMPLOS QUE FUNCIONAN


# def facturacionv(request, fxf):
#     datas = {
#           "Serie": "CHIDO",
#           "Currency": "MXN",
#           "ExpeditionPlace": "06100",
#           "PaymentConditions": "CREDITO A SIETE DIAS",
#           "Folio": "102",
#           "CfdiType": "I",
#           "PaymentForm": "03",
#           "PaymentMethod": "PUE",
#           "Issuer": {
#             "FiscalRegime": "601",
#             "Rfc": "SER140425PN3",
#             "Name": "Soluciones Empresariales Rentables S.A.P.I. de C.V.",
#             "TaxAddress": {"Street": "Puebla",
#                            "ExteriorNumber": "387",
#                            "InteriorNumber": "Piso 2",
#                            "Neighborhood": "Roma Norte",
#                            "ZipCode": "06700",
#                            "Municipality": "CUAUHTEMOC",
#                            "State": "CIUDAD DE MEXICO",
#                            "Country": "MEXICO"
#                              },
#           },
#           "Receiver": {
#             "Rfc": "SME111110NY1",
#             "Name": "SinDelantal Mexico",
#             "CfdiUse": "P01"
#           },
#           "Items": [
#             {
#               "ProductCode": "10101504",
#               "IdentificationNumber": "EDL",
#               "Description": "Estudios de laboratorio",
#               "Unit": "NO APLICA",
#               "UnitCode": "MTS",
#               "UnitPrice": 1.0,
#               "Quantity": 1.0,
#               "Subtotal": 1.0,
#               "Taxes": [
#                 {
#                   "Total": 0.16,
#                   "Name": "IVA",
#                   "Base": 1.0,
#                   "Rate": 0.16,
#                   "IsRetention": "False"
#                 }
#               ],
#               "Total": 1.16
#             },
#             {
#               "ProductCode": "10101505",
#               "IdentificationNumber": "001",
#               "Description": "SERVICIO DE COLOCACION",
#               "Unit": "NO APLICA",
#               "UnitCode": "E49",
#               "UnitPrice": 100.0,
#               "Quantity": 15.0,
#               "Subtotal": 1500.0,
#               "Taxes": [
#                 {
#                   "Total": 240.0,
#                   "Name": "IVA",
#                   "Base": 1500.0,
#                   "Rate": 0.16,
#                   "IsRetention": "False"
#                 }
#               ],
#               "Total": 1740.0
#             }
#           ],
#         }
#     csds = {
#         "Rfc" : "SER140425PN3",
#         "Certificate": "MIIGajCCBFKgAwIBAgIUMDAwMDEwMDAwMDA0MDg2NjkxNzIwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTcxMjIwMTg1OTQxWhcNMjExMjIwMTg1OTQxWjCCAQkxOTA3BgNVBAMTMFNPTFVDSU9ORVMgRU1QUkVTQVJJQUxFUyBSRU5UQUJMRVMgUyBBIFAgSSBERSBDVjE5MDcGA1UEKRMwU09MVUNJT05FUyBFTVBSRVNBUklBTEVTIFJFTlRBQkxFUyBTIEEgUCBJIERFIENWMTkwNwYDVQQKEzBTT0xVQ0lPTkVTIEVNUFJFU0FSSUFMRVMgUkVOVEFCTEVTIFMgQSBQIEkgREUgQ1YxJTAjBgNVBC0THFNFUjE0MDQyNVBOMyAvIEdBTEo4OTAxMDZURTQxHjAcBgNVBAUTFSAvIEdBTEo4OTAxMDZIREZSUEwwMzEPMA0GA1UECxMGVU5JREFEMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmYAITAzQb-oDM0QRCQuk5boDyjgMV2MjK9rBmsjqlqN-5urBVI1ltjG8N5sk9m3lTfveVZCjBGzde3xwHK2FC_3kzznzhnulr_Q9GfEm5aknqlFAGi0XwZOKW1IxKxHfMsqSMyGL8qLMdj-aN0b6PhrINDrsRyfrHN2BoGRm2dMMah50Zvmi4JZPS_0dDf9XjAKXfxco2bfKPKQH81-fxLGg3abd1lnIzDF_QOdJ9VzXC4MYu0pH-xpZ4yU_GyqtUieYd6S4k3xWNu7MYsw7OwFMDnVOHMl1A8rRaCaN9SIvom2sPmfh6qWQZGAcCv1BdE7r-O7UsVLmWyvEfLSlWwIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQsFAAOCAgEAYEg8JWjVxdWDgg4F2eUN3tKoMG32HeHX2AwbAr8zVL1oWfhuF4XFhVGtpF3e8OFCOH-kT9cew5c4VXFU4l2XF_AT-QZ9wzFRKbSamcxm1zqYi0vp8DAgLAjl_XoSRLFUjUUINIYDDevOzuX_wVUoPGMEMvPktJaajMczhv1B-3_cQ9nWRiGNsCre8nF9prNs6UdCGL7L9hjRQUP9BXsSAy8-qtvV9iO3zgRxK__4TQn2V27Im6gdOneagMPFjQuUoYJp0DBjJTZWJgkwBK_Rb-ip6lwnjxqIr62oJq3Kurg7MAY-whGrHb6HfxLyA3n_Vn3KosyQUe9ubmHhKJvznKeQyOIa1EuuSetObFXgg6PmZcZm970x2ZhtX0FuT-tZuA6qkKuqBGmzzg6ErqxhGA4g4eU6D_QIfCeRxz-oc90FDB_KICT4zQS8Fp53p34VglRLDTJqIchXzvSC6PsrtmvLs2I1O0H6rvW2dess4wQDvTNoibxwkuMTHzTt1Jt9j1i-vMWW_vH6aMcsQJ0b_9RXV72vmIA4EcJD9DQbzjwvEv3qTcO7P-rOSFMOAMq1ldlgxb7fy2C_PgBUPmIMqhtPNeP7akECYI2frL8WPnZrmt33vfPRFl6klv57sCFptrZwnzmULDA62RPe0dXVQ7mOynXXbKexX3XK7DrMKgM", # FF
#         "PrivateKey": "MIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQIAgEAAoIBAQACAggAMBQGCCqGSIb3DQMHBAgwggS9AgEAMASCBMhBEAjE-LoNsK0NNYCl8XmjwIZ2HMCZq82dJaw3OD0VwY8Uz8kucYrCf-6dbrveBpvzI0Fz3lHAZC29r1cM3F5cuapK690HFSzS_KS372kga0jRUTflhyaI8v41wDZq8Od1WUAq17f9u8OfJJEStsv5unHMq1K-JE-nA4pPJBYNirSLR0bIzf9RVwiHvrrfMQigbM7PdC37M4TdXa7V3SSioBj2zaiGR8q6yE_JsAMDqebcBMDiPdeSAUJ80_kGfGiAxQYJF6QYGXldW4hzFnW1FvraOWZRBIOD58w6QFsZQmtwu73lKPOZIGBW5iOux6gqu4vqq0pEZwQRA9R3U5x9HAlhNIiJjSKuXBFL-CMbyHVyVPP6NYg7W3m6ml9aXOa6ZKMlOnnyWCH1A0u9rLKme0lplod0gO7RJiwgGnaosDsY1M57Bt8u4wPSEriUyATOQ78oOBwBAqCxkVlbSF1WpiYLCq7Nh8JV9RFmkXqXgLZo7CiioQBpgM48_g7RpRpQsPvYhPPQxzit56vseF5PcTlJSAHm4cB8txGeUdBcyRMDk12qsOdmsxd9mIdYBR1gphabZMbBBvCqz95SIewh4KcOIyYhYQqj7zkM-oHER4hYRvoh-jSKG2uhvvatzjHNiOyEZAZOi2Srxzr4lLDlWtmLoD2LXIAfGa5uieF-QwSm6F7oMii14OO7c-KOGhPFCcT7NfmmJ_Kw2bDTF3wE5WL-jNTio3T73Yq9bOo8gt07kIrdRccrZvP5sYAvA_V7RAEc7HOi4PWS7V7RYcQEAypBVtF_UDIo8WkwcprplPs_2ucXjCKvqB3cA2xCzp7EvsENrgQu3Kyv7FA3IY9qLmJcohMSNCTWfOms4MpUHoCULmtYAinidXxgbUebp78RhHKiI_NeL4atFqcSpku-i6NZ11-Q25v5PUDIqDSspoQWZXpTexTdps-aDOq53SRW6q45ZUPfIn6Sxv9ljsRMsEbXdYAjVnfrudWXGDm-_m0m-tGfU_tbPIw4omU-azxbvYk25s0WAKFKgYr-DLJezUj_TBBerYglA3Hl0WMaw44Ph0qbVDY-1uGiOgz3JKernsDbnb_g91gTzzVVG3769Rx_AKnapKYYT8U_VOlpiMEE3jB4urxbx9aq7LrOohFtJTbtdhQQdIzEFA-mDVImEoUqrxSka5zZH1sE2MHq0g6LCtItyALo4MzyvTSp0lSNz1TzWv1YsPgP3aU6kSn9N4zK_nFuZeR4qMPxo5wohUdVqQ0BdiXtSEWfvlqN_t9384tLPiRF44zfQElrJQyX5qs5CDBUxuUb9ovGrfV1KLkXvchbVy6KvdeHIAg24KG8e2QnFQ9jUYy3F9dQguxbLqvPcuapBXzMPwxX-OEJAYFTurXTl7O3vcxcd_qflIopZmfI-f5OeVGTODQXGSA7vxL2YOmhOrpaOVZnC9MVOHNT9Zwa61GwMKBYc3LK6ehQGLpsUmrIs987wefXCBkBPvSdQRPLdAYangyuqn_ArXMeIBpoMnwJJpziX_ni_31RVh5RH0MmFtw0NpONoSTZMozWX0RQG3jLsGl1-g-rqCyJ70Wt5IzI7tZiHcTamMDbU9xBTcjyuQPrULMuzpscflf6R6S_wxU", # FF
#         "PrivateKeyPassword" : "Gruposer18"
#     }
#     urlcsd = 'https://apisandbox.facturama.mx/api-lite/csds'
#     head = {
#         'Content-Type': "application/json; charset=utf-8",
#         'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
#         'Cache-Control': "no-cache"
#     }
#     url = "https://apisandbox.facturama.mx/api-lite/2/cfdis"
#     # dict = services.crea_diccionario(fxf)
#     # s = requests.post(urlcsd,  data=csds,  headers=head)
#     # if s.status_code == 200:
#     #     print s
#     #     r = requests.post(url, data=data, headers=head)
#     #     print r.status_code, r.text.encode('utf-8')
#     #     json_data = r.json()
#     # else:
#     #     print s.text
#     #     json_data = s.text
#     urlcsd = "https://apisandbox.facturama.mx/api-lite/csds/SER140425PN3"
#     r = requests.post(url, data=json.dumps(datas), headers=head)
#     get = requests.request("GET", urlcsd, headers=head)
#     print r, r.text.encode('utf-8')
#     json_data = r.json()
#     urf = "https://www.api.facturama.com.mx/Help/Api/GET-api-Cfdi-pdf-issuedLite-"+r.json()["Id"]
#     urx = "https://www.api.facturama.com.mx/Help/Api/GET-api-Cfdi-xml-issuedLite-"+r.json()["Id"]
#     xml = wget.download(urx)
#     a = Eco.objects.get(fxf=fxf)
#     setattr(a, 'factura_ecoluz', xml)
#     a.save()
#     print a.factura_ecoluz
#     return render(request, 'ecoclientes/factura.html', {'data':json_data, 'link': urf }) # 'dict':data})
#
#
# def facturacionn(request, fxf):
#     url = "https://apisandbox.facturama.mx/api-lite/2/cfdis"
#
#     payload = "{\n  \"Serie\": \"CHIDO\",\n  \"Currency\": \"MXN\",\n  \"ExpeditionPlace\": \"06100\",\n  \"PaymentConditions\": \"CREDITO A SIETE DIAS\",\n  \"Folio\": \"101\",\n  \"CfdiType\": \"I\",\n  \"PaymentForm\": \"03\",\n  \"PaymentMethod\": \"PUE\",\n  \"Issuer\": {\n    \"FiscalRegime\": \"601\",\n    \"Rfc\": \"SER140425PN3\",\n    \"Name\": \"Soluciones Empresariales Rentables S.A.P.I. de C.V.\",\n    \"TaxAddress\": {\"Street\": \"Puebla\",\n                   \"ExteriorNumber\": \"387\",\n                   \"InteriorNumber\": \"Piso 2\",\n                   \"Neighborhood\": \"Roma Norte\",\n                   \"ZipCode\": \"06700\",\n                   \"Municipality\": \"CUAUHTEMOC\",\n                   \"State\": \"CIUDAD DE MEXICO\",\n                   \"Country\": \"MEXICO\"\n                     },\n  },\n  \"Receiver\": {\n    \"Rfc\": \"SME111110NY1\",\n    \"Name\": \"SinDelantal Mexico\",\n    \"CfdiUse\": \"P01\"\n  },\n  \"Items\": [\n    {\n      \"ProductCode\": \"10101504\",\n      \"IdentificationNumber\": \"EDL\",\n      \"Description\": \"Estudios de laboratorio\",\n      \"Unit\": \"NO APLICA\",\n      \"UnitCode\": \"MTS\",\n      \"UnitPrice\": 1.0,\n      \"Quantity\": 1.0,\n      \"Subtotal\": 1.0,\n      \"Taxes\": [\n        {\n          \"Total\": 0.16,\n          \"Name\": \"IVA\",\n          \"Base\": 1.0,\n          \"Rate\": 0.16,\n          \"IsRetention\": \"False\"\n        }\n      ],\n      \"Total\": 1.16\n    },\n    {\n      \"ProductCode\": \"10101505\",\n      \"IdentificationNumber\": \"001\",\n      \"Description\": \"SERVICIO DE COLOCACION\",\n      \"Unit\": \"NO APLICA\",\n      \"UnitCode\": \"E49\",\n      \"UnitPrice\": 100.0,\n      \"Quantity\": 15.0,\n      \"Subtotal\": 1500.0,\n      \"Taxes\": [\n        {\n          \"Total\": 240.0,\n          \"Name\": \"IVA\",\n          \"Base\": 1500.0,\n          \"Rate\": 0.16,\n          \"IsRetention\": \"False\"\n        }\n      ],\n      \"Total\": 1740.0\n    }\n  ],\n}"
#     headers = {
#         'Content-Type': "application/json; charset=utf-8",
#         'Authorization': "Basic Z3NlcnNpc3RlbWFzOkVjb3RlY2gxNy4=",
#         'Cache-Control': "no-cache"
#     }
#
#     response = requests.request("POST", url, data=payload, headers=headers)
#     a = response.text.encode('utf-8')
#     print(response.text.encode('utf-8'))
#     return render(request, 'ecoclientes/factura.html', {'data': a, }) # 'dict':data})





