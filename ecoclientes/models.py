# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import RegexValidator
from usuarios.models import Usuario
from datetime import date, time, datetime
from django.db.models import Q

# Create your models here.


def ecocliente_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'ecoclientes/ecocliente_{0}/{1}'.format(instance.id, filename)


class Plaza(models.Model):

    ciudad_choices = (
         ('ACA', 'Acapulco'),
         ('AGU', 'Aguascalientes'),
         ('MET', 'Área Metropolitana'),
         ('CPE', 'Campeche'),
         ('CUN', 'Cancún'),
         ('CJS', 'Cd. Juárez'),
         ('COB', 'Cd. Obregón'),
         ('CVA', 'Cd. Valles'),
         ('CVM', 'Cd. Victoria'),
         ('CUU', 'Chihuahua'),
         ('CHG', 'Chilpancingo'),
         ('CTZ', 'Coatzacoalcos'),
         ('COL', 'Colima'),
         ('CVJ', 'Cuernavaca'),
         ('CUL', 'Culiacán'),
         ('DGO', 'Durango'),
         ('GDL', 'Guadalajara'),
         ('HMO', 'Hermosillo'),
         ('IRA', 'Irapuato'),
         ('GTO', 'León'),
         ('MAZ', 'Mazatlán'),
         ('MID', 'Mérida'),
         ('MXL', 'Mexicali'),
         ('MTY', 'Monterrey'),
         ('MOR', 'Morelia'),
         ('OAX', 'Oaxaca'),
         ('PCA', 'Pachuca'),
         ('PAZ', 'Poza Rica'),
         ('PUE', 'Puebla'),
         ('PVR', 'Puerto Vallarta'),
         ('QRO', 'Querétaro'),
         ('SCX', 'Salina Cruz'),
         ('SAL', 'Saltillo'),
         ('SLP', 'San Luis Potosí'),
         ('TAM', 'Tampico'),
         ('NAY', 'Tepic'),
         ('TIJ', 'Tijuana'),
         ('TLX', 'Tlaxcala'),
         ('TLC', 'Toluca'),
         ('TRC', 'Torreón'),
         ('TGZ', 'Tuxtla Gutiérrez'),
         ('UPN', 'Uruapan'),
         ('VER', 'Veracruz'),
         ('VSA', 'Villahermosa'),
         ('XAL', 'Xalapa'),
         ('ZAM', 'Zamora')
    )

    DAR = models.ForeignKey(Usuario, on_delete=models.CASCADE, ) #  limit_choices_to=Q('DAR_id'!= '') )
    ciudad = models.CharField(_('Ciudad'), choices=ciudad_choices, max_length=25, blank=True, null=True)
    estado = models.CharField(_('Estado'),  max_length=25, blank=True, null=True)
    clave = models.CharField(_('Clave'),  max_length=3, blank=True, null=True)

    def __unicode__(self):
        return getattr(self.DAR, 'DAR_id')+'-'+str(self.ciudad)

    class Meta:
        ordering = ['DAR']








class Eco(models.Model):
    Estatus_CHOICES = (
        ('EN VALIDACIÓN', 'EN VALIDACIÓN'),
        ('PENDIENTE', 'PENDIENTE'),
        ('PAGADO', 'PAGADO'),
        #('TRASPASO', 'TRASPASO'),
        #('CANCELADO SIN FONDEO', 'CANCELADO SIN FONDEO'),
        ('CANCELADO', 'CANCELADO'),
        ('POR FONDEAR', 'POR FONDEAR'),
        #('ADEUDO', 'ADEUDO'),
        #('INVENTARIO', 'INVENTARIO'),
        #('COM DAR Pendiente', 'COM DAR Pendiente'),
        ('COM DAR CxP a hoy', 'COM DAR CxP a hoy'),
        ('LIBERADO', 'LIBERADO')   )

    # DAR_CHOICES = ( # list(set(a).union(b))
    #     ('PRF-IRA', 'PRF-IRA'),
    #     ('PRF-PUE', 'PRF-PUE'),
    #     ('PRF-SLP', 'PRF-SLP'),
    #     ('SOP-DUR', 'SOP-DUR'),
    #     ('SOP-ZAC', 'SOP-ZAC'),
    #     ('GMC-SIN', 'GMC-SIN'),
    #     ('MCM-MTY', 'MCM-MTY'),
    #     ('MCM-SAL', 'MCM-SAL'),
    #     ('JAM-TOL', 'JAM-TOL'),
    #     ('CNG-TAM', 'CNG-TAM'),
    #     ('EMR-NAY', 'EMR-NAY'),
    #     ('MFV-MTY', 'MFV-MTY'),
    #     ('TCU-MER', 'TCU-MER'),
    #     ('FIT-MTY', 'FIT-MTY'),
    #     ('BHA-XAL', 'BHA-XAL'),
    #     ('POM-GTO', 'POM-GTO'),
    #     ('BPC-HGO', 'BPC-HGO'),
    #     ('AVA-MET', 'AVA-MET'),
    #     ('CGL-MET', 'CGL-MET'),
    #     ('VNT-MET', 'VNT-MET'),
    # )

    tipo_venta_CHOICES = (
        ('PAEEEM', 'PAEEEM'),
        ('CONAVI', 'CONAVI'),
        ('ProveCapital', 'ProveCapital'),
        ('Credijusto', 'Credijusto'),
        ('Afirme', 'Afirme'),
        ('Contado', 'Contado'),)


    tecnologia_CHOICES = (
        ('Refrigerción Comercial', 'Refrigerción Comercial'),
        ('Cámaras de Refrigeración', 'Cámaras de Refrigeración'),
        ('Sistemas Fotovoltaicos', 'Sistemas Fotovoltaicos'),
        ('Aires Acondicionados', 'Aires Acondicionados'),
        ('Luminarias', 'Luminarias'),
        ('Calentadores Solares', 'Calentadores Solares'),
        ('Mejora Estructura', 'Mejora Estructura'),
    )

    fxf = models.CharField(_('FxF'), max_length=16, blank=True, null=True)
    DAR = models.CharField(_('DAR') , max_length=7,  blank=True)
    tipo_venta = models.CharField(_('Tipo de venta') , max_length=25, blank=True, choices=tipo_venta_CHOICES)
    numero_credito = models.CharField(_('Número de crédito'), max_length=20, blank=True)
    first_name = models.CharField(_('Nombre') , max_length=45, blank=True)
    last_name = models.CharField(_('Apellido paterno') , max_length=25, blank=True)
    last_last_name = models.CharField(_('Apellido materno') , max_length=25, blank=True)
    rfc = models.CharField(_('RFC') , max_length=25, blank=True)
    nombre_beneficiario = models.CharField(_('Nombre del Beneficiario'), max_length=80, blank=True, null=True)
    estatus = models.CharField(_('Estatus'), max_length=25, choices=Estatus_CHOICES, default='EN VALIDACION', blank=True)
    fecha_pago = models.DateField(_('Fecha de pago'), blank=True, null=True)
    fecha_compra = models.DateField(_('Fecha de compra'), blank=True, null=True )
    factura_ecoluz_n = models.CharField(_('Factura Eco Luz'),  max_length=200, blank=True, null=True)
    factura_ecoluz = models.FileField(_('Factura Eco Luz.xml'), upload_to=ecocliente_directory_path, blank=True, null=True)
    factura_costos_dar_n = models.CharField(_('Factura Costos DAR'),  max_length=200, blank=True, null=True)
    factura_costos_dar = models.FileField(_('Factura Costos DAR.xml'), upload_to=ecocliente_directory_path, blank=True, null=True)
    subtotal_pago = models.CharField(_('Subtotal de pago'), max_length=20, blank=True, null=True)
    cayd_cobrado = models.CharField(_('CAyD'), max_length=15, blank=True, null=True)

    gastos_inst = models.CharField(_('Gastos de instalación'), max_length=20, blank=True, null=True)
    eqp = models.FloatField(_('EQP'), max_length=15, blank=True, null=True)
    reguladores = models.FloatField(_('Instalación'), max_length=15, blank=True, null=True)
    mdm = models.FloatField(_('MDM'), max_length=15, blank=True, null=True)
    com = models.FloatField(_('COM'), max_length=15, blank=True, null=True)
    man = models.FloatField(_('MAN'), max_length=15, blank=True, null=True)

    monitoreo = models.FloatField(_('Sistema de Monitoreo'), max_length=15, blank=True, null=True)
    montaje = models.FloatField(_('Sistema de Montaje'), max_length=15, blank=True, null=True)

    suministro_instalacion = models.FloatField(_('Suministro de Instalación'), max_length=15, blank=True, null=True, default=0.0)
    estructura = models.FloatField(_('Estructura'), max_length=15, blank=True, null=True, default=0.0)
    inversor = models.FloatField(_('Inversor'), max_length=15, blank=True, null=True, default=0.0)
    microinversor = models.FloatField(_('Microinversor'), max_length=15, blank=True, null=True, default=0.0)

    cayd_pagado = models.FloatField(_('CAYD'), max_length=15, blank=True, null=True)

    subtotal_operacion = models.FloatField(_('Subtotal operación'), max_length=10, blank=True, null=True)
    financiamiento = models.FloatField(_('Financiamiento'), max_length=10, blank=True, null=True)
    costo_total_financiamiento = models.FloatField(_('Costo total financiamiento'), max_length=15, blank=True, null=True)
    pago_total = models.FloatField(_('Pago total'), max_length=15, blank=True, null=True)
    numero_dias = models.IntegerField(_('# Dias'), blank=True, null=True)
    com_dar = models.FloatField(_('COM DAR'), max_length=15, blank=True, null=True)
    adeudos = models.FloatField(_('Adeudos'), max_length=15, blank=True, null=True)
    descuento_f = models.FloatField(_('Descuentos'), max_length=15, default=0, blank=True, null=True)
    obs = models.TextField(_('Observaciones'), max_length=600, blank=True)
    fecha_mod = models.DateField(_('Última modificación'), auto_now=True, null=True)
    # Domicilio
    email = models.CharField('Correo electrónico', blank=True, null=True, max_length=50, default='NOHAY@valor.mx')
    telefono = models.CharField(_('Teléfono'), max_length=12, blank=True, null=True, default='AÑADIR')
    calle = models.CharField(_('Calle'), max_length=50, blank=True, null=True, default='AÑADIR VALOR')
    numero_exterior = models.CharField(_('Número exterior'), max_length=10, blank=True, null=True, default='AÑADIR')
    numero_interior = models.CharField(_('Número interior'), max_length=10, blank=True, null=True, default='AÑADIR')
    colonia = models.CharField(_('Colonia'), max_length=50, blank=True, null=True, default='AÑADIR VALOR')
    delegacion_municipio = models.CharField(_('Delegación o Municipio'), max_length=50, blank=True, null=True, default='AÑADIR VALOR')
    estado = models.CharField(_('Estado'), max_length=50, blank=True, null=True, default='AÑADIR VALOR')
    codigo_postal = models.IntegerField(_('Código postal'), blank=True, null=True, default=0)
    pais = models.CharField(_('País'), blank=True, null=True, max_length=20, default='MX')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    tecnologia = models.CharField(_('Tecnología'), max_length=50, blank=True, null=True, choices=tecnologia_CHOICES)
    solar = models.BooleanField(_('Solar'), default=0)


    def __unicode__(self):
        return str(self.fxf)

    class Meta:
        ordering = ['fxf']


class Inventario(models.Model):
    modelo = models.CharField(_('Modelo de equipo'), max_length=80, blank=True, null=True)
    precio_lista = models.DecimalField(_('Precio de lista'), max_digits=25, decimal_places=2,  blank=True, null=True)
    fabricante = models.CharField(_('Fabricante'), max_length=30, blank=True, null=True )
    descripcion = models.CharField(_('Descripción'), max_length=200, blank=True, null=True )
    codigo_producto = models.CharField(_('Código de producto'), max_length=20, blank=True, null=True)
    modalidad = models.CharField(_('Modalidad'), max_length=40, blank=True, null=True)
    id_facturama = models.CharField(_('ID facturama'), max_length=50, blank=True, null=True)
    litros_utiles = models.CharField(_('Litros útiles'), max_length=40, blank=True, null=True)
    unidad = models.CharField(_('Unidad'), max_length=20, blank=True, null=True)
    unit_code = models.CharField(_('Código de unidad'), max_length=40, blank=True, null=True)


    # marca = models.CharField(_('Fabricante'), max_length=30, blank=True, null=True )


    def __unicode__(self):
        return self.modelo

    class Meta:
        ordering = ['modelo']



class Equipo(models.Model):

    fxf_eqp = models.ForeignKey(Eco, on_delete=models.CASCADE, )
    patrocinado = models.BooleanField(_('EQP patrocinado'), default=0)
    modelo_equipo = models.CharField(_('Modelo Equipo'), max_length=100, blank=True, )
    modelo_eqp = models.ForeignKey(Inventario, on_delete=models.CASCADE, null=True)
    cantidad = models.IntegerField(_('Cantidad '), default=1, blank=True, null=True)
    subtotal_equipo = models.FloatField(_('Subtotal de pago'), max_length=20, blank=True, null=True)
    cayd_cobrado_e = models.FloatField(_('CAyD FIDE'), max_length=15, blank=True, null=True)
    numero_serie = models.CharField(_('# de serie '), max_length=200, blank=True)
    eqp_e = models.FloatField(_('EQP'), max_length=15, blank=True, null=True)
    reguladores_e = models.FloatField(_('Instalación'), max_length=15, blank=True, null=True)
    monitoreo_e = models.FloatField(_('Sistema de Monitoreo'), max_length=15, blank=True, null=True, default=0.0,)
    montaje_e = models.FloatField(_('Sistema de Montaje'), max_length=15, blank=True, null=True, default=0.0,)
    mdm_e = models.FloatField(_('MDM'), max_length=15, blank=True, null=True)
    com_e = models.FloatField(_('COM'), max_length=15, blank=True, null=True)
    man_e = models.FloatField(_('MAN'), max_length=15, blank=True, null=True)
    cayd_pagado_e = models.FloatField(_('CAYD pagado'), max_length=15, blank=True, null=True)

    gastos_inst = models.DecimalField(_('Gastos de Instalación'), max_digits=25, decimal_places=2, default=0.0,
                                      blank=True, null=True)

    suministro_instalacion_e = models.FloatField(_('Suministro de Instalación'), max_length=15, blank=True, null=True, default=0.0,)
    estructura_e = models.FloatField(_('Estructura'), max_length=15, blank=True, null=True, default=0.0,)
    inversor_e = models.FloatField(_('Inversor'), max_length=15, blank=True, null=True, default=0.0,)
    microinversor_e = models.FloatField(_('Microinversor'), max_length=15, blank=True, null=True, default=0.0,)


    subtotal_operacion_e = models.FloatField(_('Subtotal operación'), max_length=10, blank=True, null=True)
    financiamiento_e = models.FloatField(_('Financiamiento'), max_length=10, blank=True, null=True)
    costo_total_financiamiento_e = models.FloatField(_('Costo total financiamiento'), max_length=15, blank=True, null=True)
    pago_total_e = models.FloatField(_('Pago total'), max_length=15, blank=True, null=True)
    numero_dias_e = models.IntegerField(_('# Dias'), blank=True, null=True)
    com_dar_e = models.FloatField(_('COM DAR'), max_length=15, blank=True, null=True)
    factura_fab_n = models.CharField(_('Factura FAB'),  max_length=200, blank=True, null=True)
    factura_fab = models.FileField(_('Factura FAB.xml'), upload_to=ecocliente_directory_path, blank=True, null=True)
    factura_reg_n = models.CharField(_('Factura REG'),  max_length=200, blank=True, null=True)
    factura_reg = models.FileField(_('Factura REG.xml'), upload_to=ecocliente_directory_path, blank=True, null=True)
    adeudos_eq = models.FloatField(_('Adeudos eqp'), max_length=20, default=0.0, blank=True, null=True)
    descuentos = models.CharField(_('Descuentos eqp'), max_length=15, blank=True, null=True, default=0)
    obs_e = models.CharField(_('Observaciones del equipo'), max_length=600, blank=True)

    independiente = models.BooleanField(_('Facturación independiente'), default=0)
    fecha_compra_e = models.DateField(_('Fecha de compra'),  null=True, blank=True )
    numero_dias_e = models.IntegerField(_('# Dias eqp'), blank=True, null=True)
    factura_ecoluz_ne = models.CharField(_('Factura Eco Luz 1'), max_length=200, null=True)
    factura_ecoluz_e = models.FileField(_('Factura Eco Luz 1.xml'), upload_to=ecocliente_directory_path, blank=True, null=True)
    atrasos = models.CharField(_('Atrasos'), max_length=50, blank=True, null=True, )


    def __unicode__(self):
        return str(self.fxf_eqp) + ' eqp: ' + str(self.modelo_equipo)

    class Meta:
        ordering = ['fxf_eqp']


class Facturas(models.Model):
    tipo_CHOICES = (
        ('ecoluz', 'ecoluz'),
        ('dar', 'dar'),
        ('fab', 'fab'),
        ('reg', 'reg'))
    fxf_fac = models.ForeignKey(Eco, on_delete=models.CASCADE, )
    tipo = models.CharField(_('Tipo'), max_length=16, blank=True, choices=tipo_CHOICES)
    folio = models.CharField(_('Folio'), max_length=10, blank=True)
    folio_fiscal = models.CharField(_('Folio fiscal'), max_length=42, default='01234567890123456789012345678901'
                                    , validators=[
            RegexValidator(regex='^.{32}$', message='Campo debe contener 32 caracteres', code='nomatch')])
    total = models.FloatField(_('Total'), blank=True, null=True )
    subtotal = models.FloatField(_('Subtotal'), blank=True, null=True)
    no_items = models.IntegerField(_('# items'), blank= True, null=True, default=None)
    items = models.ManyToManyField(Inventario, blank=True, )
    pdf = models.FileField(_('Archivo pdf'), upload_to=ecocliente_directory_path, blank=True, null=True)
    xml = models.FileField(_('Archivo xml'), upload_to=ecocliente_directory_path, blank=True, null=True)

    def __unicode__(self):
        return self.folio + ' ' + self.tipo.upper()

    class Meta:
        verbose_name_plural = "Facturas"



class Inversionista(models.Model):
    persona = models.CharField(_('Persona'), max_length=70, blank=True, null= True)
    PRLV = models.CharField(_('PRLV'), max_length=30, blank=True, null= True)
    capital = models.FloatField(_('Capital'), max_length=30, blank=True, null= True)
    tasa_efectiva = models.FloatField(_('Tasa efectiva'), max_length=20, blank=True, null= True)
    tasa_efectiva_real = models.FloatField(_('Tasa efectiva real'), max_length=20, blank=True, null= True)
    tasa_anual = models.FloatField(_('Tasa anual'), max_length=20, blank=True, null= True)
    interes = models.FloatField(_('Interes'), max_length=20, blank=True, null= True)
    monto_deuda = models.FloatField(_('Monto Deuda'), max_length=20, blank=True, null= True)
    inicio = models.DateField(_('Inicio'), blank=True, null= True)
    termino = models.DateField(_('Termino'), blank=True, null= True)
    corte = models.DateField(_('Corte'), blank=True, null= True)
    corte1 = models.DateField(_('Segundo corte'), blank=True, null= True)
    corte2 = models.DateField(_('Tercer corte'), blank=True, null= True)
    dias = models.IntegerField(_('Días'), blank=True, null= True)
    intereses_hoy = models.FloatField(_('Intereses a hoy'), max_length=20, blank=True, null= True)
    folio_fiscales = models.CharField(_('Folio Fiscales'), max_length=100, blank=True, null= True)
    estatus_PRLV = models.CharField(_('Estatus PRLV'), max_length=40, blank=True, null= True)
    archivado = models.BooleanField(_('Archivado'), default=0)

    def __unicode__(self):
        return self.persona


class Flujo(models.Model):
    ventas_cxc_inventarios = models.CharField(_('Ventas en CxC + Inventarios'), max_length = 30, blank=True, null=True)
    banco = models.CharField(_('Banco'), max_length = 30, blank=True, null=True)
    total_entradas = models.CharField(_('Total entradas'), max_length = 30, blank=True, null=True)
    capital = models.CharField(_('Capital'), max_length = 30, blank=True, null=True)
    deuda = models.CharField(_('Deuda'), max_length = 30, blank=True, null=True)
    intereses_hoy = models.CharField(_('Intereses a hoy'), max_length = 30, blank=True, null=True)
    monto_deuda = models.CharField(_('Monto deuda'), max_length = 30, blank=True, null=True)
    cxp_comdar = models.CharField(_('CxP COM DAR'), max_length = 30, blank=True, null=True)
    cxp_fabricantes = models.CharField(_('CxP Fabricantes'), max_length = 30, blank=True, null=True)
    superhabit_deficit_hoy = models.CharField(_('Super habit / deficit a hoy'), max_length = 30, blank=True, null=True)
    superhabit_deficit_total = models.CharField(_('Super habit / deficit total'), max_length = 30, blank=True, null=True)
    hora_entrega = models.CharField(_('Hora de entrega'), max_length = 30, blank=True, null=True)
    puntuacion = models.CharField(_('Puntuación'), max_length = 30, blank=True, null=True)




class Concepto(models.Model):
    factura = models.ForeignKey(Facturas, on_delete=models.CASCADE)
    equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    noIdentificacion = models.CharField(_('No. Identificación'), max_length=25, blank=True, null=True)
    cantidad = models.IntegerField(_('Cantidad'), blank=True, null=True)
    descripcion = models.CharField(_('Descripción'), max_length=150, blank=True, null=True)
    unidad = models.CharField(_('Unidad'), max_length=10, blank=True, null=True)
    valorUnitario = models.FloatField(_('Valor Unitario'), max_length=25, blank=True, null=True)
    importe = models.FloatField(_('Importe'), max_length=25, blank=True, null=True)

    def __unicode__(self):
        return self.descripcion[:25]






