# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.contrib.auth.views import (
    login, logout, password_reset, password_reset_done, password_reset_confirm, password_reset_complete
)

from . import views
app_name = 'usuarios'
urlpatterns = [
    url(r'^acceso/$', login, {'template_name': 'usuarios/login.html'}, name='acceso'),
    url(r'^adios/$', logout, {'template_name': 'usuarios/logout.html'}, name='adios'),
    url(r'^registro/$', views.registro, name='registro'),
    url(r'^perfil/$', views.perfil, name='perfil'),
    url(r'^perfil/editar/$', views.editar_perfil, name='editar_perfil'),
    url(r'^cambiar-contrasena/$', views.cambiar_contrasena, name='cambiar_contrasena'),
    url(r'^reestablecer-contrasena/$', password_reset, {'template_name': 'usuarios/reestablecer-contrasena.html'},  name='reestablecer-contrasena'),
    url(r'^reestablecer-contrasena/listo/$', password_reset_done, name='password_reset_done'),
    url(r'^reestablecer-contrasena/confirmar/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', password_reset_confirm, name='password_reset_confirm'),
    url(r'^reestablecer-contrasena/hecho/$', password_reset_complete, {'template_name': 'usuarios/password_rscmp.html'}, name='password_rscmp'),
]

