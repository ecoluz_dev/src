# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from usuarios.models import Usuario
from clientes.models import Cliente
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field
from crispy_forms.bootstrap import TabHolder, Tab
from . import models
import clientes.models



class FormaDeCreacion(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Campo obligatorio.', label="Nombre")
    last_name = forms.CharField(max_length=30, required=True, help_text='Campo obligatorio.', label="Apellidos")
    email = forms.EmailField(max_length=254,required=True,label='Correo electrónico', help_text='Este campo es la clave de ingreso. No podrá modificarse posteriormente.')
    password1 = forms.CharField(widget=forms.PasswordInput() , required=True, label='Contraseña.')
    password2 = forms.CharField(widget=forms.PasswordInput(), required=True, label='Vuelva a ingresar su contraseña.')
    terms = forms.BooleanField(
        error_messages={'required': 'Debes aceptar los términos y condiciones'},
        label="Acepto los términos y condiciones"
    )
    #tipo_usuario = forms.CharField(label='tipo_usuario', widget=forms.TextInput( attrs= {'type' : 'hidden' }), required=False )

    class Meta:
        model = Cliente
        fields = (
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2',
            'terms',
            #'tipo_usuario'
        )


class CommonFormHelper(FormHelper):
    def __init__(self):
        super(CommonFormHelper, self).__init__()
        self.disable_csrf = True
        self.form_tag = False


class FormaEditarClienteU(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = (
            'first_name',
            'last_name',
            'telefono',
            'rfc',
            'razon_social',
            'domicilio_fiscal',
            'calle',
            'numero_exterior',
            'numero_interior',
            'colonia',
            'delegacion_municipio',
            'ciudad',
            'estado',
            'codigo_postal',
            'curp',
            'perfil_completo',
        )

    def __init__(self, *args, **kwargs):
        self.helper_name_info = CommonFormHelper()
        self.helper_name_info.layout = Layout(
            Tab('infos',
                Field('first_name', css_class='form-control-white'),
                Field('last_name', css_class='form-control-white'),
                Field('telefono', css_class='form-control-white'),
                Field('calle', css_class='form-control-white'),
                Field('numero_exterior', css_class='form-control-white'),
                Field('numero_interior', css_class='form-control-white'),
                Field('colonia', css_class='form-control-white'),
                Field('delegacion_municipio', css_class='form-control-white'),
                Field('ciudad', css_class='form-control-white'),
                Field('estado', css_class='form-control-white'),
                Field('codigo_postal', css_class='form-control-white'),
                Field('rfc', css_class='form-control-white', pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z0-9]{3}$",
                      title="Debe escribir un RFC con el formato válido."),
                Field('razon_social', css_class='form-control-white'),
                Field('domicilio_fiscal', css_class='form-control-white'),
                Field('curp', css_class='form-control-white',
                      pattern="^[a-zA-Z]{4}\d{6}[a-zA-Z]{5}[a-zA-Z0-9]{3}$",
                      title="Debe escribir una CURP con el formato válido."),
                Field('perfil_completo', type="hidden"),
            ),
        )

        super(FormaEditarClienteU, self).__init__(*args, **kwargs)


class FormaEditarUsuario(forms.ModelForm):

    class Meta:
        model = Usuario
        fields = (
            'a_identificacion',
            'a_identificacion_beneficiarios',
            'a_domicilio_negocio',
            'a_domicilio_fiscal',
            'a_RFC',
            'a_CURP',
            'foto_negocio',
            'a_buro',
            'a_curriculum',
            'foto_dar',
            'a_estados_cuenta',
            'a_declaraciones',
            'a_constitutiva',
            'a_rpc',
            'rfc_pm',
            'domicilio_fiscal_pm',
            'id_representante',
            'estado_cuenta',
            'bio',
        )

    def __init__(self, *args, **kwargs):
        self.helper_some_info = CommonFormHelper()
        self.helper_some_info.layout = Layout(
            Tab('subir-docs',
                'DAR_id',
                'plazas',
                'a_identificacion',
                'a_identificacion_beneficiarios',
                'a_domicilio_negocio',
                'a_domicilio_fiscal',
                'a_RFC',
                'a_CURP',
                'foto_negocio',
                'a_buro',
                'a_curriculum',
                'foto_dar',
                'a_estados_cuenta',
                'a_declaraciones',
                'a_constitutiva',
                'a_rpc',
                'rfc_pm',
                'domicilio_fiscal_pm',
                'id_representante',
                'estado_cuenta',
                'bio',
                #InlineRadios('HypertensionHX'),
            ),
        )

        # self.helper_some_other_info = CommonFormHelper()
        # self.helper_some_other_info.layout = Layout(
        #     Tab(
        #         'Tab 4',
        #         'some_other_field_or_whatevere_you_want_here',
        #     ),
        #)
        super(FormaEditarUsuario, self).__init__(*args, **kwargs)


#
# class FormaEditarClienteU(forms.ModelForm):
#     # username = forms.CharField(max_length=12, required=True, help_text='Campo obligatorio.', label="Nombre de usuario")
#     # first_name = forms.CharField(max_length=30, required=True, help_text='Campo obligatorio.', label="Nombre")
#     # last_name = forms.CharField(max_length=30, required=True, help_text='Campo obligatorio.', label="Apellidos")
#     # email = forms.EmailField(max_length=254,required=True, help_text='Ingrese su dirección de correo electrónico.')
#
#     class Meta:
#
# class FormaEditarUsuario(forms.ModelForm):
#     helper = FormHelper()
#     helper.form_tag = False
#     helper.layout = Layout(
#         TabHolder(
#             Tab(
#
#             ),
#             Tab(# Documentos del cliente
#
#                 )
#             )
#         )
#
#     class Meta:
#         model = Usuario
#         fields = [
#         ]


#
# #
# #
# class FormaEditarUser(forms.ModelForm):
#     class Meta:
#         model = Usuario
#         fields = ('first_name', 'last_name', 'email' , 'foto_file')
#
#
# class FormaEditarPerfil(forms.ModelForm):
#     helper = FormHelper()
#     helper.form_tag = False
#     helper.layout = Layout(
#         TabHolder(
#             Tab(
#                 'Mis datos',
#                 Field('first_name', css_class='form-control-white'),
#                 Field('last_name', css_class='form-control-white'),
#                 'foto_file',
#                 Field('telefono', css_class='form-control-white'),
#             ),
#         )
#     )
#
#     class Meta:
#         model = Usuario
#         fields = [
#             'first_name',
#             'last_name',
#             'foto_file',
#             'telefono',
#         ]
#
#
# class FormaVerPerfil(forms.ModelForm):
#     helper = FormHelper()
#     helper.form_tag = False
#     helper.layout = Layout(
#         TabHolder(
#             Tab(
#                 'Mis datos',
#                 'first_name',
#                 'last_name',
#                 'foto_file',
#                 'telefono',
#             ),
#
#         )
#     )
#
#     class Meta:
#         model = Usuario
#         fields = [
#             'first_name',
#             'last_name',
#             'foto_file',
#             'telefono',
#         ]
