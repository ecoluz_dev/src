# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.db import models
#from django.contrib.auth.models import User
from django.dispatch import receiver
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save
from clientes.models import Cliente

# Create your models here.


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'usuarios/user_{0}/{1}'.format(instance.id, filename)


class Usuario(models.Model):

    plazas_CHOICES = (
        ('AGS', 'Aguascalientes'),
        ('MET', 'Area Metropolitana'),
        ('CPE', 'Campeche'),
        ('CUN', 'Cancun'),
        ('CJS', 'Cd. Juarez'),
        ('COB', 'Cd. Obregon'),
        ('CVA', 'Cd. Valles'),
        ('CVM', 'Cd. Victoria'),
        ('CUU', 'Chihuahua'),
        ('CHG', 'Chilpancingo'),
        ('CTZ', 'Coatzacoalcos'),
        ('COL', 'Colima'),
        ('CVJ', 'Cuernavaca'),
        ('CUL', 'Culiacan'),
        ('DGO', 'Durango'),
        ('GDL', 'Guadalajara'),
        ('HMO', 'Hermosillo'),
        ('IRA', 'Irapuato'),
        ('GTO', 'Leon'),
        ('MAZ', 'Mazatlan'),
        ('MID', 'Merida'),
        ('MXL', 'Mexicali'),
        ('MTY', 'Monterrey'),
        ('MOR', 'Morelia'),
        ('OAX', 'Oaxaca'),
        ('PCA', 'Pachuca'),
        ('PAZ', 'Poza Rica'),
        ('PUE', 'Puebla'),
        ('PVR', 'Puerto Vallarta'),
        ('QRO', 'Queretaro'),
        ('SCX', 'Salina Cruz'),
        ('SAL', 'Saltillo'),
        ('SLP', 'San Luis Potosi'),
        ('TAM', 'Tampico'),
        ('NAY', 'Tepic'),
        ('TIJ', 'Tijuana'),
        ('TLX', 'Tlaxcala'),
        ('TLC', 'Toluca'),
        ('TRC', 'Torreon'),
        ('TGZ', 'Tuxtla Gutierrez'),
        ('UPN', 'Uruapan'),
        ('VER', 'Veracruz'),
        ('VSA', 'Villahermosa'),
        ('XAL', 'Xalapa'),
        ('ZAM', 'Zamora')
    )

    ESTADO_CIVIL = (
        ('SOLTERA/O', 'SOLTERA/O'),
        ('CASADA/O', 'CASADA/O'),
    )
    REGIMEN = (
        ('NO APLICA', 'NO APLICA'),
        ('SEPARACIÓN DE BIENES', 'SEPARACIÓN DE BIENES'),
        ('SOCIEDAD CONYUGAL', 'SOCIEDAD CONYUGAL'),
    )

    ecouser = models.OneToOneField('clientes.Cliente', on_delete=models.CASCADE)
    DAR_id = models.CharField(_('ID DAR'), max_length=3, blank=True)
    plazas = models.CharField(_('Plaza DAR'),  max_length=23, choices=plazas_CHOICES)
    # ATRIBUTOS DE CLIENTE
    # email = models.EmailField('Correo electrónico', unique=False)                                                 # *
    # foto_file = models.FileField(_('Foto del cliente'), upload_to=user_directory_path, blank=True)               # *
    # telefono = models.CharField(_('Teléfono'), max_length=10, blank=True)
    # calle = models.CharField(_('Calle'), max_length=10, blank=True)                                              # *
    # numero_exterior = models.CharField(_('Número exterior'), max_length=10, blank=True)                          # *
    # numero_interior = models.CharField(_('Número interior'), max_length=10, blank=True)                          # *
    # colonia = models.CharField(_('Colonia'), max_length=10, blank=True)                                          # *
    # delegacion_municipio = models.CharField(_('Delegación o Municipio'), max_length=50, blank=True)              # *
    # ciudad = models.CharField(_('Ciudad'), max_length=50, blank=True)                                            # *
    # estado = models.CharField(_('Estado'), max_length=50, blank=True)                                            # *
    # codigo_postal = models.IntegerField(_('Código postal'), blank=True, default=0)                               # *
    # estado_civil = models.CharField(_('Estado civil'), max_length=10, choices=ESTADO_CIVIL, blank=True)
    # regimen_conyugal = models.CharField(_('Régimen conyugal'), max_length=20, choices=REGIMEN, blank=True)
    # rfc = models.CharField(_('RFC'), max_length=20, blank=True)
    # razon_social = models.CharField(_('Razón social'), max_length =50, blank=True)
    # domicilio_fiscal = models.CharField(_('Domicilio fiscal'), max_length=50, blank=True)
    # curp = models.CharField(_('CURP'), max_length=30, blank=True)
    # DOCUMENTOS DAR
    a_identificacion = models.FileField(_('Identificación oficial'), upload_to=user_directory_path, blank=True)
    a_identificacion_beneficiarios = models.FileField(_('Identificación oficial beneficiarios'), upload_to=user_directory_path, blank=True)
    a_domicilio_negocio = models.FileField(_('Comprobante domicilio negocio'), upload_to=user_directory_path, blank=True)
    a_domicilio_fiscal = models.FileField(_('Comprobante domicilio fiscal'), upload_to=user_directory_path, blank=True)
    a_RFC = models.FileField(_('Cédula RFC'), upload_to=user_directory_path, blank=True)
    a_CURP = models.FileField(_('Cédula CURP'), upload_to=user_directory_path, blank=True)
    foto_negocio = models.FileField(_('Foto del negocio'), upload_to=user_directory_path, blank=True)
    a_buro = models.FileField(_('Consulta buró de crédito'), upload_to=user_directory_path, blank=True)
    a_curriculum = models.FileField(_('Currículum'), upload_to=user_directory_path, blank=True)
    foto_dar = models.FileField(_('Foto del usuario'), upload_to=user_directory_path, blank=True)
    a_estados_cuenta = models.FileField(_('Estados de cuenta bancarios'), upload_to=user_directory_path, blank=True)
    a_declaraciones = models.FileField(_('Declaraciones fiscales anuales'), upload_to=user_directory_path, blank=True)
    a_constitutiva = models.FileField(_('Acta constitutiva'), upload_to=user_directory_path, blank=True)
    a_rpc = models.FileField(_('Cédula RPC'), upload_to=user_directory_path, blank=True)
    rfc_pm = models.FileField(_('RFC de Persona Moral'), upload_to=user_directory_path, blank=True)
    domicilio_fiscal_pm = models.FileField(_('Comprobante domicilio fiscal de Persona Moral'), upload_to=user_directory_path, blank=True)
    id_representante = models.FileField(_('Identificación del representante'), upload_to=user_directory_path, blank=True)
    estado_cuenta = models.FileField(_('Estado de cuenta'), upload_to=user_directory_path, blank=True)
    bio = models.TextField(max_length=400, blank=True)
    # OTROS
    #perfil_completo = models.BooleanField(_('Información de perfil completa'), blank=True, default=False)


    def __unicode__(self):
        return str(self.DAR_id)


    def nombre_usuario(self):
        return self.ecouser.first_name + ' ' + self.ecouser.last_name

    nombre_usuario.short_description = 'Nombre'

    def ciudad_usuario(self):
        return self.ecouser.ciudad

    ciudad_usuario.short_description = 'Ciudad'


    def es_staff(self):
        return self.ecouser.is_staff

    es_staff.short_description = 'Staff'


    def correo_usuario(self):
        return self.ecouser.email
    correo_usuario.short_description = 'Correo Electrónico'


    def fecha_creacion(self):
        return self.ecouser.date_joined
    fecha_creacion.short_description = 'Fecha de creación'

    class Meta:
        ordering = ['DAR_id']


    #
    # def link_cliente(self):
    #     return "<a href="/eco-v1/clientes/cliente/%s/" target='_blank'>Detalles cliente<a/>" % self.ecouser.pk
    #
    # link_cliente.allow_tags = True





@receiver(post_save, sender='clientes.Cliente')
def create_user_profile(sender, instance, created, **kwargs):
    if created and instance.tipo_usuario == 'usuarioECO':
        Usuario.objects.create(ecouser=instance)





