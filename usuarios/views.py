# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
# from .tokens import account_activation_token
from django.contrib import messages
from django.utils.translation import ugettext as _
from .models import Usuario
from usuarios.forms import FormaEditarClienteU, FormaDeCreacion, FormaEditarUsuario, CommonFormHelper #, FormaEditarPerfil, FormaEditarUser
from clientes.models import Cliente
from ecoclientes.models import Eco, Equipo
import clientes.models

# Create your views here.

def registro(request):
    if request.method == 'POST':
        form = FormaDeCreacion(data=request.POST)
        if form.is_valid():
            a = form.save(commit=False)
            stt = str(form.cleaned_data['first_name'])[0]
            stap = str(form.cleaned_data['last_name'])
            try:
                sta = stap.split(' ')
                iddar = stt + sta[0][0] + sta[-1][0]
            except:
                iddar = stt + stap[:2].upper()
            setattr(a, 'tipo_usuario', 'usuarioECO')
            a.save()
            b = Usuario.objects.get(ecouser=a)
            setattr(b, 'DAR_id', iddar.upper())
            b.save()
            new_user = authenticate(email=form.cleaned_data['email'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            return redirect('usuarios:editar_perfil')
    else:
        form = FormaDeCreacion()
    return render(request, 'usuarios/registro.html', {'form': form})

#
@login_required(login_url='usuarios:acceso')
#@transaction.atomic
def editar_perfil(request):
    if request.method == 'POST':
        cliente_form = FormaEditarClienteU(request.POST, request.FILES, instance=request.user)
        user_form = FormaEditarUsuario(request.POST, request.FILES, instance=request.user.usuario)
        if user_form.is_valid() and cliente_form.is_valid():
            cliente_form.save()
            user_form.save()
            messages.success(request, _('La información se ha guardado correctamente.'))
            return redirect('/usuarios/perfil')
        else:
            messages.error(request, _('Por favor corrige los errores.'))
    else:
        user_form = FormaEditarUsuario(instance=request.user.usuario)
        cliente_form = FormaEditarClienteU(instance=request.user)
        return render(request, 'usuarios/editar_perfil.html', {
        'user_form': user_form, 'cliente_form': cliente_form
    })



# @login_required
# #@transaction.atomic
# def editar_perfil_(request):
#     if request.method == 'POST':
#         user_form = FormaEditarPerfil(request.POST, instance=request.user)
#       #  profile_form = FormaEditarPerfil(request.POST, instance=request.user.profile)
#         if user_form.is_valid(): #and profile_form.is_valid():
#             user_form.save()
#             # profile_form.save()
#             messages.success(request, _('Tu perfil se ha actualizado correctamente.'))
#             return redirect('/usuarios/perfil')
#         else:
#             messages.error(request, _('Por favor corrige los errores.'))
#     else:
#         user_form = FormaEditarPerfil(instance=request.user)
#       #  profile_form = FormaEditarPerfil(instance=request.user)
#     return render(request, 'usuarios/editar_perfil.html', {
#         'user_form': user_form,
#     })
#
#
#

@login_required(login_url='usuarios:acceso')
def perfil(request):
    request.session.set_expiry(5000)
    ultic = request.user
    ultiu = Usuario.objects.get(ecouser=ultic)
    dar = ultiu.DAR_id
    registros = Equipo.objects.all()
    regsdar = []
    for uno in registros:
        s = str(uno.fxf_eqp)
        if s[:3] == dar:
            regsdar += [uno]
    for eco_registro in regsdar:
        eco_regis = Eco.objects.get(fxf=eco_registro.fxf_eqp)
        eco_registro.fxf_eqp = eco_regis
        eco_registro.numero_credito = eco_regis.numero_credito
        eco_registro.nombre_beneficiario = eco_regis.nombre_beneficiario
        # eco_registro.patrocinado = eco_regis.patrocinado
        eco_registro.estatus = eco_regis.estatus
        eco_registro.fecha_compra = eco_regis.fecha_compra
        eco_registro.obs = eco_regis.obs
    args = {'user_profile': request.user, 'authenticated': request.user.is_authenticated(), 'usuario': ultiu, 'registros': regsdar,} # 'eco_regis': eco_regis}
    return render(request, 'usuarios/perfil.html', args)


@login_required(login_url='usuarios:acceso')
def cambiar_contrasena(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            messages.success(request, 'La contraseña se actualizó existosamente')
            return redirect('/usuarios/perfil')
        else:
            return redirect('usuarios/cambiar-contrasena')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form, 'authenticated': request.user.is_authenticated()}
        return render(request, 'usuarios/cambiar_contrasena.html', args)