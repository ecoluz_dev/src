# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from models import Usuario

from django.contrib import admin
from django.db import models
from django.conf.locale.en import formats as en_formats

# Register your models here.



en_formats.DATETIME_FORMAT = "d b Y"



class UsuarioAdmin(admin.ModelAdmin):

    def formato_fecha(self, obj):
        return obj.timefield.strftime("%d %b %Y ")

    formato_fecha.admin_order_field = 'fecha_creacion'
    formato_fecha.short_description = 'Fecha de creación'

    list_display = ['nombre_usuario', 'DAR_id',  'plazas' , 'correo_usuario', 'es_staff', 'fecha_creacion'] # 'link_cliente', 'fecha_creacion']
   # list_filter = [ 'fecha_creacion', ]



admin.site.register(Usuario, UsuarioAdmin)


