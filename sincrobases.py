
from ecoclientes.models import Inventario



prods = [
    {
        "Id": "baHks72VCVYmwHinfSsUKQ2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "Ajuste",
        "Name": "Ajuste",
        "Description": "Ajuste de Renta Diciembre 2017",
        "Price": 193.9655,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "idLQq4pxs8732vu0WU1tBA2",
        "Unit": "",
        "IdentificationNumber": "Arreglo",
        "Name": "Arreglo ",
        "Description": "Arreglo de actuadores de bomba. Incluye materiales y mano de obra.",
        "Price": 2500,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "U7j8zX5mf-kfMU7TnKjDUg2",
        "Unit": "",
        "IdentificationNumber": "ASW-12A2INV/SS",
        "Name": "ASW-12A2INV/SS",
        "Description": "Aire acondicionado tipo minisplit inverter solo frio Marca Aux Modelo ASW-12A2INV/SS 1 Tr",
        "Price": 9826.72,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "RMcSOVWHJRg9JAx_trG-qg2",
        "Unit": "",
        "IdentificationNumber": "ASW-H18B2INV/SS",
        "Name": "ASW-H18B2INV/SS",
        "Description": "Aire acondicionado tipo minisplit inverter frio y calor Marca Aux Modelo ASW-H18B2INV/SS 1.5 Tr",
        "Price": 13361.21,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "eRk3kkVtr-PwfvkiWdFfKw2",
        "Unit": "",
        "IdentificationNumber": "ASW-H24B2INV/SS",
        "Name": "ASW-H24B2INV/SS",
        "Description": "Aire acondicionado tipo minisplit Inverter frío y calor Marca Aux, Modelo ASW-H24B2INV/SS, CAP 2 Tr",
        "Price": 16809.48,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "6rGS4z6M48bvlqWOHAW8eQ2",
        "Unit": "",
        "IdentificationNumber": "BHS20-F2 E1C115",
        "Name": "BHS20-F2 E1C115BSN",
        "Description": "Vitrina cerrada de temperatura media con vidrio curvo con refrigerante R-134A Marca Imbera Modelo BHS20-F2 E1C115BSN, CAP. 286.8 Lts. ",
        "Price": 30300,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "133YucxMpEiMQebZGh6BVQ2",
        "Unit": "",
        "IdentificationNumber": "BHS30-F2 E1C115",
        "Name": "BHS30-F2 E1C115BSN",
        "Description": "Vitrina cerrada de temperatura media con vidrio curvo con refrigerante R-134A Marca Imbera Modelo BHS30-F2 E1C115BSN, CAP. 496 Lts. ",
        "Price": 38602,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "u1mB6nTw3auar5dU60m4sg2",
        "Unit": "",
        "IdentificationNumber": "L200",
        "Name": "Camioneta",
        "Description": "Camioneta Pick Up marca Mitsubishi modelo L200 Color Blanco Versión Doble Cabina 4x2 Gasolina, transmisión manual, 4 cilindros, año 2014 número de serie MMBMG45H2ED020599, motor UCAV0443. ",
        "Price": 1,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "xSmowsoSyh9S69FUyOJh-Q2",
        "Unit": "",
        "IdentificationNumber": "Cargo extra SER",
        "Name": "Cargo extra SER HUB",
        "Description": "Cargo por pago extemporaneo SER HUB",
        "Price": 517.7585,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "mhQsPir3xb676SVyk3-kqA2",
        "Unit": "",
        "IdentificationNumber": "CAT-05",
        "Name": "CAT-05",
        "Description": "Congelador horizontal con placa fría curvo, Marca Criotec Modelo CAT-05 cap. 141.3 lts.",
        "Price": 11073.42,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "x85VgrM4NSDHUHBIRN0fIw2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "CAYD",
        "Name": "CAYD",
        "Description": "Centro de Acopio y Destruccion",
        "Price": 366.37,
        "CodeProdServ": "76121501",
        "NameCodeProdServ": "Recolección o destrucción o transformación o eliminación de basuras",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "ZDl2-7du261yRskva091YA2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "CAYD",
        "Name": "CAYD 2018",
        "Description": "Centro de Acopio y Destruccion",
        "Price": 366.3793,
        "CodeProdServ": "76121501",
        "NameCodeProdServ": "Recolección o destrucción o transformación o eliminación de basuras",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "p1NdS_odBeHQDCxc5rLCAQ2",
        "Unit": "",
        "IdentificationNumber": "CBH-60",
        "Name": "CBH-60",
        "Description": "Conservador vertical con puerta solida y placa fria Marca Criotec Modelo CHB-060 litros utiles 739",
        "Price": 19078.37,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "0V5iB50EYBYGiW-lEkgj4A2",
        "Unit": "",
        "IdentificationNumber": "CCH-15",
        "Name": "CCH-15",
        "Description": "Congelador horizontal con placa fría cristal plano, Marca Criotec Modelo CCH-15 litros utiles 298.3 ",
        "Price": 15380.34,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "4s03ibF4Vn2qkYH0GBeJIQ2",
        "Unit": "",
        "IdentificationNumber": "CFC-6P080",
        "Name": "CFC-6P080",
        "Description": "Cuarto frío de conservación con 6 puertas de exhibición Marca Criotec Modelo CFC-6P080 Litros útiles 5743",
        "Price": 133671,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "wFVmjKEYoz-kl5ekwyVUUg2",
        "Unit": "",
        "IdentificationNumber": "CFX-08",
        "Name": "CFX-08",
        "Description": "Enfriador vertical con circulación forzada de aire Marca Criotec Modelo CFX-08  Litros útiles 163",
        "Price": 14424.61,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "MS8wdyfIMrqyv1du6wkBfA2",
        "Unit": "",
        "IdentificationNumber": "CFX-13",
        "Name": "CFX-13",
        "Description": "Enfriador vertical con circulación forzada de aire Marca Criotec Modelo CFX-13  Litros útiles 280",
        "Price": 19356.93,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "IkNz04fv9zZ4-EfMhdlc7g2",
        "Unit": "",
        "IdentificationNumber": "CFX-17",
        "Name": "CFX-17",
        "Description": "Enfriador vertical Marca Criotec Modelo CFX-17 1P litros útiles 394",
        "Price": 20923.91,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "Oak8H4yJEfdev6Knn5iqng2",
        "Unit": "",
        "IdentificationNumber": "CFX-19",
        "Name": "CFX-19",
        "Description": "Enfriador vertical con circulación forzada de aire Marca Criotec Modelo CFX-19 litros útiles 437",
        "Price": 21742.99,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "2Xbvj5NhJ0YJUSoLr8CsNw2",
        "Unit": "",
        "IdentificationNumber": "CFX-24-2P",
        "Name": "CFX-24-2P",
        "Description": "Enfriador vertical Marca Criotec Modelo CFX-24 2P litros útiles 465",
        "Price": 21972.58,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "2WR7rk3Y3E2JyyveUObVDg2",
        "Unit": "",
        "IdentificationNumber": "CFX-30 3P",
        "Name": "CFX-30 3P",
        "Description": "ENFRIADOR VERTICAL CON CIRCULACIÓN FORZADA DE AIRE, MARCA CRIOTEC, MODELO CFX-30 3P,  CAP. 577 LTS",
        "Price": 37777.47,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "nBr5SFb-KPHZUk7zT0OiXg2",
        "Unit": "",
        "IdentificationNumber": "CFX-37 2P",
        "Name": "CFX-37 2P",
        "Description": "Enfriador vertical con circulacion forzada de aire Marca Criotec Modelo CFX-37 2P litros útiles 756",
        "Price": 35416.82,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "wGzqtoci28jcF8mo71qEtA2",
        "Unit": "",
        "IdentificationNumber": "CFX-37 4P",
        "Name": "CFX-37 4P",
        "Description": "Enfriador vertical con circulacion forzada de aire Marca Criotec Modelo CFX-37 4P litros útiles 756",
        "Price": 34585.77,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "o_kYZqFqvw0-rU16bMQLfw2",
        "Unit": "",
        "IdentificationNumber": "CFX-42 2P",
        "Name": "CFX-42 2P",
        "Description": "Enfriador Vertical con circulación forzada de aire, Marca Criotec, Modelo CFX-42 2p, Litros Utiles. 886 LTS",
        "Price": 36722.66,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "3MyRE0TIFCeNjQuIIvkzZQ2",
        "Unit": "",
        "IdentificationNumber": "CFX-42 4P",
        "Name": "CFX-42 4P",
        "Description": "Enfriador vertical con circulacion forzada de aire Marca Criotec Modelo CFX-42 4P litros útiles 886",
        "Price": 39500.46,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "yziBkMV_R7cnZ6sfxFI-xg2",
        "UnitCode": "H87",
        "Unit": "Pieza",
        "IdentificationNumber": "CFX-64-3P",
        "Name": "CFX-64-3P",
        "Description": "Enfriador vertical con circulacion forzada de aire Marca Criotec Modelo CFX-64 3P litros útiles 1260",
        "Price": 52720.32,
        "CodeProdServ": "24131514",
        "NameCodeProdServ": "Unidad de almacenamiento de pre - enfriamiento y frío",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "sVSdISixqRjT-nBKt4XXmg2",
        "Unit": "",
        "IdentificationNumber": "CFX-SL",
        "Name": "CFX-SL",
        "Description": " Enfriador vertical con circulación forzada de aire Marca Criotec Modelo CFX-SL litros útiles 217.50",
        "Price": 17491.13,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "zKO35NsfHytwadR4agBwAg2",
        "Unit": "",
        "IdentificationNumber": "CBH-150",
        "Name": "CHB-150",
        "Description": "Conservador vertical con puerta solida y placa fria Marca Criotec Modelo CHB-150 litros utiles 1653",
        "Price": 26762.38,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "1stNGr5krPdgtNC_QG3FUg2",
        "Unit": "",
        "IdentificationNumber": "-",
        "Name": "comdar",
        "Description": "comdar",
        "Price": 2100,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "WlUEblRjIt2fDxxwVY5j7w2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "CSC-3M 216",
        "Name": "CSC-3M 216",
        "Description": "Cuarto frio con 6 puertas de exhibicion sin piso Marca Criotec Modelo CSC-3M 216 Litros utiles 14319",
        "Price": 106358.76,
        "CodeProdServ": "24131503",
        "NameCodeProdServ": "Cuartos fríos",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "J1a2SYkzizbbeXwfChGGDg2",
        "Unit": "",
        "IdentificationNumber": "CTCC-10",
        "Name": "CTCC-10",
        "Description": "CONGELADOR HORIZONTAL CON PLACA FRIA CRIOTEC, TAPA COFRE, CAP. 262 LTS",
        "Price": 9770.08,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "QQ590FI0VuavoNJSbOVv-w2",
        "Unit": "",
        "IdentificationNumber": "CTCC-15 AI",
        "Name": "CTCC-15 AI",
        "Description": "Congelador Horizontal con Placa Fría, Marca Criotec, Modelo CTCC-15 AI, Tapa de Acero Inoxidable, CAP. 378 Lts. Utiles",
        "Price": 12694.6,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "t6e5XKJdp_l0WCqEWnJ53Q2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "CTCC-25",
        "Name": "CTCC-25",
        "Description": "Congelador horizontal con placa fria, tapa cofre Marca Criotec Modelo CTCC-25 litros Utiles 643                            ",
        "Price": 15098.12,
        "CodeProdServ": "24131510",
        "NameCodeProdServ": "Refrigerador de mostrador",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "ySVIoCwi-Ekd--1x-bkSbg2",
        "UnitCode": "H87",
        "Unit": "Pieza",
        "IdentificationNumber": "CTCC-25 AI ",
        "Name": "CTCC-25 AI ",
        "Description": "Congelador horizontal con placa fria, tapa acero inoxidable Marca Criotec Modelo CTCC-25AI litros útiles 643",
        "Price": 16902.03,
        "CodeProdServ": "24131601",
        "NameCodeProdServ": "Congeladores horizontales",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "m4g7NXS9defhk1dcEBmzAA2",
        "Unit": "",
        "IdentificationNumber": "-",
        "Name": "descuento cfx",
        "Description": "descuento cfx",
        "Price": 11073.42,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "i1lAa5L3ryIJgVt2EIPnjQ2",
        "Unit": "",
        "IdentificationNumber": "E9W",
        "Name": "Eco 9W",
        "Description": "Eco 9W",
        "Price": 124.8103,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "mP1iSMt0QcsJjnJISRrcZg2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "Gastos",
        "Name": "Gastos de Instalacion",
        "Description": "Gastos de Instalacion",
        "Price": 27000,
        "CodeProdServ": "80161600",
        "NameCodeProdServ": "Supervisión de instalaciones de negocios",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "lDoRJnhKIcwXCpXbjiGj-w2",
        "Unit": "",
        "IdentificationNumber": "T-5 2x28",
        "Name": "Luminario Gabinete Canal 2 x 28 T-5 ",
        "Description": "Luminario Gabinete Canal 2 x 28 T-5 con lamparas plusrite y balasto electrónico Philips con sello. ",
        "Price": 1044.14,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "MRD70dNbMvDjaahcYYFkcQ2",
        "Unit": "",
        "IdentificationNumber": "T1",
        "Name": "Mantenimiento de Maquinas y Equipos",
        "Description": "Nu´mero de\r\nProveedor: 0050486764",
        "Price": 559.68,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "ZjY1i0IRc2Il55ahTf0OFQ2",
        "Unit": "",
        "IdentificationNumber": "PR-GB-CNL-2X28-",
        "Name": "PR-GB-CNL-2X28-T5",
        "Description": "Gabineta canal 2 X 28 T-5, con lamparas Marca Plusrite y Balastro electronico Marca Universal Lighting Technologies, con sello FIDE Cap. 65 W",
        "Price": 650,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "Xkc6qY64vX3nWhin51uJag2",
        "Unit": "",
        "IdentificationNumber": "SER HUB SH-04",
        "Name": "SER HUB Oficina 4",
        "Description": "Servicios administrativos de oficina SER HUB SH-04",
        "Price": 4765.71,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "CBBcDDBn7PuEJBmwu7PVpQ2",
        "Unit": "",
        "IdentificationNumber": "SERHUB 11-12",
        "Name": "Servicios Administrativos de Oficina ",
        "Description": "Servicios Administrativos de Oficina #11 y #12 Diciembre 2017",
        "Price": 8474.5086,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "qiJbGsCIOeZ8GXAHgj4uzA2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB 11 ",
        "Name": "Servicios Administrativos de Oficina  SH-11  ",
        "Description": "Servicio Administrativo de Oficina SH-11 ",
        "Price": 4491.387,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "QfOkTx8mlrdmORpbDQtg4Q2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SER HUB # 1",
        "Name": "Servicios Administrativos de Oficina # 1 ",
        "Description": "Servicios Admisnitrativos de Oficina # 1",
        "Price": 5811.21,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "IOc6qUo9E9J2UX5xCTfgBg2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB-10",
        "Name": "Servicios Administrativos de Oficina # 10",
        "Description": "Servicios Administrativos de Oficina # 10",
        "Price": 3670.44,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "353GOfefYyeJ6HbLSupmWw2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB-13",
        "Name": "Servicios Administrativos de Oficina # 13",
        "Description": "Servicios Administrativos de Oficina # 13",
        "Price": 4291.94,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "CJMgrsuEzzVj5lVaDdYUng2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB-14",
        "Name": "Servicios Administrativos de Oficina # 14",
        "Description": "Servicios Administrativos de Oficina # 14 ",
        "Price": 6063.23,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "-ZIpSWe7v75RN7SCXScvQw2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB #15",
        "Name": "Servicios Administrativos de Oficina # 15",
        "Description": "Servicios Administrativos de Oficina #15",
        "Price": 8941.1982,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "AGp4kgwZvlxm7ekLbLeA7w2",
        "Unit": "",
        "IdentificationNumber": "SER HUB # 3",
        "Name": "Servicios Administrativos de Oficina # 3 ",
        "Description": "Servicios Administrativos de Oficina # 3",
        "Price": 4491.3793,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "nOtUQZyfhalKlv-aIO6q0w2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SER HUB # 4",
        "Name": "Servicios Administrativos de Oficina # 4 ",
        "Description": "Servicios Administrativos de Oficina # 4 ",
        "Price": 5439.29,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "p930Z_7hcbanESNn6upcwQ2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB #5",
        "Name": "Servicios Administrativos de Oficina #5 ",
        "Description": "Servicios Administrativos de Oficina de Enero 2018",
        "Price": 3879.3103,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "2OYCOKcnZ2AbHYAAuQDfnw2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "#6",
        "Name": "Servicios Administrativos de Oficina #6 ",
        "Description": "Servicios Administrativos de Oficina #6 ",
        "Price": 3017.2413,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "ypVFaMgDrGfJZlH32BFiIA2",
        "Unit": "",
        "IdentificationNumber": "SER HUB #8",
        "Name": "Servicios Administrativos de oficina #8 Diciembre ",
        "Description": "Servicios Administrativos de oficina #8 Diciembre 2016 - Abril 2017 ",
        "Price": 3794.5402,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "QlEujcA2h-K_dBH9_HApHQ2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SER HUB #8",
        "Name": "Servicios Administrativos de oficina #8 Diciembre ",
        "Description": "Servicios Administrativos de Oficina Noviembre 2017",
        "Price": 4310.35,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "4A1ZP_KZ_g84aaK5s82kBQ2",
        "Unit": "",
        "IdentificationNumber": "SH-02",
        "Name": "Servicios Administrativos de Oficina SH-02",
        "Description": "Servicios Administrativos de Oficina SH-02\r\nAgosto 2017",
        "Price": 6000,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "elc_-xfTJ5F-5aXTNi6DfQ2",
        "Unit": "",
        "IdentificationNumber": "SH-03",
        "Name": "Servicios Administrativos de Oficina SH-03",
        "Description": "Servicios Administrativos de Oficina SH-03\r\nAgosto 2017",
        "Price": 4536.97,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "D-tFQkZeZHCNgakUpjVpsg2",
        "Unit": "",
        "IdentificationNumber": "SH-06",
        "Name": "Servicios Administrativos de Oficina SH-06",
        "Description": "Servicios Administrativos de Oficina SH-06\r\nAgosto 2017",
        "Price": 2596.37,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "NHygRB8FhXB51jqwYS7zVQ2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SERHUB #07",
        "Name": "Servicios Administrativos de Oficina SH-07",
        "Description": "Servicios Administrativos de Oficina #07 Enero 2018",
        "Price": 2779.38,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "ROA51I8fco4E50ZFc3Kp4Q2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "LocalAcambao",
        "Name": "Servicios Administrativos Local Comercial ",
        "Description": "Servicios Administrativos Local Comercial Acambaro, Guanajuato.",
        "Price": 3550,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "nI7VUbirvEixyKIGd-Lwig2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "SH GTO-01",
        "Name": "SH GTO-01",
        "Description": "Servicios Administrativos de local comercial",
        "Price": 3900,
        "CodeProdServ": "80161504",
        "NameCodeProdServ": "Servicios de oficina",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "OmZdelAtiJ5Wy86NrW_blA2",
        "Unit": "",
        "IdentificationNumber": "SH-01",
        "Name": "SH-01",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017",
        "Price": 5811.21,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "xpliDQXkuCR5wlfA9e7o4w2",
        "Unit": "",
        "IdentificationNumber": "SH-02 ",
        "Name": "SH-02",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 6000,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "so9tk3tNW8CzTvNKGRM8vQ2",
        "Unit": "",
        "IdentificationNumber": "SH-05",
        "Name": "SH-05",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 3879.31,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "YCI3cMIIfXM_Sq5bZKdkOA2",
        "Unit": "",
        "IdentificationNumber": "SH-06",
        "Name": "SH-06",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 2596.37,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "SM39YPx17-zt3InYglz7LQ2",
        "Unit": "",
        "IdentificationNumber": "SH-07",
        "Name": "SH-07",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 2779.38,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "WtBX13MKVSB5Nwn6zMBjyw2",
        "Unit": "",
        "IdentificationNumber": "SH-08",
        "Name": "SH-08",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 3689.66,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "dwN3V9kUH10NHXJlwF-knQ2",
        "Unit": "",
        "IdentificationNumber": "SER HUB SH-09",
        "Name": "SH-09",
        "Description": "Servicios Administrativos de Oficina SH-09",
        "Price": 10775.86,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "-sPsqsyb0TgZtT5zhaD5fw2",
        "Unit": "",
        "IdentificationNumber": "SH-10",
        "Name": "SH-10",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 14112.63,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "gnWGhwRwLFCCQWeZ62pHSg2",
        "Unit": "",
        "IdentificationNumber": "SH-11",
        "Name": "SH-11",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 4491.38,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "sh4h9AOupY0nwW8QXBFiEw2",
        "Unit": "",
        "IdentificationNumber": "SH-12",
        "Name": "SH-12",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 4620.42,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "SKJx5TW0Zibr6bCznKxfuQ2",
        "Unit": "",
        "IdentificationNumber": "SH-13",
        "Name": "SH-13",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 4291.94,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "AhUfE4Chfn2GaDHSrxfpKA2",
        "Unit": "",
        "IdentificationNumber": "SH-14",
        "Name": "SH-14",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 6063.23,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "MdD6mRhDuQ58cbYiKnxnuw2",
        "Unit": "",
        "IdentificationNumber": "SH-15",
        "Name": "SH-15",
        "Description": "Servicios Administrativos de oficina mes de septiembre 2017\r\n",
        "Price": 9480.24,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "EHjtk9N1ItklTrwEJ6pp2w2",
        "Unit": "",
        "IdentificationNumber": "Sistema fotovoltáico",
        "Name": "Sistema Fotovoltaíco de 18 paneles Risen Policrist",
        "Description": "Sistema Fotovoltaíco de 18 paneles Risen Policristalinos de 320W con microinversiore APS, sistema de montaje y monitoreo Tigo.",
        "Price": 116728.448,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "09aPPQYi7ejDBR0b_1jp6Q2",
        "Unit": "",
        "IdentificationNumber": "S9W",
        "Name": "Spot 9W",
        "Description": "Spot 9W",
        "Price": 302.156,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "3m9asoF6XsFQgpqRYU9Pbg2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "Transporte",
        "Name": "Transporte",
        "Description": "Transporte",
        "Price": 215.52,
        "CodeProdServ": "78101801",
        "NameCodeProdServ": "Servicios de transporte de carga por carretera (en camión) en área local",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "8L2vjcCtXr2VSzUlIWbZ_A2",
        "UnitCode": "E48",
        "Unit": "Unidad de servicio",
        "IdentificationNumber": "T2018",
        "Name": "Transporte 2018",
        "Description": "Transporte ",
        "Price": 215.5173,
        "CodeProdServ": "78101801",
        "NameCodeProdServ": "Servicios de transporte de carga por carretera (en camión) en área local",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "ZFMb3ATmcxlmzAZi8CYacg2",
        "Unit": "",
        "IdentificationNumber": "U8W",
        "Name": "Universal 8W",
        "Description": "Universal 8W",
        "Price": 225.0344,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "n3GnAdsyB7a6hYVq-Kg09w2",
        "Unit": "",
        "IdentificationNumber": "VCC-120",
        "Name": "VCC-120",
        "Description": "Vitrina cerrada de temperatura media, Marca Criotec Modelo VCC-120 litros útiles 298                                             ",
        "Price": 36647.95,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "8gRNhLpr4dxDkgMVYl0-MA2",
        "UnitCode": "H87",
        "Unit": "Pieza",
        "IdentificationNumber": "VCC-200",
        "Name": "VCC-200",
        "Description": "Vitrina horizontal Marca Criotec Modelo VCC-200 litros Utiles 566                                              ",
        "Price": 42267.75,
        "CodeProdServ": "24131510",
        "NameCodeProdServ": "Refrigerador de mostrador",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "Xgh1nA6eDjJcLtM2l9cLEg2",
        "UnitCode": "H87",
        "Unit": "Pieza",
        "IdentificationNumber": "VCR-200",
        "Name": "VCR-200",
        "Description": "Vitrina horizontal Marca Criotec Modelo VCR-200 litros útiles 1193",
        "Price": 68583.2,
        "CodeProdServ": "24131510",
        "NameCodeProdServ": "Refrigerador de mostrador",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "XXA69vWCkCj8xMtcMmP3rQ2",
        "Unit": "",
        "IdentificationNumber": "VFS16 C BMAB R4",
        "Name": "VFS16 C BMAB R4",
        "Description": "Congelador vertical Marca Imbera Modelo VSF16C  con puerta de cristal y circulación forzada de aire, CAP. 325.16 Lts.",
        "Price": 25488.36,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "Ed5OJIAgrMiJo5jZs_8ejw2",
        "Unit": "",
        "IdentificationNumber": "VFS24 C BMAD R4",
        "Name": "VFS24 C BMAD R4",
        "Description": "Congelador vertical con puerta de cristal y circulación forzada de aire Marca Imbera Modelo VFS24 C BMAD R4 408 litros utiles",
        "Price": 38606.0344,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "3PnkGPy27y92sn7_dIz6Sg2",
        "Unit": "",
        "IdentificationNumber": "VIT VLH17 C BMA",
        "Name": "VIT VLH17 C BMAE EST",
        "Description": "Vitrina cerrada de temperatura media Marca Imbera Modelo VIT VLH17 C BMAE EST litros utiles 362.73",
        "Price": 25483.2,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "q0kRoNeMfaqFdmw2IBOh7g2",
        "Unit": "",
        "IdentificationNumber": "VL140 D BMAD",
        "Name": "VL140 D BMAD",
        "Description": "Enfriador vertical con circulación forzada de aire con refrigerante R- 134A Marca Imbera Modelo VL140 D BMAD 253 litros utiles",
        "Price": 21463.97,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "1_iR5-NOzNJGeoUyTMPgxQ2",
        "Unit": "",
        "IdentificationNumber": "VL192 C BMAD",
        "Name": "VL192 C BMAD",
        "Description": "Enfriador vertical con circulación forzada de aire con refrigerante R- 134A Marca Imbera Modelo VL192 C BMAD 353 litros utiles",
        "Price": 23479.02,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "hDsEuuD9QS2_jIZ0LwWXCw2",
        "Unit": "",
        "IdentificationNumber": "VL480 C BMAD",
        "Name": "VL480 C BMAD",
        "Description": "Enfriador vertical con circulación forzada de aire con refrigerante R- 134A Marca Imbera Modelo VL480 C BMAD 719 litros utiles",
        "Price": 37114.76,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "CRQX89ClPDnvljCBDt7ASg2",
        "Unit": "",
        "IdentificationNumber": "VL735 C BMAD",
        "Name": "VL735 C BMAD",
        "Description": "Enfriador vertical con circulación forzada de aire, Marca Imbera Modelo VL735 C BMAD 1370 litros útiles",
        "Price": 39000,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    },
    {
        "Id": "sgaO-dz-ypiUZSK8PI8I2Q2",
        "UnitCode": "H87",
        "Unit": "Pieza",
        "IdentificationNumber": "VLH20 C BMAE",
        "Name": "VLH20 C BMAE",
        "Description": "Vitrina cerrada de temperatura media con vidrio plano con refrigerante R-134A, Marca Imbera  Modelo VLH20 C BMAE litros utiles 366.1",
        "Price": 35886.9,
        "CodeProdServ": "24131510",
        "NameCodeProdServ": "Refrigerador de mostrador",
        "CuentaPredial": "",
        "Taxes": [
            {
                "Name": "IVA",
                "Rate": 0.16,
                "IsRetention": False,
                "IsFederalTax": True
            }
        ]
    },
    {
        "Id": "MymXjvvqGyN8nLpvpkQbaw2",
        "Unit": "",
        "IdentificationNumber": "VR35 D BMAD 4PC",
        "Name": "VR35 D BMAD 4PC",
        "Description": "Enfriador vertical con circulación forzada de aire con refrigerante R- 134A, Marca Imbera  Modelo VR35 D BMAD 4PC litros utiles 647",
        "Price": 35868.9655,
        "CodeProdServ": "",
        "NameCodeProdServ": "",
        "Taxes": []
    }
]

no = 0
si = 0
for un in prods:
    for inv in Inventario.objects.filter(modelo__startswith=un['IdentificationNumber']):
        if inv:
            setattr(inv, 'id_facturama', un['Id'])
            inv.save()
            si += 1
            print inv, si, un['IdentificationNumber']
        else:
            print un['IdentificationNumber']
            no += 1
print si, no

