# -*- coding: utf-8 -*-
"""ecoluz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from ecoluzweb import views
from django.conf import settings
from django.conf.urls.static import static
from . import views as vw
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import (
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView
)


urlpatterns = [
    url(r'^eco-v1/', admin.site.urls),
    url(r'^$', views.inicio, name='inicio'),
    url(r'^clientes/', include('clientes.urls')),
    url(r'^usuarios/', include('usuarios.urls'), name='usuarios'),
    url(r'^ecoclientes/', include('ecoclientes.urls'), name='ecoclientes'),
    url(r'^conoce-tu-ahorro/', include('bots.urls'), name='conoce-tu-ahorro'),
    url(r'^proyectos/', views.proyectos, name='proyectos'),
    url(r'^que-es-ecoluz/', views.que_es_ecoluz, name='que-es-ecoluz'),
    url(r'^terminos-y-condiciones', views.terminos_y_condiciones, name='terminos-y-condiciones'),
    url(r'^aviso-de-privacidad', views.aviso_de_privacidad, name='aviso-de-privacidad'),
    url(r'^contacto', views.contacto, name='contacto'),
    url(r'^ingresando', vw.wherenext, name='ingresando'),

    url('^reestablecer-contrasena/$', auth_views.password_reset,
        {
            'template_name': 'clientes/password_reset_form.html',
            'html_email_template_name': 'clientes/password_reset_email.html',
        }, name='password_reset'),
    url(r'^reestablecer-contrasena/listo/$',
        PasswordResetDoneView.as_view(template_name='clientes/password_reset_done.html'),
        name='password_reset_done'),
    url(r'^reestablecer-contrasena/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirmView.as_view(template_name='clientes/password_reset_confirm.html'),
        name='password_reset_confirm'),
    url(r'^reestablecer-contrasena/hecho/$',
        PasswordResetCompleteView.as_view(template_name='clientes/password_reset_complete.html'),
        name='password_reset_complete'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
