# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, reverse



@login_required
def wherenext(request):
    """Simple redirector to figure out where the user goes next."""
    print request.user.tipo_usuario
    if request.user.tipo_usuario == 'usuarioECO':
        return HttpResponseRedirect(reverse('usuarios:perfil'))
    else:
        return HttpResponseRedirect(reverse('clientes:perfil'))
