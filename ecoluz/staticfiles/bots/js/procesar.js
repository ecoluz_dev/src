function procesar(option){

    var monto = document.getElementById("id_credito").value;
    var prom_pago = document.getElementById("id_importe_promedio").value;
    var enganche = document.getElementById("enganche").value;
    var consumo_kw = document.getElementById("id_consumo_kw_mes").value;
    var n_paneles = document.getElementById("id_numero_de_paneles").value;

    var mens
    var n_pagos
    if (option == 1){
        mens = document.getElementById("id_pago_mensual_express").value;
        n_pagos = document.getElementById("id_numero_de_pagos_express").value;
        document.getElementById("id_plan").value ="Express";
    }
    if (option == 2){
        mens = document.getElementById("id_pago_mensual_moderado").value;
        n_pagos = document.getElementById("id_numero_de_pagos_moderado").value;
        document.getElementById("id_plan").value = "Moderado";
    }
    if (option == 3){
        mens = document.getElementById("id_pago_mensual_lp").value;
        n_pagos = document.getElementById("id_numero_de_pagos_lp").value;
        document.getElementById("id_plan").value ="Largo Plazo";
    }

    document.getElementById("id_monto_credito").value = monto;
    document.getElementById("id_eng").value = enganche;
    document.getElementById("id_mensualidad").value = mens;
    document.getElementById("id_num_pagos").value = n_pagos;
    document.getElementById("id_consumo_kw").value = consumo_kw;
    document.getElementById("id_importe_mens_cfe").value = prom_pago;
    document.getElementById("id_paneles").value = n_paneles;

    $('#opcion').modal('show')
}
function enviar(){
    var frm = $('#procesar')

    $('#procesar').validate({
    submitHandler: function(){
    $.ajax({//begin
        type: 'POST',
        url: '/conoce-tu-ahorro/procesar/',
        data: $(frm).serialize(),    <!-- package the data -->
        success: function(data){
            <!-- $(frm).hide(); -->
    $("#div-form").html("<h3 style='text-align:center;'>Gracias. Ahora te llevaremos a tu perfil.</h3>");

        },
        error: function(data) {           <!-- else print error -->
            $("#div-form").html("Algo salió mal :( ya hemos sido notificados.");
        }//end error
    });//end ajax

    return false;
    }
    });
}

$(document).ready(function(){
     // Attach Button click event listener
    $("#express").click(function(e){
        e.preventDefault();
        procesar(1);
        document.getElementById("id_enganche_express").value = document.getElementById("enganche").value;
    });

    $("#moderado").click(function(e){
        e.preventDefault();
        procesar(2);

    });

    $("#lp").click(function(e){
        e.preventDefault();
        procesar(3);

    });

});


