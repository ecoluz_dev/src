function calcular(){
    var monto = parseFloat(document.getElementById("id_credito").value);
    console.log(monto)
    var prom_pago = parseFloat(document.getElementById("id_importe_promedio").value);
    console.log(prom_pago)
    var cred = monto - parseFloat(document.getElementById("enganche").value);
    var pag_exp = parseFloat(document.getElementById("id_pago_mensual_express").value);
    console.log(pag_exp)
    var pag_mod = parseFloat(document.getElementById("id_pago_mensual_moderado").value);
    console.log(pag_mod)
    var pag_lp = parseFloat(document.getElementById("id_pago_mensual_lp").value);
    console.log(pag_lp)
    tdi = 0.0145;

    var n_exp = getN(pag_exp, tdi, cred)

    var n_mod = getN(pag_mod, tdi, cred)

    var n_lp = getN(pag_lp, tdi, cred)

    document.getElementById("id_numero_de_pagos_express").value = n_exp;

    document.getElementById("id_numero_de_pagos_moderado").value = n_mod;

    document.getElementById("id_numero_de_pagos_lp").value = n_lp;

}

function getN(p_mens, tdi, cred){
    var a = (tdi * cred) / p_mens;
    var num = (-1 * Math.log(1 - a)).toFixed(5);
    var den = (Math.log(1 + tdi)).toFixed(5);
    var n = Math.ceil(num / den);
    return n
}


window.onload = function () {
    document.getElementById("calculate_button").onclick = calcular;
}